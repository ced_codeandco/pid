<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Cms extends Public_controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $headerData;
    public $contentData;
    public $footerData;
    public function __construct()
    {
        parent::__construct();
    }
    public function index($page_slug)
    {
        $this->load->library('session');
        $wrapperType = $this->uri->segment(3);
        $this->headerData['title']= 'Homepage';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['succMsg'] = $succ_msg;
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $err_msg;
        }
        if ($cmsData = $this->cms_model->getPageDetails($page_slug)) {
//            print_r($cmsData);
            $this->contentData['cmsData'] = $cmsData;
            $this->contentData['show404'] = false;
        } else {
            $this->contentData['show404'] = true;
        }

        if (!empty($cmsData->id) && $cmsData->id == CMS_CONTACT_US_PAGE_ID) {

            $this->load->library('form_validation');

            $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
            $this->form_validation->set_rules('name', 'Name', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|email|required');
            $this->form_validation->set_rules('comments', 'Comments', 'trim|required');
            $this->form_validation->set_rules('captcha', 'Captcha', 'trim|required|callback_valid_captcha');

            if ($this->form_validation->run() == TRUE) {
                $this->load->model('contact_model');
                $data = $this->input->post();
                if (isset($data['captcha'])) unset($data['captcha']);
                $data['created_ip'] = $this->input->ip_address();
                $data['created_date_time'] = date('Y-m-d H:i:s');
                $this->contact_model->addDetails($data);
                $this->sendContactEmail();
                $this->session->set_flashdata('flash_success', 'Enquiry sent successfully!!');
                $url_slug = !empty($cmsData->cms_slug) ? $cmsData->cms_slug : 'contact-us';
                redirect(ROOT_URL . $url_slug, 'refresh');
                exit;
            } else {
                //die('Here');
            }
            $this->generateRobotText();
        }


        if (!empty($wrapperType) && $wrapperType =='ajax') {
            //$this->load->view('templates/header_ajax', $this->headerData);
            $this->load->view('ajax-cms', $this->contentData);
            //$this->load->view('templates/footer_ajax', $this->footerData);
        } else {
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('cms', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }

        /*$this->load->view('templates/header', $this->headerData);
        $this->load->view('cms', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);*/
    }

    public function news_list() {
        $this->load->model('news_model');
        $this->load->library('pagination');

        $count_result = $this->news_model->getCount("is_active='1'");

        $cur_page = $this->input->get('per_page');
        $config['page_query_string'] = TRUE;
        $config['enable_query_strings'] = TRUE;
        $config['base_url'] = ROOT_URL.'/news_list?';
        $config['total_rows'] = $count_result;
        $config['per_page'] = 10;

        $config['full_tag_open'] = '<ul class="pagination">';
        $config['full_tag_close'] = '</ul>';

        $config['first_link'] = '<span >&lt;&lt;</span>';;
        $config['first_tag_open'] = '<li>';
        $config['first_tag_close'] = '</li>';

        $config['prev_link'] = '<span >&lt;</span>';
        $config['prev_tag_open'] = '<li class="prev_li" >';
        $config['prev_tag_close'] = '</li>';

        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';

        $config['next_link'] = '<span >&gt;</span>';
        $config['next_tag_open'] = '<li class="next_li">';
        $config['next_tag_close'] = '</li>';

        $config['cur_tag_open'] = '<li class="active_li"><a href="javascript:void(0)">';
        $config['cur_tag_close'] = '</a></li>';

        $config['last_link'] = '<span >&gt;&gt;</span>';;
        $config['last_tag_open'] = '<li>';
        $config['last_tag_close'] = '</li>';

        $this->pagination->initialize($config);
        $this->contentData['pagination'] = $this->pagination->create_links();

        $limit_string = empty($cur_page) ? " LIMIT $config[per_page]" : " LIMIT $cur_page, $config[per_page]";
        $this->contentData['news_list'] = $this->news_model->getAllRecords('id,title,url_slug,small_description',"is_active='1' AND is_deleted = '0'", "ORDER BY sort_order ASC", $limit_string);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('news_list', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function news($page_slug)
    {
        $this->load->model('news_model');
        $wrapperType = $this->uri->segment(3);
        $this->headerData['title']= 'News';
        $succ_msg = $this->session->flashdata('flash_success');
        $err_msg = $this->session->flashdata('flash_error');
        if(isset($succ_msg) && $succ_msg != ''){
            $this->contentData['succMsg'] = $succ_msg;
        }
        if(isset($err_msg) && $err_msg != ''){
            $this->contentData['errMsg'] = $err_msg;
        }
        if ($cmsData = $this->news_model->getPageDetails($page_slug)) {
//            print_r($cmsData);
            $this->contentData['cmsData'] = $cmsData;
            $this->contentData['show404'] = false;
        } else {
            $this->contentData['show404'] = true;
        }


        if (!empty($wrapperType) && $wrapperType =='ajax') {
            //$this->load->view('templates/header_ajax', $this->headerData);
            $this->load->view('ajax-cms', $this->contentData);
            //$this->load->view('templates/footer_ajax', $this->footerData);
        } else {
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('news', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }

        /*$this->load->view('templates/header', $this->headerData);
        $this->load->view('cms', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);*/
    }

    function sendContactEmail()
    {
        $admin_subject = "New Inquiry on ".SITE_NAME;
        $user_subject = "Thank you for Inquiry on ".SITE_NAME;

        $admin_header = "Dear Administrator,<br /><br />".$_POST['name']." has submitted contact form with below details on ".SITE_NAME.". <br /><br />";
        $user_header = "Dear ".$_POST['name'].",<br /><br /> Thank you for Inquiry on ".SITE_NAME." with below details. <br /><br />";

        $emailMessage  = "<strong>Detail :</strong> <br /><br />";
        $emailMessage .= "Name   		  : ".$_POST['name']." <br /><br />";

        if(isset($_POST['email']) && $_POST['email'] != '')
            $emailMessage .= "E-mail  	  : ".$_POST['email']." <br /><br />";

        if(isset($_POST['comments']) && $_POST['comments'] != '')
            $emailMessage .= "Comments   	  : ".$_POST['comments']." <br /><br />";

        $emailMessage .= "Thanks <br />".SITE_NAME." Team";


        $headers = 'From: '.DEFAULT_EMAIL . "\r\n" .
            'Reply-To: '.DEFAULT_EMAIL . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        if (mail(DEFAULT_EMAIL, $admin_subject, $admin_header.$emailMessage, $headers)){
            mail($_POST['email'], $user_subject, $user_header.$emailMessage, $headers);
        }
        /*if(mail(DEFAULT_EMAIL,SITE_NAME,$_POST['email'],$_POST['name'],$user_subject,$user_header.$emailMessage)){

            sendMail(DEFAULT_EMAIL,SITE_NAME,SUPPORT_EMAIL, SITE_NAME, $admin_subject,$admin_header.$emailMessage);
            return true;
        }*/
    }


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */