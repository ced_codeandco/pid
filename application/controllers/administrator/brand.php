<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Brand extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('brand_model');
		$this->load->model('model_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$brandId =  $this->uri->segment(4);
			if($brandId == ''){
				redirect(ADMIN_ROOT_URL.'brand');
			}else{
				$this->brand_model->changeStatus(0,$brandId);
				$this->session->set_flashdata('flash_success', 'Brand Status changed successfully');
				redirect(ADMIN_ROOT_URL.'brand');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$brandId =  $this->uri->segment(4);
			if($brandId == ''){
				redirect(ADMIN_ROOT_URL.'brand');
			}else{
				$this->brand_model->changeStatus(1,$brandId);
				$this->session->set_flashdata('flash_success', 'Brand Status changed successfully');
				redirect(ADMIN_ROOT_URL.'brand');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$brandId =  $this->uri->segment(4);			
			$this->brand_model->deleteRecord($brandId);
			$this->session->set_flashdata('flash_success', 'Brand deleted successfully');
			redirect(ADMIN_ROOT_URL.'brand');
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$brandId =  $this->uri->segment(4);
			$action = 'Add';
			if($brandId == ''){
				$action = 'Add';
				$this->contentData['brandDetails'] = array();				
			}else{
				$action = 'Edit';
				$brandDetails = $this->brand_model->getDetails($brandId);
				$this->contentData['brandDetails'] = $brandDetails;
				
			}
			
			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
				$this->form_validation->set_rules('title', 'Title', 'trim|required|callback_brand_exist');
					
				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {					
					
						$insertedId = $this->brand_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'Brand Details Added successfully');
							
								redirect(ADMIN_ROOT_URL.'brand');
						}
					}else{
						
						$updateStatus = $this->brand_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'Brand Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'brand');
						}
					}
				}else{
					$_SESSION = $_POST;
					
				}
				
			}
			//$this->contentData['parentPageList'] = $this->brand_model->getParentBrandLists('id, title' ,' parent_id=0',' ORDER BY brand_order ASC');
			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' Brand | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_brand', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function get_parent(){
		$this->brand_model->getParentBrandLists($_GET['id'],$_GET['current_parent_id']);
		exit;
		
	}
	function order(){
		
		$updateStatus = $this->brand_model->changeOrder($_GET['id'],$_GET['brand_order'],$_GET['position']);
		$this->session->set_flashdata('flash_success', 'Brand Order Updated successfully');
		if(isset($_GET['parent']) && $_POST['parent'] != 0)
			redirect(ADMIN_ROOT_URL.'brand/index/'.$_GET['parent']);
		else
			redirect(ADMIN_ROOT_URL.'brand');
								
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['brand_image']['name'];
		$_POST['brand_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_BANNER;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('brand_image'))
		{
			if($this->input->post('action') == 'Edit') {
					if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists(DIR_UPLOAD_BANNER.$_POST['uploaded_file'])){
						unlink(DIR_UPLOAD_BANNER.$_POST['uploaded_file']);
					}
			}
			return TRUE;
		}
		else
		{
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
		
	}
	
	public function index()
	{
		$this->load->library('session');
		$pId = $this->uri->segment(4);
		$parentId =  (isset($pId) && $pId != '') ? $this->uri->segment(4) : 0;
		
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			
			$this->contentData['brandList'] = $this->brand_model->getAllRecords('*' ,'',' ORDER BY id ASC');
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'Brand List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/brand_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
	function model_list(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			$brand_id = $this->uri->segment(4);			
			if($brand_id != '' && $brand_id != 0){			
				
				$brandDetails = $this->brand_model->getDetails($brand_id);
				
				if(isset($brandDetails->id)) {
					$modelList = $this->model_model->getAllRecords('*','brand_id='.$brand_id,'');
					$this->contentData['brandDetails'] = $brandDetails;
					$this->contentData['modelList'] = $modelList;
					$this->headerData['title']= 'Model Model | Admin Module';
					$this->load->view('admin/templates/header', $this->headerData);
					$this->load->view('admin/model_list', $this->contentData);
					$this->load->view('admin/templates/footer', $this->footerData);
				}else{
					redirect(ADMIN_ROOT_URL.'brand');
				}
			}else{
				redirect(ADMIN_ROOT_URL.'brand');
			}
		}
	}
	function add_model(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$brandId =  $this->uri->segment(4);
			if(isset($brandId) && $brandId != 0) {
			$modelId =  $this->uri->segment(5);
				if(isset($modelId) && $modelId != 0) {
				$action = 'Edit';
				if($this->input->post()){
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
								
				$this->form_validation->set_rules('title', 'Title', 'trim|required|callback_model_exist');
					
				if ($this->form_validation->run() == TRUE)
				{
					$insertedId = $this->model_model->updateDetails();
					if($insertedId){
						$this->session->set_flashdata('flash_success', 'Model Details Added successfully');
						redirect(ADMIN_ROOT_URL.'brand/model_list/'.$brandId);
					}
					
				}else{
					$_SESSION = $_POST;				
				}
				
				}
					$modelDetails = $this->model_model->getDetails($modelId);
				$this->contentData['modelDetails'] = $modelDetails;	
			
				}else{
					
				$action = 'Add';
				if($this->input->post()){
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
								
				$this->form_validation->set_rules('title', 'Title', 'trim|required|callback_model_exist');
					
				if ($this->form_validation->run() == TRUE)
				{
					$insertedId = $this->model_model->addDetails();
					if($insertedId){
						$this->session->set_flashdata('flash_success', 'Model Details Added successfully');
						redirect(ADMIN_ROOT_URL.'brand/model_list/'.$brandId);
					}
					
				}else{
					$_SESSION = $_POST;				
				}
				
				}
				$this->contentData['modelDetails'] = array();;	
				}
				$this->contentData['action'] = $action;
				$brandDetails = $this->brand_model->getDetails($brandId);
				$this->contentData['brandDetails'] = $brandDetails;
				$this->headerData['title']= $action.' Model | Admin Module';
				$this->load->view('admin/templates/header', $this->headerData);
				$this->load->view('admin/add_model', $this->contentData);
				$this->load->view('admin/templates/footer', $this->footerData);
			}else{
				redirect(ADMIN_ROOT_URL.'brand');
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}

	}
	function model_exist(){
		$alreadyExist = $this->model_model->checkTitleExist($_POST['title'],$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('model_exist', 'The %s is already exist !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	function brand_exist(){
		$alreadyExist = $this->brand_model->checkTitleExist($_POST['title'],$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('brand_exist', 'The %s is already exist !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	function model_delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$modelId =  $this->uri->segment(4);
				$modelDetails = $this->model_model->getDetails($modelId);
				$this->model_model->deleteRecord($modelId);
				$this->session->set_flashdata('flash_success', 'Model deleted successfully');
				redirect(ADMIN_ROOT_URL.'brand/model_list/'.$modelDetails->brand_id);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function model_status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$modelId =  $this->uri->segment(4);
			$modelDetails = $this->model_model->getDetails($modelId);
			if($modelId == ''){
				redirect(ADMIN_ROOT_URL.'brand');
			}else{
				$this->model_model->changeStatus(0,$modelId);
				$this->session->set_flashdata('flash_success', 'Model Status changed successfully');
				redirect(ADMIN_ROOT_URL.'brand/model_list/'.$modelDetails->brand_id);
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function model_status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$modelId =  $this->uri->segment(4);
			$modelDetails = $this->model_model->getDetails($modelId);
			if($modelId == ''){
				redirect(ADMIN_ROOT_URL.'brand');
			}else{
				$this->model_model->changeStatus(1,$modelId);
				$this->session->set_flashdata('flash_success', 'Model Status changed successfully');
				redirect(ADMIN_ROOT_URL.'brand/model_list/'.$modelDetails->brand_id);
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */