<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Admin_controller.php';

class Classified extends Admin_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	 {
		parent::__construct();

		$this->load->helper('url');
		$this->load->helper('form');
		$this->load->model('admin_model');
		$this->load->model('classified_model');
		$this->load->model('inquiry_model');
		$this->load->model('category_model');
		$this->load->model('brand_model');
		$this->load->model('member_model');
		$this->load->model('image_model');
		$this->load->library('form_validation');
		$this->load->library('session');
		$this->headerData['adminModuleList'] = $this->admin_model->getModuleList();
		$this->headerData['isAdminLogin'] = $this->admin_model->checkAdminLogin();
		$this->headerData['activeAdminDetails'] = $this->admin_model->activeAdminDetails();
		$this->headerData['noneEditPage'] = array('home');
		
	}
	function status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			if($classifiedId == ''){
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}else{
				$this->classified_model->changeStatus(0,$classifiedId);
				$this->session->set_flashdata('flash_success', 'File Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			if($classifiedId == ''){
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}else{
				$this->classified_model->changeStatus(1,$classifiedId);
				$this->session->set_flashdata('flash_success', 'File Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function image_status_inactive(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$imageId =  $this->uri->segment(4);
			$imageDetails = $this->image_model->getDetails($imageId);
			if($imageId == ''){
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}else{
				$this->image_model->changeStatus(0,$imageId);
				$this->session->set_flashdata('flash_success', 'File Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified/image_list/'.$imageDetails->classified_id);
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}		
	}
	function image_status_active(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$imageId =  $this->uri->segment(4);
			$imageDetails = $this->image_model->getDetails($imageId);
			if($imageId == ''){
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}else{
				$this->image_model->changeStatus(1,$imageId);
				$this->session->set_flashdata('flash_success', 'File Status changed successfully');
				redirect(ADMIN_ROOT_URL.'classified/image_list/'.$imageDetails->classified_id);
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		
	}
	function delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			
				$this->classified_model->deleteRecord($classifiedId);
				$this->session->set_flashdata('flash_success', 'File deleted successfully');
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function image_delete(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$imageId =  $this->uri->segment(4);
				$imageDetails = $this->image_model->getDetails($imageId);
				if(isset($imageDetails->classified_image) && $imageDetails->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$imageDetails->classified_image)){
						unlink(DIR_UPLOAD_CLASSIFIED.$imageDetails->classified_image);
				}
				$this->image_model->deleteRecord($imageId);
				$this->session->set_flashdata('flash_success', 'File deleted successfully');
				redirect(ADMIN_ROOT_URL.'classified/image_list/'.$imageDetails->classified_id);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
	function add_image(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			if(isset($classifiedId) && $classifiedId != 0) {
				$action = 'Add';
				if($this->input->post()){
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
								
				if(isset($_FILES['classified_image']) && $_FILES['classified_image']['name']!=''){
					$this->form_validation->set_rules('classified_image', 'Image', 'trim|callback_upload_image');					
				}
					
				if ($this->form_validation->run() == TRUE)
				{
					$insertedId = $this->image_model->addDetails();
					if($insertedId){
						$this->session->set_flashdata('flash_success', 'Image Details Added successfully');
						redirect(ADMIN_ROOT_URL.'classified/image_list/'.$classifiedId);
					}
					
				}else{
					$_SESSION = $_POST;				
				}
				
				}
				$classifiedDetails = $this->classified_model->getDetails($classifiedId);
				$this->contentData['classifiedDetails'] = $classifiedDetails;
				$this->headerData['title']= 'Add Image | Admin Module';
				$this->load->view('admin/templates/header', $this->headerData);
				$this->load->view('admin/add_image', $this->contentData);
				$this->load->view('admin/templates/footer', $this->footerData);
			}else{
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}

	}
	function add(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
			$classifiedId =  $this->uri->segment(4);
			$action = 'Add';
			if($classifiedId == ''){
				$action = 'Add';
				$this->contentData['classifiedDetails'] = array();
				$classifiedCateId = 0;
			}else{
				$action = 'Edit';
				$classifiedDetails = $this->classified_model->getDetails($classifiedId);
				$this->contentData['classifiedDetails'] = $classifiedDetails;
			}
			$this->load->library('ckeditor');
			$this->load->library('ckfinder');
			$this->ckeditor->basePath = base_url().'assets/ckeditor/';
			
			$this->ckeditor->config['language'] = 'en';
			$this->ckeditor->config['width'] = '1000px';
			$this->ckeditor->config['height'] = '300px';            
			
			//Add Ckfinder to Ckeditor
			$this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/'); 

			if($this->input->post()){
				
				$this->load->helper(array('form', 'url'));
				$this->form_validation->set_error_delimiters('<div class="alert alert-danger"><button data-dismiss="alert" class="close" type="button">×</button>', '</div>');
                $this->form_validation->set_rules('title', 'Title', 'xss_clean|trim|required');
                $this->form_validation->set_rules('created_by', 'File uploaded by', 'xss_clean|trim|required');
                $this->form_validation->set_rules('myfile', 'File', 'xss_clean|trim|callback_file_upload');


				if ($this->form_validation->run() == TRUE)
				{
					if($this->input->post('action') == 'Add') {					
						$_POST['classified_slug'] = $this->classified_model->generateClassifiedSlug($this->input->post('title'));
						$_POST['description'] = addslashes($_POST['description']);
						
						$insertedId = $this->classified_model->addDetails();
						
						if($insertedId){
							$this->session->set_flashdata('flash_success', 'File Details Added successfully');
							redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
						}
					}else{
						$updateStatus = $this->classified_model->updateDetails();
						if($updateStatus){
							$this->session->set_flashdata('flash_success', 'File Details Updated successfully');
							redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
						}
					}
				}else{
					$_SESSION = $_POST;	
					$classifiedCateId = $_POST['category_id'];
				}
				
			}
			$memberList = $this->member_model->getAllRecords('id, first_name,last_name','is_active = "1"');
			$countryList = $this->admin_model->getCountryList();
			$this->contentData['countryList'] = $countryList;
			$this->contentData['memberList'] = $memberList;

			$this->contentData['action'] = $action;
			$this->headerData['title']= $action.' File | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/add_classified', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
			
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
	}
    function file_upload(){
        if (!empty($_FILES['file_name']['name'])) {
            $uploaded_path_parts = pathinfo($_FILES['file_name']['name']);
            $config['file_name'] = date('dmYHis') . '.' . $uploaded_path_parts['extension'];
            $counter = 100;
            $created_by = $this->input->post('created_by');
            if (!empty($created_by) && is_numeric($created_by)) {
                $userDetails = $this->member_model->getDetails($created_by);
                $userEmail = !empty($userDetails->email) ? $userDetails->email : '';
            } else {
                $this->form_validation->set_message('file_upload', 'No/Invalid user');
                return false;
            }

            $uploadPath = DIR_UPLOAD_FILE;
            if (!file_exists(DIR_UPLOAD_FILE . $userEmail) OR !is_dir(DIR_UPLOAD_FILE . $userEmail)) {
                mkdir(DIR_UPLOAD_FILE . $userEmail);
                $uploadPath = DIR_UPLOAD_FILE . $userEmail . '/';
            } else {
                $uploadPath = DIR_UPLOAD_FILE . $userEmail . '/';
            }

            while (file_exists($uploadPath . $config['file_name'])) {
                $counter++;
                $config['file_name'] = date('dmYHis') . '_' . $counter . '.' . $uploaded_path_parts['extension'];
            }

            $_POST['file_name'] = $config['file_name'];
            $config['upload_path'] = $uploadPath;
            $config['allowed_types'] = ALLOWED_FILE_TYPES;
            $config['max_size'] = MAX_FILE_SIZE;
            $this->load->library('upload');
            $this->upload->initialize($config);
            if ($this->upload->do_upload('file_name')) {
                if ($this->input->post('action') == 'Edit') {
                    if (isset($_POST['uploaded_file']) && $_POST['uploaded_file'] != '' && file_exists($uploadPath . $_POST['uploaded_file'])) {
                        unlink($uploadPath . $_POST['uploaded_file']);
                    }
                }
                return TRUE;
            } else {
                $this->form_validation->set_message('file_upload', $this->upload->display_errors());
                return FALSE;
            }
        }
    }
	function order(){
		
		$updateStatus = $this->classified_model->changeOrder($_GET['id'],$_GET['classified_order'],$_GET['position']);
		$this->session->set_flashdata('flash_success', 'File Order Updated successfully');

        redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
	}
	function upload_image(){
		$config['file_name'] = date('dmYHis').'_'.$_FILES['classified_image']['name'];
		$_POST['classified_image'] = $config['file_name'];
		$config['upload_path'] = DIR_UPLOAD_CLASSIFIED;
		$config['allowed_types'] = IMAGE_ALLOWED_TYPES;
		$config['max_size']	= MAX_BANNER_IMAGE_SIZE;				
		$this->load->library('upload', $config);		
		if ($this->upload->do_upload('classified_image')) {
			return TRUE;
		} else {
			$this->form_validation->set_message('upload_image', $this->upload->display_errors());
			return FALSE;
		}
	}
	function image_list(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			$classified_id = $this->uri->segment(4);			
			if($classified_id != '' && $classified_id != 0){			
				
				$classifiedDetails = $this->classified_model->getDetails($classified_id);
				
				if(isset($classifiedDetails->id)) {
					$imageList = $this->image_model->getAllRecords('*','classified_id='.$classified_id,'');
					$this->contentData['classifiedDetails'] = $classifiedDetails;
					$this->contentData['imageList'] = $imageList;
					$this->headerData['title']= 'File Image | Admin Module';
					$this->load->view('admin/templates/header', $this->headerData);
					$this->load->view('admin/image_list', $this->contentData);
					$this->load->view('admin/templates/footer', $this->footerData);
				}else{
					redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
				}
			}else{
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}
		}
	}
	
	function inquiry_list(){
		if($this->headerData['activeAdminDetails']->module_access[0] == 'FULL' || in_array('3', $this->headerData['activeAdminDetails']->module_access)){
		
		}else{
			redirect(ADMIN_ROOT_URL.'no_access');
		}
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			$classified_id = $this->uri->segment(4);			
			if($classified_id != '' && $classified_id != 0){			
				
				$classifiedDetails = $this->classified_model->getDetails($classified_id);
				
				if(isset($classifiedDetails->id)) {
					$inquiryList = $this->inquiry_model->getAllRecords('*','classified_id='.$classified_id);
					$this->contentData['classifiedDetails'] = $classifiedDetails;
					$this->contentData['inquiryList'] = $inquiryList;
					$this->headerData['title']= 'File Inquiry | Admin Module';
					$this->load->view('admin/templates/header', $this->headerData);
					$this->load->view('admin/inquiry_list', $this->contentData);
					$this->load->view('admin/templates/footer', $this->footerData);
				}else{
					redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
				}
			}else{
				redirect(ADMIN_ROOT_URL.'classified'.($this->session->userdata('file_owner_id') ? '/index/mem-'.$this->session->userdata('file_owner_id') : ''));
			}
		}
	}
	function email_exist($email){
		$alreadyExist = $this->admin_model->checkEmailExist($email,$_POST['id']);
		if(count($alreadyExist) > 0){
			$this->form_validation->set_message('email_exist', 'The %s is already registered !!!');
			return FALSE;
		}else{
			
			return TRUE;
		}
	}
	function inquiry_reply(){
		if($this->input->post()){
			$inquiryReply = $this->inquiry_model->replyInquiry();
			$inquiryDetails = $this->inquiry_model->getDetails($this->input->post('inquiryId'));
			$memberDetails = $this->member_model->getDetails($this->input->post('replied_member_id'));
			$classifiedDetails = $this->classified_model->getDetails($this->input->post('classifiedId'));
			$subject = 'Reply of your inquiry on '.$classifiedDetails->title;
			$content = '';
			$content .= $this->admin_model->emailHeader();
			$content .= $this->input->post('reply_text');
			$content .= $this->admin_model->emailFooter();
			$email = $this->admin_model->sendEmail($inquiryDetails->inquiry_name, $memberDetails->first_name.' '.$memberDetails->last_name, $inquiryDetails->inquiry_email , $memberDetails->email, $subject, $content);
			if($email){
				echo 'replied';
			}else{
				echo 'error';
			}
		}else{
			echo 'error';
		}
		exit;
		
	}
	public function index()
	{
		$this->load->library('session');
		$uid_string = $this->uri->segment(4);
        $file_owner_id = !empty($uid_string) ? str_replace('mem-', '', $uid_string) : '';
        if (!empty($file_owner_id)) {
            $this->contentData['file_owner_id'] = !empty($file_owner_id) ? $file_owner_id : '';
            $data = array('file_owner_id' => $this->contentData['file_owner_id']);
            $this->session->set_userdata($data);
        } else {
            $this->session->unset_userdata('file_owner_id');
            $this->contentData['file_owner_id'] = '';
            //$file_owner_id = $this->session->userdata('file_owner_id') ? $this->session->userdata('file_owner_id') : '';
        }
		
		if($this->session->userdata('admin_id')==''){
			redirect(ADMIN_ROOT_URL.'login');
		}else{
			if (!empty($this->contentData['file_owner_id'])) {
                $this->contentData['owner_details'] = $this->member_model->getDetails($this->contentData['file_owner_id']);
            }
			$this->contentData['classifiedList'] = $this->classified_model->getAllFileList($this->contentData['file_owner_id']);
			$succ_msg = $this->session->flashdata('flash_success');
			$err_msg = $this->session->flashdata('flash_error');
			if(isset($succ_msg) && $succ_msg != ''){				
				$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
			}
			if(isset($err_msg) && $err_msg != ''){				
				$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
			}
			$this->headerData['title']= 'File List | Admin Module';
			$this->load->view('admin/templates/header', $this->headerData);
			$this->load->view('admin/classified_list', $this->contentData);
			$this->load->view('admin/templates/footer', $this->footerData);
		}
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */