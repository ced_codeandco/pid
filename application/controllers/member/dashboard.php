<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Member_controller.php';
class Dashboard extends Member_controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/welcome
     *	- or -
     * 		http://example.com/index.php/welcome/index
     *	- or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
    public $headerData;
    public $contentData;
    public $footerData;

    const CLASSIFIED_LIST_COUNT_PER_PAGE = 10;
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Function to render forgot password page and to process
     */
    function forgot_password(){

        if($this->input->post()){
            $isLogin = $this->member_model->getDetailsFromEmail($this->input->post('username'));
//var_dump($isLogin); die();
            if(!empty($isLogin)){
                $this->load->library('emailclass');
                $message = "<p>Dear ".$isLogin->first_name.",<br/><br/>Please use the below link to update your password</p>";
                $passwordToken = $this->member_model->setPasswordToken($isLogin->id);
                $message .= '<p><a target="_blank" href="'.MEMBER_ROOT_URL."reset_password/".$passwordToken.'">'.MEMBER_ROOT_URL."reset_password/".$passwordToken."</a></p>";
                $subject = 'Password recovery link for  '.SITE_NAME;
                $content = '';
                $content .= $this->emailclass->emailHeader();
                $content .= $message;
                $content .= $this->emailclass->emailFooter();

                $email = $this->emailclass->send_mail($isLogin->email, $subject, $content);
                if($email){
                    $this->session->set_flashdata('flash_success', 'Email sent successfully !! Please check your inbox.');
                    $this->contentData['succMsg'] = 'Email sent successfully !! Please check your inbox.';
                    redirect(ROOT_URL.'login', 'refresh');
                } else {
                    $this->session->set_flashdata('flash_success', 'Email Error Please try again later !!!!');
                    $this->contentData['errMsg'] = 'Email Error Please try again later !!!!';
                }
            }else{
                $this->contentData['errMsg'] = 'Invalid Email Address';
            }
        }
        if($this->session->userdata('id')==''){

            $this->headerData['title']= 'Forgot Password';
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('member/forgot_password', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }else{
            redirect(MEMBER_ROOT_URL, 'refresh');
        }


    }

    public function login()
    {
        $wrapperType = $this->uri->segment(2);
        $redirectUrl = ROOT_URL.(!empty($_GET['back']) ? base64_decode($_GET['back']) : 'submit_file');
        if($this->input->post()){
            $isLogin = $this->member_model->memberLogin();
            if($isLogin != 0){
                $memberDetails = $this->member_model->getMemberDetails($isLogin);
                $data = array(
                    'display_name'  => $memberDetails->first_name.' '.$memberDetails->last_name,
                    'id'            => $memberDetails->id,
                    'email'         => $memberDetails->email,
                    'is_logged_in'  => true,
                    'is_member'     => true
                );
                $this->session->set_userdata($data);
                if ($wrapperType == 'ajax') {
                    echo "<script>window.top.location.reload();</script>";
                } else {
                    redirect($redirectUrl);
                }
            }else{
                $this->contentData['errMsg'] = 'Invalid Email or Password';
            }
        }
        if ($this->session->userdata('id')=='') {

            $succ_msg = $this->session->flashdata('flash_success');
            $err_msg = $this->session->flashdata('flash_error');
            if (isset($succ_msg) && $succ_msg != '') {
                $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
            }
            if (isset($err_msg) && $err_msg != '') {
                $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
            }
            $this->contentData['questionList'] = $this->admin_model->getRandomSecurityQuestions();
            $this->headerData['title']= 'Login';
            $this->headerData['active_tab']= 'login';
            $this->contentData['privacy_popup_content'] = $this->cms_model->getDetails(CMS_PRIVACY_POPUP_PAGE_ID);
            $this->contentData['terms_popup_content'] = $this->cms_model->getDetails(CMS_TERMS_POPUP_PAGE_ID);
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('login', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        } else {
            $member_id = $this->session->userdata('id');
            $memberDetails = $this->member_model->getMemberDetails($member_id);
            //if (empty($memberDetails->))
            redirect($redirectUrl, 'refresh');
        }
    }

    public function facebookLogin()
    {
        $response = array('status' => 0, 'message' => 'Something went wrong! Please try again later', 'first_login' => 0 );
        if($this->input->post()){
            $data = $this->input->post();
            if (!empty($data['socialMedia']) && $data['socialMedia'] == 'facebook') {
                $isMember = $this->member_model->getAllRecords('*', ' email="'.$data['email'].'" ');
            } else if (!empty($data['socialMedia']) && $data['socialMedia'] == 'linkedIn') {
                $isMember = $this->member_model->getAllRecords('*', ' email="'.$data['email'].'" ');
            }


            if (!$isMember) {
                $memberData = array(
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'social_media_type' => $data['socialMedia'],
                    'registration_ip' => $this->input->ip_address(),
                    'is_active' => '1',
                    'email' => $data['email'],
                    'created_date' => date('Y-m-d H:i:s')
                );
                if (!empty($data['socialMedia']) && $data['socialMedia'] == 'facebook') {
                    $memberData['facebook_id'] = $data['socialMediaId'];
                    $memberData['is_facebook_login'] = '1';
                } else if (!empty($data['socialMedia']) && $data['socialMedia'] == 'linkedIn') {
                    $memberData['linkedin_id'] = $data['socialMediaId'];
                    $memberData['is_linked_in_login'] = '1';
                }
                $user_id = $this->member_model->addDetails(false, $memberData);
                $memberDetails = $this->member_model->getMemberDetails($user_id);
                $response['first_login'] = 1;
                $this->member_model->sendAdminNotification($user_id);
            } else {
                $memberDetails = array_shift($isMember);
                $user_id = $memberDetails->id;
                $memberData = array(
                    'is_facebook_login' => (!empty($data['socialMedia']) && $data['socialMedia'] == 'facebook') ? '1' : '0',
                    'is_linked_in_login' => (!empty($data['socialMedia']) && $data['socialMedia'] == 'linkedIn') ? '1' : '0',
                    'social_media_type' => $data['socialMedia'],
                    'first_name' => $data['first_name'],
                    'last_name' => $data['last_name'],
                    'last_login_ip' => $this->input->ip_address(),
                    'is_active' => '1',
                    'email' => $data['email'],
                    'updated_date' => date('Y-m-d H:i:s'),
                    'last_login_date' => date('Y-m-d H:i:s'),
                );
                if (!empty($data['socialMedia']) && $data['socialMedia'] == 'facebook') {
                    $memberData['facebook_id'] = $data['socialMediaId'];
                } else if (!empty($data['socialMedia']) && $data['socialMedia'] == 'linkedIn') {
                    $memberData['linkedin_id'] = $data['socialMediaId'];
                }
                $this->member_model->updateDetails($user_id, $memberData);

                //last_login_date
                //$memberDetails = $this->member_model->getMemberDetails($user_id);

            }

            if (!empty($memberDetails)) {
                $data = array(
                    'display_name'  => $data['first_name'],
                    'id'            => $user_id,
                    'email'         => '',
                    'is_logged_in'  => true,
                    'is_member'     => true,
                );

                $this->session->set_userdata($data);
                if (!empty($user_id)) {
                    $response['status'] = 1;
                    $response['message'] = 'Welcome back to '.SITE_NAME;
                    $response['userId'] = $user_id;
                }
            }
        }
        echo json_encode($response);
    }

    function logout(){
        $logout=$this->member_model->memberLogout();
        if($logout == TRUE)	{
            $this->session->unset_userdata('display_name');
            $this->session->unset_userdata('id');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('module_access');
            $this->session->unset_userdata('is_logged_in');
            $this->session->unset_userdata('is_member');

            redirect(ROOT_URL);
        }
    }

    public function verify_email()
    {
        $emailToken = $this->uri->segment(3);
        $this->contentData['emailToken'] = $emailToken;

        if($emailToken && !empty($emailToken) && $this->member_model->isValidEmailToken($emailToken)) {
            //Make email verified
            $fieldValueArray = array('emailVerified' => '1', 'is_active' => '1', 'emailToken' => '');
            $this->member_model->verifyEmail($emailToken, $fieldValueArray);

            $this->session->set_flashdata('flash_success', 'Your email verified successfully!!');
            //redirect to login
            redirect(ROOT_URL.'login');
        } else {
            $this->contentData['validToken'] = $this->member_model->isValidEmailToken($emailToken);
            if (!$this->contentData['validToken']) {
                $this->contentData['errMsg'] = 'This link seems to be expired or invalid. Please try again';
            }
            $this->headerData['title']= 'Verify email';
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('verify_email', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
    }

    public function reset_password()
    {
        $password_token = $this->uri->segment(3);
        $this->contentData['password_token'] = $password_token;

        if($password_token && !empty($password_token)) {
            $user_id = $this->member_model->isValidPasswordToken($password_token);
            if($this->input->post()){
                $this->load->helper(array('form', 'url'));
                $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
                $this->form_validation->set_rules('password', 'Password', 'trim|required');
                $this->form_validation->set_rules('retype_password', 'Password again', 'trim|required');

                if ($this->form_validation->run() == TRUE)
                {
                    $isLogin = $this->member_model->updatePassword($user_id, $password_token);
                    $this->session->set_flashdata('flash_success', 'Password updated successfully');
                    redirect(ROOT_URL.'login', 'refresh');
                    exit(0);
                }
            }
            if($this->session->userdata('id')==''){
                $this->contentData['validToken'] = !empty($user_id);
                if (!$this->contentData['validToken']) {
                    $this->contentData['errMsg'] = 'Invalid link. Please try again';
                }
                $this->headerData['title']= 'Reset Password';
                $this->load->view('templates/header', $this->headerData);
                $this->load->view('member/reset_password', $this->contentData);
                $this->load->view('templates/footer', $this->footerData);
            }else{
                redirect(MEMBER_ROOT_URL, 'refresh');
            }
        }else{

            redirect(MEMBER_ROOT_URL, 'refresh');
        }
    }

    /**
     * Function to save search criteria called from client search page via AJAX
     */
    public function save_user_search()
    {
        $return = array();
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_rules('queryString', 'Search criteria', 'xss_clean|trim|required|valid_email');
        if($this->form_validation->run() === FALSE)
        {
            $this->load->model('saved_search_model');
            if ($this->saved_search_model->getDetailsFromSearchQuery(urldecode($this->input->post('search_query')))) {
                $return = array('status' => 0, 'message' => 'You already saved this search');
            } else {
                $this->saved_search_model->addDetails($this->user_id);
                $return = array('status' => 1, 'message' => 'Saved your search');
            }
        }
        else
        {
            $return = array('status' => 0, 'message' => '');
        }
        echo json_encode($return);
    }

    /**
     * Action to display member's adds based on logged in user id
     */
    public function my_files()
    {
        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        $condition = ' created_by="'.$this->user_id.'" ';
        $order = ' order by created_date_time DESC ';
        $getTotalRecords = true;
        $limit_start = ($this->input->get('per_page') ? $this->input->get('per_page') : '0');
        $limit = ' LIMIT '.$limit_start.', '.self::CLASSIFIED_LIST_COUNT_PER_PAGE ;
        $records = $this->classified_model->getAllRecords('*', $condition, $order, $limit, $getTotalRecords);

        $this->contentData['classifiedList'] = $records['result'];
        $this->contentData['recordCountStart'] = ++$limit_start;
        $searchResultTotal = $records['total_row_count'];
        $searchCriteria = array();
        $base_url = MEMBER_ROOT_URL.'my_files?';
        $this->contentData['paginator'] = $this->classified_model->build_search_paginator($searchResultTotal, self::CLASSIFIED_LIST_COUNT_PER_PAGE, $searchCriteria, $base_url);

        $this->load->view('templates/header', $this->headerData);
        $this->contentData['tabData'] = $this->member_model->buildTabData('my_files');
        $this->load->view('my_files', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    public function delete_classifieds($classifiedId)
    {
        if (!empty($classifiedId)) {
            $this->load->model(array('package_model'));
            $classifiedDetails = $this->classified_model->getDetails($classifiedId);
            $this->classified_model->deleteRecord($classifiedId, $this->user_id);
//var_dump($classifiedDetails);
            if (!empty($classifiedDetails->file_status) && $classifiedDetails->file_status != 2) {
                $this->package_model->increment_file_count(1, $this->user_id);
            }

            $this->session->set_flashdata('flash_success', 'Your file removed successfully!!');
        } else {
            $this->session->set_flashdata('flash_error', 'Something went wrong!! <br />Your Ad was not removed!');
        }
        redirect(MEMBER_ROOT_URL.'my_files');
    }

    public function my_searches()
    {
        $this->load->model('saved_search_model');
        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        $condition = ' user_id="'.$this->user_id.'" ';
        $order = ' order by created_date DESC ';
        $getTotalRecords = true;
        $limit_start = ($this->input->get('per_page') ? $this->input->get('per_page') : '0');
        $limit = ' LIMIT '.$limit_start.', '.self::CLASSIFIED_LIST_COUNT_PER_PAGE ;
        $records = $this->saved_search_model->getAllRecords('*', $condition, $order, $limit, $getTotalRecords);

        $this->contentData['searchList'] = $records['result'];
        $this->contentData['recordCountStart'] = ++$limit_start;
        $searchResultTotal = $records['total_row_count'];
        $searchCriteria = array();
        $base_url = MEMBER_ROOT_URL.'my_searches?';
        $this->contentData['paginator'] = $this->classified_model->build_search_paginator($searchResultTotal, self::CLASSIFIED_LIST_COUNT_PER_PAGE, $searchCriteria, $base_url);


        $this->load->view('templates/header', $this->headerData);
        $tabData = $this->member_model->buildTabData('my_searches');
        $this->load->view('templates/member_tab', $tabData);
        $this->load->view('my_searches', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }

    /**
     * Function to delete saved search
     */
    public function delete_saved_search($savedSearchId)
    {
        if (!empty($savedSearchId)) {
            $this->load->model('saved_search_model');
            $this->saved_search_model->deleteRecord($savedSearchId);
            $this->session->set_flashdata('flash_success', 'Your search removed successfully!!');
        } else {
            $this->session->set_flashdata('flash_error', 'Something went wrong!! <br />Your search was not removed!');
        }
        redirect(MEMBER_ROOT_URL.'my_searches');
    }

    public function my_profile()
    {
        $this->load->model('saved_search_model');
        $this->contentData['tabData'] = $this->member_model->buildTabData('my_profile');
        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');

        //$formData
        $user_id = !empty($this->user_id) ? $this->user_id : false;

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_message('is_unique', '%s is already in use.');

        $this->form_validation->set_rules('first_name', 'First Name', 'xss_clean|trim|required');
        $this->form_validation->set_rules('last_name', 'Last Name', 'xss_clean|trim|required');

        $this->form_validation->set_rules('date_of_birth', 'Date of Birth', 'xss_clean|trim|required');
        $this->form_validation->set_rules('city', 'City', 'xss_clean|trim');
        $this->form_validation->set_rules('subscribe_site_update', 'subscribe_site_update', 'xss_clean|trim');
        $this->form_validation->set_rules('subscribe_offers', 'subscribe_offers', 'xss_clean|trim');


        if ($this->form_validation->run()) {
            $dateOfBirthField = strtotime($this->input->post('date_of_birth'));
            $dob = date('Y-m-d', $dateOfBirthField);
            $_POST['date_of_birth'] = $dob;
            $data = $this->input->post();
            //Register
            $this->member_model->updateDetails($this->user_id, $data);
            $this->session->set_flashdata('flash_success', 'Profile updated successfully!!');

            redirect(MEMBER_ROOT_URL.'my_profile');
        } else {
            $this->contentData['profileData'] = $this->headerData['isMemberLogin'];
            $this->contentData['formData'] = (array)$this->headerData['isMemberLogin'];
            $this->contentData['countryList'] = $this->admin_model->getCountryList();
            //$this->contentData['businessList'] = $this->admin_model->getBusinessList();
            //$this->contentData['dashboardNotification'] = $this->member_model->getDashboardCounts($this->user_id);
            $this->contentData['cityList'] = $this->admin_model->getCityList();
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('my_profile', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
    }

    public function _edit_unique($email, $userId = false)
    {
        $this->load->model('member_model');

        if ($this->member_model->is_duplicate_email($email, $userId)) {
            $this->form_validation->set_message('_edit_unique', 'The %s you entered is already in use');

            return FALSE;
        }

        return true;
    }

    public function _check_email_changed($email, $userId)
    {
        $this->load->model('member_model');
        $userDetails = $this->member_model->getDetails($userId);
        if ($userDetails->email == $email) {
            $this->form_validation->set_message('_check_email_changed', 'The %s you entered same as old email id');

            return FALSE;
        }

        return true;
    }
    public function _validate_password($password, $userId)
    {
        $this->load->model('member_model');

        if (!$this->member_model->is_password_valid($password, $userId)) {
            $this->form_validation->set_message('_validate_password', 'The %s you entered is wrong');

            return FALSE;
        }

        return true;
    }

    public function account_settings()
    {
        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');

        $formAction = $this->input->post('formAction');
        if ($this->member_model->validate_account_forms($formAction, $this->user_id)) {

            redirect(MEMBER_ROOT_URL.'change_password');
        } else {
            $this->contentData['profileData'] = $this->headerData['isMemberLogin'];
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('account', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
    }

    public function change_password()
    {
        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');

        $formAction = $this->input->post('formAction');
        if ($this->member_model->validate_account_forms($formAction, $this->user_id)) {

            redirect(MEMBER_ROOT_URL.'change_password');
        } else {
            $this->contentData['profileData'] = $this->headerData['isMemberLogin'];
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('change_password', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
    }

    public function details($classified_slug = '')
    {
        if (!empty($classified_slug)) {
            $back = $this->input->get('back');
            $classifiedDetails = $this->classified_model->prepareClassifiedSearch($classified_slug, $back);
            $this->load->model('brand_model');
            $this->load->model('model_model');
            $this->load->model('inquiry_model');

            if ($this->inquiry_model->validate_inquiry_forms()) {
                $this->session->set_flashdata('flash_success', 'Your enquiry submitted successfully !!');
                $redirectUrl = getCurrentUrl();

                redirect($redirectUrl);
            } else {

                $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
                $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
                //$back
                $searchCriteria = $this->classified_model->getSearchCriteriaFromBack($back);
                if (!empty($searchCriteria['brand'])) {
                    $brandDetails = $this->brand_model->getDetailsFromTitle($searchCriteria['brand']);
                    if ($brandDetails && count($brandDetails) > 0) {
                        $this->contentData['modelDetails'] = $this->model_model->getAllRecords('id,title', " brand_id=" . $brandDetails->id);
                    }
                }
                $this->contentData['inquiry_member_id'] = $this->user_id;
                $this->contentData['searchCriteria'] = $searchCriteria;
                $this->contentData['classifiedDetails'] = $classifiedDetails['classifiedDetails'];
                $this->contentData['classifiedImages'] = $classifiedDetails['classifiedImages'];
                $this->contentData['userDetails'] = $this->headerData['isMemberLogin'];

                $this->contentData['cityList'] = $this->admin_model->getCityList();
                $this->contentData['brandList'] = $this->brand_model->getAllRecords('id,title', " is_active='1'");
                $this->load->view('templates/header', $this->headerData);
                $this->load->view('details', $this->contentData);
                $this->load->view('templates/footer', $this->footerData);
            }
        }
    }

    /**
     * Function to create a classifieds
     */
    function create_classified($classifiedId = 0){

        $this->load->model(array('package_model'));
        if (!$this->is_logged_in OR !$this->is_member) {
            $this->session->set_flashdata('flash_error', 'Please Login/Register to place an add!!');
            redirect(ROOT_URL.'login');
        }

        $this->contentData['succMsg'] = $this->session->flashdata('flash_success');
        $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
        $this->contentData['user_id'] = $this->user_id;

        $editClassifieds = false;
        if(empty($classifiedId)){
            $this->contentData['action'] = 'Post a free ad';
            $this->contentData['classifiedDetails'] = array();
            $classifiedCateId = 0;
            $editClassifieds = false;
        } else {
            $this->contentData['action'] = 'Edit your ad';
            $editClassifieds = true;
        }
        $this->contentData['editClassifieds'] = $editClassifieds;
        if ($this->classified_model->validate_add()) {
            $files_moved = $this->input->post('files_moved');
            if (!empty($files_moved) && is_array($files_moved)) {
                $file_count = 0;
                foreach ($files_moved as $file) {
                    $data['created_by'] = $this->user_id;
                    $data['title'] = $file['title'];
                    $data['file_name'] = $file['file'];
                    $data['classified_ip'] = $this->input->ip_address();
                    $data['created_date_time'] = date('Y-m-d H:i:s');
                    $this->classified_model->addDetails($data);
                    $file_count++;
                }
                $this->member_model->sendUploadEmailToAdmin($this->user_id, $file_count);
                //Update member table, decrement file count,
                $this->package_model->update_file_count($file_count, $this->user_id);
            }

            $this->session->set_flashdata('flash_success', 'Your file uploaded successfully!!');


            redirect(MEMBER_ROOT_URL.'my_files');
        } else {
            if ($editClassifieds) {
                $classifiedDetails = $this->classified_model->getDetails($classifiedId);
                $this->contentData['classifiedDetails'] = (array) $classifiedDetails;

            } else {
                $this->contentData['classifiedDetails'] = $_POST;
            }

//            $this->contentData['package_limits'] = $this->package_model->get_active_package_details($thi
            $this->contentData['package_limits'] = $this->package_model->get_active_package_limits($this->user_id);
            $this->contentData['active_package_details'] = $this->package_model->getDetails($this->headerData['activeMemberDetails']->active_package_id);


            $this->load->model('brand_model');
            $this->contentData['editClassifieds'] = $editClassifieds;

            $this->load->view('templates/header', $this->headerData);
            $this->load->view('member/create', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
    }
    function file_exists($input, $name) {

        $imageArray = $this->classified_model->move_cropped_files();
        if (empty($imageArray) && empty($_POST['files_moved'])) {
            $this->form_validation->set_message('file_exists', '%s upload failed');
            $_POST['files_moved'] = false;
            return false;
        } else if (!empty($imageArray)) {
            //print_r($imageArray);
            $_POST['files_moved'] = $imageArray;
        }

        return true;
    }

    function file_upload(){
        $uploaded_path_parts = pathinfo($_FILES['myfile']['name']);
        $config['file_name'] = date('dmYHis').'.'.$uploaded_path_parts['extension'];
        $counter = 100;
        $userEmail = !empty($this->headerData['isMemberLogin']->email) ? $this->headerData['isMemberLogin']->email : '';
        $uploadPath = DIR_UPLOAD_FILE;
        if (!file_exists(DIR_UPLOAD_FILE.$userEmail) OR !is_dir(DIR_UPLOAD_FILE.$userEmail)) {
            mkdir(DIR_UPLOAD_FILE.$userEmail);
            $uploadPath = DIR_UPLOAD_FILE.$userEmail.'/';
        } else {
            $uploadPath = DIR_UPLOAD_FILE.$userEmail.'/';
        }

        while (file_exists($uploadPath.$config['file_name'])) {
            $counter++;
            $config['file_name'] = date('dmYHis').'_'.$counter.'.'.$uploaded_path_parts['extension'];
        }

        $_POST['file_name'] = $config['file_name'];
        $config['upload_path'] = $uploadPath;
        $config['allowed_types'] = ALLOWED_FILE_TYPES;
        $config['max_size']	= MAX_FILE_SIZE;
        $this->load->library('upload');
        $this->upload->initialize($config);
        if ($this->upload->do_upload('myfile'))
        {
            if($this->input->post('action') == 'Edit') {
                if(isset($_POST['uploaded_file']) && $_POST['uploaded_file']!='' && file_exists($uploadPath.$_POST['uploaded_file'])){
                    unlink($uploadPath.$_POST['uploaded_file']);
                }
            }
            return TRUE;
        } else {
            $this->form_validation->set_message('file_upload', $this->upload->display_errors());
            return FALSE;
        }
    }

    public function no_access(){
        $this->headerData['title']= 'Access Denied';
        $this->load->view('member/templates/header', $this->headerData);
        $this->load->view('member/no_access', $this->contentData);
        $this->load->view('member/templates/footer', $this->footerData);
    }

    public function upload_file() {
        $action = !empty($_GET['action']) ? $_GET['action'] : '';//=delete
        $file = !empty($_GET['file']) ? $_GET['file'] : '';//=delete
        if ($action == 'delete' && !empty($file)) {
            if (file_exists(TEMP_DIR_UPLOAD_FILE.$file)) {
                unlink(TEMP_DIR_UPLOAD_FILE.$file);
            }
            $response = array('status' => 1, 'message' => 'File deleted successfully');
            echo json_encode($response);
            exit;
        }
        //print_r($_FILES);
        $response = array('status' => 0, 'message' => 'Something went wrong');
        if (!empty($_FILES['files']['name'])) {

            /*$file_parts = pathinfo($_FILES['files']['name']);
            $file_name = preg_replace('/[^A-Za-z0-9]/', '_', $file_parts['filename']);
            $file_name = preg_replace('/_+/', '_', $file_name);
            $config['file_name'] = $file_name . 'xxx.' . $file_parts['extension'];*/
            $counter = 0;
            /*while (file_exists($config['upload_path'] . $config['file_name'])) {
                $counter++;
                $config['file_name'] = $file_name . '_' . $counter . '.' . $file_parts['extension'];
            }*/


            $config['upload_path'] = TEMP_DIR_UPLOAD_FILE;
            $config['allowed_types'] = ALLOWED_FILE_TYPES;
            $config['max_size'] = MAX_FILE_SIZE;
            $this->load->library('upload', $config);
            if ($upload_data = $this->manage_multi_upload('files', $config)) {
                $upload_data = array_shift($upload_data);
                //print_r($upload_data);
            //if ($this->upload->do_upload('files')) {
                //print_r($this->upload->get_multi_upload_data());
                $response['status'] = 1;
                $response['message'] = 'File uploaded successfully';
                $response['uploaded_name'] = !empty($upload_data['file_name']) ? $upload_data['file_name']  : '';
                $response['original_file_name'] = !empty($upload_data['client_name']) ? $upload_data['client_name']  : '';
                $response['delete_url'] = !empty($upload_data['file_name']) ? "upload_file?action=delete&file=$upload_data[file_name]"  : '';
                $response['file_url'] = !empty($upload_data['file_name']) ? TEMP_DIR_UPLOAD_FILE_SHOW."$upload_data[file_name]"  : '';

            } else {
                $response['message'] = strip_tags($this->upload->display_errors());
                //$response['message'] .= " - ".TEMP_DIR_UPLOAD_FILE;
            }
        }

        echo json_encode($response);
    }

    function manage_multi_upload($name, $config)
    {
        //print_r($config);
        $upload_data = false;
        $success = false;
        $this->load->library('upload');

        $files = $_FILES;
        $cpt = count($_FILES[$name]['name']);
        for($i=0; $i<$cpt; $i++)
        {
            $_FILES[$name]['name']= $files[$name]['name'][$i];
            $_FILES[$name]['type']= $files[$name]['type'][$i];
            $_FILES[$name]['tmp_name']= $files[$name]['tmp_name'][$i];
            $_FILES[$name]['error']= $files[$name]['error'][$i];
            $_FILES[$name]['size']= $files[$name]['size'][$i];

            $file_parts = pathinfo($_FILES['files']['name']);
            $file_name = preg_replace('/[^A-Za-z0-9]/', '_', $file_parts['filename']);
            $file_name = preg_replace('/_+/', '_', $file_name);
            $config['file_name'] = $file_name . '.' . $file_parts['extension'];
            $config['upload_path'] = TEMP_DIR_UPLOAD_FILE;
            $config['allowed_types'] = ALLOWED_FILE_TYPES;
            $config['max_size'] = MAX_FILE_SIZE;

            $this->upload->initialize($config);
            $success = $this->upload->do_upload('files');
            $upload_data[] = $this->upload->data();
        }

        return $success ? $upload_data : false;
    }

    public function package()
    {
        $this->load->view('templates/header', $this->headerData);
        //$this->load->view('member/package', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */