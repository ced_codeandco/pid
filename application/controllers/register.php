<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . 'libraries/Public_controller.php';

class Register extends Public_controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 public $headerData;
	 public $contentData;
	 public $footerData;
	 public function __construct()
	{
		parent::__construct();
	}

    public function register_via_linkedin() {
        $this->contentData['privacy_popup_content'] = $this->cms_model->getDetails(CMS_PRIVACY_POPUP_PAGE_ID);
        $this->contentData['terms_popup_content'] = $this->cms_model->getDetails(CMS_TERMS_POPUP_PAGE_ID);
        $this->load->view('templates/header', $this->headerData);
        $this->load->view('register_via_linkedin', $this->contentData);
        $this->load->view('templates/footer', $this->footerData);
    }
	public function index()
	{
		$this->load->library('session');
        $this->load->model('cms_model');
        if ($this->is_logged_in && !empty($this->is_member)) {
            redirect(ROOT_URL);
            exit;
        }
		$this->headerData['title']= 'Member::Register';
		$succ_msg = $this->session->flashdata('flash_success');
		$err_msg = $this->session->flashdata('flash_error');
		if(isset($succ_msg) && $succ_msg != ''){				
			$this->contentData['successMsg'] = $this->session->flashdata('flash_success');				
		}
		if(isset($err_msg) && $err_msg != ''){				
			$this->contentData['errMsg'] = $this->session->flashdata('flash_error');				
		}

        if ($this->member_model->validate_registration()) {
            //$dob = DateTime::createFromFormat('d M Y', $this->input->post('date_of_birth'));
            //$_POST['date_of_birth'] = $dob->format("Y-m-d");
            $dateOfBirthField = strtotime($this->input->post('date_of_birth'));
            $dob = date('Y-m-d', $dateOfBirthField);
            $_POST['date_of_birth'] = $dob;
            //Register
            $emailVerificationToken = trim(strtolower(base64_encode(uniqid('', true) . "-" . date("YmdHis"))));
            $user_id = $this->member_model->addDetails($emailVerificationToken);
            $this->member_model->sendVerificationEmailOnRegistration($emailVerificationToken, $user_id);
            $this->member_model->sendAdminNotification($user_id);

            redirect(ROOT_URL.'select_package/'.$user_id);
            exit;
            //redirect(ROOT_URL.'login');
        } else {
            $this->contentData['privacy_popup_content'] = $this->cms_model->getDetails(CMS_PRIVACY_POPUP_PAGE_ID);
            $this->contentData['terms_popup_content'] = $this->cms_model->getDetails(CMS_TERMS_POPUP_PAGE_ID);
            $this->contentData['formData'] = $this->input->post();
            $countryList = $this->admin_model->getCountryList();
            $countryListOp = $countryList;
            $currentCountry = !empty($countryListOp[0]) ? array_shift($countryListOp) : null;


            $this->contentData['currentCountry'] = $currentCountry;
            $this->contentData['countryList'] = $countryList;
            $this->contentData['cityList'] = $this->admin_model->getCityList();
            $this->contentData['questionList'] = $this->admin_model->getRandomSecurityQuestions();
            $this->headerData['active_tab']= 'register';
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('register', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
	}

    public function select_package($member_id = 0)
    {
        $this->user_id = $member_id;
        $this->contentData['member_id'] = $member_id;
        $memberDetails = $this->member_model->getDetails($member_id);
        if (empty($memberDetails)) {
            $this->session->set_flashdata('flash_error', 'Something went wrong! Please try again later');
            redirect(ROOT_URL . 'login');
        } else if (!empty($memberDetails->file_pending_count) && $memberDetails->file_pending_count > 0) {
            $this->session->set_flashdata('flash_error', 'Please convert pending files of your package before you can purchase new packages');
            if ($this->is_logged_in && !empty($this->headerData['isMemberLogin'])) {
                redirect(ROOT_URL . 'submit_file');
            } else {
                redirect(ROOT_URL . 'login');
            }
            exit;
        } else if (empty($this->user_id)) {
            //Redirect to login page
            $this->session->set_flashdata('flash_error', 'Please login to select your package');
            redirect(ROOT_URL.'login');
            exit;
        }

        $this->load->library('form_validation');
        $this->load->model('package_model');

        $this->form_validation->set_error_delimiters('<div class="alert alert-danger">', '</div>');
        $this->form_validation->set_message('is_unique', '%s is already in use.');
        $this->form_validation->set_message('required', 'Please select a %s.');
        $this->form_validation->set_rules('package_id', 'Package', 'xss_clean|trim|required|numeric|callback_is_valid_package');

        if ($this->form_validation->run()) {
            //Add data to SESSION get id redirect
            $package_id = $this->input->post('package_id');
            $packageDetails = $this->package_model->getDetails($package_id);
            $this->contentData['packageDetails'] = $packageDetails;
            if ($packageDetails->price == 0) {
                //Add package info and Update package info in member table
                $data = array(
                    'package_id' => $package_id,
                    'member_id' => $member_id,
                    'price' => $packageDetails->price,
                    'package_file_count' => $packageDetails->files_count,
                    'pending_file_count' => $packageDetails->files_count,
                    'files_per_day' => $packageDetails->files_per_day,
                    'current_package_start_date' => date('Y-m-d H:i:s'),
                    'current_package_end_date' => $this->package_model->calculate_package_end_date($packageDetails->duration_value),
                    'status' => '1',
                    'created_by' => $member_id,
                    'is_active' => '1'
                );
                $member_package_id = $this->package_model->update_member_package($data, $member_id);
                //Send package email on completion
                $this->package_model->sendPackageEmail($data, $packageDetails);
                //Redirect to login
                $this->session->set_flashdata('flash_success', 'Registration successfull.<br />Please check your inbox for verification email');
                redirect(ROOT_URL.'login');
                exit;
            } else {
                $data['package_cart'] = array(
                    'package_id' => $this->input->post('package_id'),
                    'amount' => $packageDetails->price,
                    'package_file_count' => $packageDetails->files_count,
                );
                $this->session->set_userdata($data);

                $this->config->load('two_checkout');
                $this->contentData['two_co_account_number'] = config_item('two_co_account_number');
                $this->contentData['two_co_account_mode'] = config_item('two_co_account_mode');
                if ($this->contentData['two_co_account_mode'] == 'demo') {
                    $this->contentData['two_co_post_url'] = config_item('two_co_post_url_sandbox');
                } else {
                    $this->contentData['two_co_post_url'] = config_item('two_co_post_url_live');
                }

                $this->load->view('templates/header', $this->headerData);
                $this->load->view('two_checkout_form', $this->contentData);
                $this->load->view('templates/footer', $this->footerData);
            }
        } else {

            $this->config->load('two_checkout');
            $this->contentData['two_co_account_number'] = config_item('two_co_account_number');
            $this->contentData['two_co_account_mode'] = config_item('two_co_account_mode');
            if ($this->contentData['two_co_account_mode'] == 'demo') {
                $this->contentData['two_co_post_url'] = config_item('two_co_post_url_sandbox');
            } else {
                $this->contentData['two_co_post_url'] = config_item('two_co_post_url_live');
            }

            $succ_msg = $this->session->flashdata('flash_success');
            $err_msg = $this->session->flashdata('flash_error');
            if (isset($succ_msg) && $succ_msg != '') {
                $this->contentData['successMsg'] = $this->session->flashdata('flash_success');
            }
            if (isset($err_msg) && $err_msg != '') {
                $this->contentData['errMsg'] = $this->session->flashdata('flash_error');
            }

            $where = "is_public = '1' AND is_active ='1'";
            if (!empty($memberDetails->active_package_id)) {
                $where .= " AND 	price > 0 ";
            }
            $this->contentData['package_list'] = $this->package_model->getAllRecords('*', $where, 'ORDER BY sort_order ASC');
            $this->load->view('templates/header', $this->headerData);
            $this->load->view('select_package', $this->contentData);
            $this->load->view('templates/footer', $this->footerData);
        }
    }

    public function is_valid_package($package_id)
    {
        if ($this->package_model->is_valid_package($package_id)) {

            return true;
        }

        $this->form_validation->set_message('is_valid_package', "Invalid package");

        return false;
    }

    /**
     * function to check duplicate email id called from client registration page via AJAX
     *
     * @access public
     * @param string $email
     * @return status & message
     */
    public function check_duplicate_email()
    {
        $return = array();
        $this->form_validation->set_error_delimiters('', '');
        $this->form_validation->set_message('is_unique', '%s is already in use.');
        $this->form_validation->set_rules('email', 'Email Address', 'xss_clean|trim|required|valid_email|is_unique['.Member_model::$user_table.'.email]');
        if($this->form_validation->run() === FALSE)
        {
            $return = array('status' => 1, 'message' => form_error('email'));
        }
        else
        {
            $return = array('status' => 0, 'message' => '');
        }
        echo json_encode($return);
    }

    function payment_status()
    {
        $credit_card_processed = $this->input->post('credit_card_processed');
        $status = 'failed';
        if(!empty($credit_card_processed) && $credit_card_processed =='Y') {
            $this->load->model('package_model');
            $this->config->load('two_checkout');
            $hashSecretWord = config_item('two_co_secret_word');
            $two_co_account_mode = config_item('two_co_account_mode');
            $hashSid = $this->input->post('sid');//2Checkout account number
            $hashTotal = $this->input->post('total');//Sale total to validate against
            $hashOrder = (!empty($two_co_account_mode) && $two_co_account_mode == 'demo') ? 1 : $this->input->post('order_number'); //2Checkout Order Number

            $StringToHash = strtoupper(md5($hashSecretWord . $hashSid . $hashOrder . $hashTotal));

            if ($StringToHash != $this->input->post('key')) {
                $result = 'Fail - Hash Mismatch';
                $this->session->set_flashdata('flash_error', 'Sorry! We could not authenticate your payment. Please contact your bank for details');
                $status = 'failed';
            } else {
                $result = 'Success - Hash Matched';

                $custom_userid = $this->input->post('custom_userid');
                $billing_email = $this->input->post('email');
                $order_number = $this->input->post('order_number');
                $invoice_id = $this->input->post('invoice_id');
                $total = $this->input->post('total');
                $ip_country = $this->input->post('ip_country');
                $package_id = $this->input->post('package_id');
                $packageDetails = $this->package_model->getDetails($package_id);
                //print_r($packageDetails);
                $data = array(
                    'member_id' => $custom_userid,
                    'billing_email' => $billing_email,
                    'order_number' => $order_number,
                    'invoice_id' => $invoice_id,
                    'price' => $total,
                    'ip_country' => $ip_country,
                    'package_id' => $this->input->post('package_id'),
                    'package_file_count' => $packageDetails->files_count,
                    'pending_file_count' => $packageDetails->files_count,
                    'files_per_day' => $packageDetails->files_per_day,
                    'current_package_start_date' => date('Y-m-d H:i:s'),
                    'current_package_end_date' => $this->package_model->calculate_package_end_date($packageDetails->duration_value),
                    'status' => '1',
                    'created_by' => $custom_userid,
                    'created_date' => date('Y-m-d H:i:s'),
                    'created_ip' => $this->input->ip_address(),
                    'is_active' => '1'
                );
                $this->package_model->update_member_package($data, $custom_userid);
                $this->package_model->sendPackageEmail($data, $packageDetails);
                $this->session->set_flashdata('flash_success', 'Payment received successfully');
                $status = 'success';
            }
        } else {
            if ($this->is_logged_in && !empty($this->headerData['isMemberLogin'])) {
                $this->session->set_flashdata('flash_error', 'Sorry! Payment not received.');
            } else {
                $this->session->set_flashdata('flash_error', 'Sorry! Payment not received. Please check your inbox for account activation email');
            }
            $status = 'failed';
        }

        if ($status == 'failed') {
            $this->session->set_flashdata('flash_error', 'Sorry! We could not authenticate your payment. Please contact your bank for details');
        } else if ($status == 'success') {
            if ($this->is_logged_in && !empty($this->headerData['isMemberLogin'])) {
                $this->session->set_flashdata('flash_success', 'Payment received successfully');
            } else {
                $this->session->set_flashdata('flash_success', 'Registration completed succesfully. Please check you inbox for account activation email.');
            }
        }
        if ($this->is_logged_in && !empty($this->headerData['isMemberLogin'])) {
            redirect(ROOT_URL . 'submit_file');
            exit;
        } else {
            redirect(ROOT_URL . 'login');
            exit;
        }
    }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */