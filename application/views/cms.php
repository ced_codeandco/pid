<div class="row cms_page_wrap">
    <div class="col-lg-12">
        <h3 class="text-left"><?php echo (!empty($show404)) ? 'Page not found' :$cmsData->title;?></h3>
    </div>
    <div class="devider-25px"></div>
    <?php if (!empty($cmsData->cms_banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->cms_banner_image)) {?>
    <div class="col-lg-4">
        <center><img src="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->cms_banner_image;?>" class="img-responsive"></center>
        <p>&nbsp;</p>
    </div><!-- /.col-lg-4 -->
    <?php }?>

    <?php
    if (!empty($cmsData->id) && $cmsData->id == CMS_CONTACT_US_PAGE_ID) {?>

        <div class="col-lg-4 ">
            <?php echo stripslashes($cmsData->description); ?>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
            <?php echo validation_errors(); //die('fffffff');?>
            <?php
            if(isset($errMsg) && $errMsg != ''){ ?>
                <div class="alert alert-danger">
                    <?php echo $errMsg;?>
                </div>
                <?php unset($errMsg);
            }
            if(isset($succMsg) && $succMsg != ''){ ?>
                <div class="alert alert-success">
                    <?php echo $succMsg;?>
                </div>
                <?php unset($succMsg);
            }?>
            <form method="post" action="">
                <div class="form-group">
                    <!--<label for="exampleInputEmail1">Your Name*</label>-->
                    <input value="<?php echo !empty($_POST['name']) ? $_POST['name'] : '';?>" class="form-control validate[required] inputbg" placeholder="Your Name*" type="text" name="name" id="Name" required />
                </div>

                <div class="form-group">
                    <!--<label for="exampleInputEmail1">Email*</label>-->
                    <input value="<?php echo !empty($_POST['email']) ? $_POST['email'] : '';?>" class="form-control validate[required, custom[email]] inputbg" placeholder="Email*" type="email" name="email" id="Email" required />
                </div>

                <div class="form-group">
                    <!--<label for="exampleInputEmail1">Inquiry*</label>-->
                    <textarea name="comments" id="Comments" placeholder="Inquiry*" class="form-control validate[required] textbg" rows="4" style="height:80px; font-size:13px;" required ><?php echo !empty($_POST['comments']) ? $_POST['comments'] : '';?></textarea>
                </div>

                <div class="form-group">
                    <!--<label for="exampleInputEmail1">How much is <?php /*echo $verify_robot[0].' + '.$verify_robot[1]*/?>*</label>-->
                    <input value="<?php echo !empty($_POST['captcha']) ? $_POST['captcha'] : '';?>" class="form-control validate[required, custom[email]] inputbg" placeholder="How much is <?php echo $verify_robot[0].' + '.$verify_robot[1]?>*" type="text" name="captcha" id="captcha" required />
                </div>



                <button type="submit" class="sent-btn">Submit</button>

            </form>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
        </div><!-- /.col-lg-4 -->

        <div class="col-lg-4">
            <p><span class="red-text">EMAIL</span><br>
                <a href="mailto:<?php echo (defined('CONTACT_EMAIL')) ? CONTACT_EMAIL : '';?>"><?php echo (defined('CONTACT_EMAIL')) ? CONTACT_EMAIL : '';?></a></p>
            <!--<p><span class="red-text">TELEPHONE</span><br>
                <?php /*echo (defined('CONTACT_TELEPHONE')) ? CONTACT_TELEPHONE : '';*/?></p>
            <p><span class="red-text">ADDRESS</span><br>
                <?php /*echo (defined('CONTACT_ADDRESS')) ? nl2br(CONTACT_ADDRESS) : '';*/?>
            </p>-->
        </div><!-- /.col-lg-4 -->
        <?php
    } else if ($show404 != true) {?>
        <div class="cms_page_content_wrap">
            <?php echo stripslashes($cmsData->description); ?>
        </div>
    <?php } else {?>
        <div class="col-lg-12">
            <div class="page-not-found">
                <p class="error-code">404</p>
                <p class="error-message">Page not found</p>
                <p class="error-message+"><a href="<?php echo ROOT_URL;?>">Click here to go to home page</a></p>
            </div>
        </div>
    <?php }?>
</div>
