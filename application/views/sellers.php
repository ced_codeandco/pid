<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/8/2015
 * Time: 6:27 PM
 */?>
<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>How It Works</li>
    </ul>
    <h2 class="page-title">How It Works</h2>
    <div class="row">
        <div class="sellers-box">
            <div class="div-sellers-box">
                <span class="heading-stpes">Step 1</span>
                <p><span>List your products</span><br />
                    Uploading your products is really simple through our self-serve <br />
                    tool. We also help you put together an attractive catalog by <br />
                    connecting you to industry experts.</p>

                <ul>
                    <li><img src="images/sellery-icon01.png">Easy to use Self-serve portal</li>
                    <li><img src="images/sellery-icon02.png">Catalog & photo-shoot partners across India</li>
                </ul>
            </div>

            <div class="div-sellers-box steps-two">
                <span class="heading-stpes">Step 2</span>
                <p><span>Sell across UAE</span> <br />
                    Maximise your online sales; attract more buyers and achieve higher <br />
                    conversion rates.</p>

                <ul>
                    <li><img src="images/sellery-icon03.png">Dedicated pick-up service</li>
                    <li><img src="images/sellery-icon04.png">Packaging support</li>
                    <li><img src="images/sellery-icon05.png">Packaging support</li>
                </ul>
            </div>

            <div class="div-sellers-box steps-three">
                <span class="heading-stpes">Step 3</span>
                <p><span>Ship with ease</span><br />
                    Enjoy hassle-free pick up and delivery across India through our  <br />
                    logistics services and sell across the nation!</p>

                <ul>
                    <li><img src="images/sellery-icon06.png">Dedicated pick-up service</li>
                    <li><img src="images/sellery-icon07.png">Packaging support</li>
                </ul>
            </div>

            <div class="div-sellers-box steps-four">
                <span class="heading-stpes"Step 4</span>
                <p><span>Earn big </span> <br />
                    Make use of the host of services that we offer and earn more. Our  <br />
                    payments process is the fastest in the industry - get your  <br />
                    payments within 5-10 days of sales!</p>

                <ul>
                    <li><img src="images/sellery-icon08.png">Fastest payments settlements in the industry</li>
                    <li><img src="images/sellery-icon09.png">Lending partner network</li>
                </ul>
            </div>
        </div>

        <div class="clerafix"></div>
        <!--<div class="start-selling-box">
            <h2>Start Selling</h2>
            <div class="contact-form" style="width:662px;">
                <div class="sellers-field">
                    <input value="Email ID" class="validate[required] border-radius" type="text" name="Name" id="Name">
                </div>
                <div class="sellers-field pull-right">
                    <input value="Phone Number" class="validate[required] border-radius" type="text" name="Name" id="Name">
                </div>
                <div class="clerafix"></div>
                <div class="start-sling-btn"><a href="#"></a></div>
            </div>
        </div>-->
        <span class="big-heading">More Than <strong>30,000</strong> Online Sellers</span>
        <div class="clerafix"></div>
        <div class="readmore-title"><span>Read Success Stories</span></div>
        <div class="clerafix"></div>

        <div class="testimonials-box">
            <div class="flexslider">
                <ul class="slides">
                    <li>Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur,
                        adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore
                        magnam aliquam quaerat voluptatem.
						<span class="test-title"><span>Alvin Lusing</span><br />
CEO, Hint Health & Founding team, WellnessFX</span></li>
                    <li>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam at mauris non arcu
                        suscipit imperdiet. Fusce mi lacus, pharetra at dignissim at, malesuada nec enim. Nullam
                        massa tellus, malesuada non mollis vitae
						<span class="test-title"><span>Fusce Mi Lacus</span><br />
CEO, Hint Health & Founding team, Euismod mauris</span></li>
                    <li>Euismod mauris suscipit diam volutpat ultrices. Quisque sagittis augue tristique erat
                        faucibus, eget vestibulum augue pharetra. Vestibulum pellentesque, lectus at sagittis fringilla,
                        dui mauris mattis ipsum.
						<span class="test-title"><span>Alvin Lusing</span><br />
CEO, Hint Health & Founding team, Malesuada Mollis </span></li>
                </ul>
            </div>
        </div>


    </div>


</div>

<div class="divider-futered"></div>
<link rel="stylesheet" href="<?php echo ROOT_URL;?>css/flexslider.css" type="text/css" media="screen" />
<script src="<?php echo ROOT_URL;?>js/jquery.flexslider-min.js"></script>
<script type="text/javascript">
    $(window).load(function() {
        $('.flexslider').flexslider();
    });
</script>