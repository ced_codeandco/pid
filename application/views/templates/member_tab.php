<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 4/23/2015
 * Time: 10:37 AM
 */?>
<?php if(!empty($tabList) && is_array(($tabList))) {?>
    <ul id="myTabs" class="nav nav-tabs font-18px" role="tablist"><?php
    $count = 1;
    foreach ($tabList as $urlstring => $tab) {
        $tabId = 'memberTab'.$count;?>
        <li role="presentation" id="<?php echo $tabId;?>" class="<?php echo ($tab['is_active'] == 1) ? 'active' : '';?>">
            <a role="tab" aria-expanded="<?php echo ($tab['is_active'] == 1) ? 'true' : 'false';?>" class=" " href="<?php echo MEMBER_ROOT_URL.$urlstring;?>"><?php echo $tab['title'];?></a>
        </li><?php
        $count++;
    }?>
    </ul><?php
}?>

<!--<ul id="myTabs" class="nav nav-tabs font-18px" role="tablist">
    <li role="presentation" class=""><a href="#home" id="home-tab" role="tab" data-toggle="tab" aria-controls="home" aria-expanded="false">My Profile</a></li>
    <li role="presentation" class="active"><a href="#profile" role="tab" id="profile-tab" data-toggle="tab" aria-controls="profile" aria-expanded="true">My Ads</a></li>
</ul>-->