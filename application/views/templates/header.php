<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?php echo defined('DEFAULT_META_TITLE') ? DEFAULT_META_TITLE : SITE_NAME; ?></title>
    <meta name="keywords" content="<?php echo DEFAULT_META_KEYWORDS?>" />
    <meta name="Description" content="<?php if (empty($pageMetaName)) { echo DEFAULT_META_DESCRIPTION; } else echo $pageMetaName;?>"/>
    <?php echo !empty($ogTitle) ? '<meta property="og:title" content="'.$ogTitle.'" />' : '';?>
    <meta property="og:site_name" content="<?php echo defined('DEFAULT_META_TITLE') ? DEFAULT_META_TITLE : SITE_NAME; ?>"/>
    <?php echo !empty($ogImage) ? '<meta property="og:image" content="'.$ogImage.'" />' : '';?>

    <link rel="shortcut icon" type="image/x-icon" href="<?php echo ROOT_URL_BASE;?>favicon.ico">

    <link href="<?php echo ROOT_URL_BASE;?>css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo ROOT_URL_BASE;?>css/style.css" rel="stylesheet">
    <link href="<?php echo ROOT_URL_BASE;?>fonts/fonts.css" rel="stylesheet">
    <link  href="<?php echo ROOT_URL_BASE;?>css/hover-effect.css" rel="stylesheet">
    <link  href="<?php echo ROOT_URL_BASE;?>css/flexslider.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?php echo ROOT_URL_BASE;?>css/dev.css" />

    <!--<script type="text/javascript" src="<?php /*echo ROOT_URL_BASE;*/?>js/jquery-1.11.3.min.js"></script>-->
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/jquery-1.11.3.min.js"></script>
    <link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/colorbox/colorbox.css" />
    <script src="<?php echo ROOT_URL_BASE;?>js/jquery.colorbox.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/helper.js"></script>
    <script type="text/javascript" src="<?php echo ROOT_URL_BASE;?>js/common.js"></script>

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="<?php echo ROOT_URL_BASE;?>css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="<?php echo ROOT_URL_BASE;?>js/ie-emulation-modes-warning.js"></script>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href="<?php echo ROOT_URL_BASE;?>css/carousel.css" rel="stylesheet">
</head>



<body class="<?php echo (!empty($homePage) && $homePage == true) ? 'body-img' : 'bg_pages';?>">
<input type="hidden" id="rootUrlLink" value="<?php echo ROOT_URL;?>" />

<div class="navbar-wrapper">
    <div class="container">
        <div class="col-md-6  logo-brand">
            <a href="<?php echo ROOT_URL;?>" class="visible-lg"><img src="<?php echo ROOT_URL_BASE;?>images/logo.jpg"></a>
            <a href="<?php echo ROOT_URL;?>" class="visible-md visible-sm visible-xs" ><img src="<?php echo ROOT_URL_BASE;?>images/logo.jpg" class="img-responsive"></a>
        </div>

        <div class="col-md-6 left-peding-none">

                <?php if( !empty($isMemberLogin) &&  $isMemberLogin != false) {?>
                    <div class="top-right-btn top-right-btn-widht-auto">
                        <ul class="nav navbar-nav login-btn-header-two">
                            <li class="dropdown">
                                <a href="<?php echo ROOT_URL;?>submit_file" class="upload_button button shadow-radial">Upload</a>
                                <a href="#" class="dropdown-toggle login button shadow-radial title-icon" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo !empty($isMemberLogin->first_name) ? $isMemberLogin->first_name : '';?></a>

                                <ul class="dropdown-menu">
                                    <li><a href="<?php echo MEMBER_ROOT_URL;?>my_profile">My Profile</a></li>
                                    <li><a href="<?php echo MEMBER_ROOT_URL;?>my_files">My  Files</a></li>
                                    <?php if (!empty($isMemberLogin->password)) {?>
                                    <li><a href="<?php echo MEMBER_ROOT_URL;?>change_password">Change Password</a></li>
                                    <?php }?>
                                    <!--<li><a href="#">My Searches</a></li>-->
                                    <!--<li><a href="<?php /*echo MEMBER_ROOT_URL;*/?>account_settings">Account Settings</a></li>-->
                                    <li><a href="<?php echo ROOT_URL;?>logout">Sign Out</a></li>
                                </ul>
                            </li>
                        </ul>

                    </div>
                <?php } else {?>
                    <div class="top-right-btn">
                        <a href="<?php echo ROOT_URL;?>register_via_linkedin" class="register button shadow-radial">Register</a>
                        <ul class="nav navbar-nav login-btn-header">
                            <li class="dropdown">
                                <a href="<?php echo ROOT_URL;?>login" class="login button shadow-radial" role="button" >Login</a>
                            </li>
                        </ul>
                    </div>
                <?php }?>


            <nav class="navbar navbar-inverse navbar-static-top z-index-0">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                </div>


            </nav>
        </div>

        <!--<div class="col-md-12 spreter-red"></div>-->

    </div>
    <!-------MENU---->
    <div class="top-menu-custom-wrap">
        <div class="container">
            <div id="navbar" class="navbar-collapse collapse">
                <ul class="nav navbar-nav">
                    <?php if (!empty($top_pages) && is_array($top_pages)){
                        foreach($top_pages as $page) {
                            if ($page->id == CMS_HOME_PAGE_ID){?>
                                <li><a class="<?php echo empty($current_slug) ? 'active' : '';?>"
                                       href="<?php echo ROOT_URL; ?>"><?php echo $page->title; ?></a>
                                </li><?php
                            } else {
                                $is_popup = '';
                                $page_url = ROOT_URL.$page->cms_slug;
                                if (!empty($page->is_popup) && $page->is_popup == 1) {
                                    $is_popup = 'ajax-popup';
                                    $page_url = ROOT_URL.'cms/'.$page->cms_slug.'/ajax';
                                }   ?>
                                <li><a class="<?php echo (!empty($current_slug) && $current_slug == $page->cms_slug) ? 'active' : '';  echo ' '.$is_popup;?>"
                                       href="<?php echo $page_url; ?>"><?php echo $page->title; ?></a>
                                </li><?php
                            }
                        }
                    }?>
                </ul>
            </div>
        </div>
    </div>
</div>


<div class="container marketing-box">