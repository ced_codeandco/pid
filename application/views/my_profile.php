<div class="row">
    <div class="col-lg-12">
        <h3 class="text-left">My Profile</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">

        <?php $this->load->view('templates/member_tab', $tabData);?>


        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">

                <p class="border-bottom">Name:<span class="red-text"><?php echo $profileData->first_name. ' ' .$profileData->last_name?></span> (not <?php echo $profileData->first_name;?> ? <a href="<?php echo ROOT_URL?>logout"><strong>logout</strong></a>)</p>

                <div class="col-lg-8 peding-left-none zeebra-form">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <form name="" method="post" action="" class="profile-lable">
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">First Name:</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control"  type="text" placeholder="First Name" required name="first_name" value="<?php echo !empty($formData['first_name']) ? $formData['first_name'] : ''?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Last Name:</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" type="text" placeholder="Last Name" required name="last_name" value="<?php echo !empty($formData['last_name']) ? $formData['last_name'] : ''?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Email:</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <?php echo !empty($formData['email']) ? $formData['email'] : ''?>
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Date Of Birth:</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" type="text" id="datePicker" placeholder="Date of Birth" required name="date_of_birth" value="<?php echo !empty($formData['date_of_birth']) ? date('d F Y', strtotime($formData['date_of_birth'])) : ''?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Country:</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <div class="select_style">
                                    <select name="country" id="country">
                                        <option value="">Select</option>
                                        <?php if (is_array($cityList)) {
                                            foreach ($countryList as $country) {
                                                echo '<option value="' .$country->id. '" ' .((!empty($formData['country']) && $formData['country'] == $country->id) ? 'selected="selected"' : ''). '>' .$country->name. '</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">City:</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" type="text" id="datePicker" placeholder="City" required name="city" value="<?php echo !empty($formData['city']) ? $formData['city'] : ''?>">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <button type="submit" class="update-profile-btn">Update Profile</button>
                    </form>
                </div>

            </div>




        </div>
    </div><!-- /.col-lg-12 -->


</div>
<link href="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.css" rel="stylesheet">
<script src="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">
    $(function() {

        $( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear()
        });
        var row_count = 1;
        $('.zeebra-form').find('div.form-group').each(function(){
            if (row_count % 2 == 1) {
                $(this).addClass('odd');
            }
            row_count++;
        })
    });
</script>