<div class="banner list-banner <?php if (empty($_GET['category']) OR ($_GET['category'] != CATEG_VEHICLES_ID  && $_GET['category'] != CATEG_REAL_EST_ID )) { echo "hidden_banner"; }?>">

    <div class="main-wraper">

        <div class="container">
            <div class="banner-list-search">
                <h1 class="selling-heaing"><?php echo $searchResultTotal;?> result(s) found </h1>




                <form method="get" action="" id="searchForm">
                    <input type="hidden" name="search" value="1">
                    <input type="hidden" name="searched" value="1">
                    <?php
                    $searchCriteria = $_GET;
                    foreach ($searchCriteria as $name => $value){
                        echo (!empty($name) && !empty($value)) ? '<input type="hidden" name="'.$name.'" value="'.$value.'" >' : '';
                    }?>
                    <div class="post-field-col contact-form" style="width:200px;">
                        <label>Category</label>
                        <select name="category" id="category" onchange="create_loadCriteriaLookup(this, this.value, $('#sub_category'), 'subCategory');" class="search-drop-down">
                            <option value="">Select</option>
                            <?php if (!empty($parentCategoriesList) && is_array($parentCategoriesList)){
                                foreach ($parentCategoriesList as $item) {
                                    $selected = (!empty($_GET['category']) && $item->id == $_GET['category']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$item->id.'">'.$item->title.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col contact-form" style="width:200px; margin-left:20px;">
                        <label>Sub Category</label>
                        <select name="sub_category" id="sub_category"  class="search-drop-down">
                            <option value="">Select</option>
                            <?php if (!empty($subCategoriesList) && is_array($subCategoriesList)){
                                foreach ($subCategoriesList as $item) {
                                    $selected = (!empty($_GET['sub_category']) && $item->id == $_GET['sub_category']) ? 'selected="selected"' : '';
                                    echo '<option '.$selected.' value="'.$item->id.'">'.$item->title.'</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col contact-form" style="width:200px; margin-left:20px;">
                        <label>Country</label>
                        <select  name="country" id="country" class="lookUpPreloader" onchange="loadLookup(this, this.value, $('#search_city'), 'city', 'loadLocalityOnCreate');">
                            <option value="">Select</option>
                            <?php if (is_array($countryList)) {
                                foreach ($countryList as $country) {
                                    $selected = (!empty($_GET['country']) && $country->id == $_GET['country']) ? 'selected="selected"' : '';
                                    echo '<option value="' .$country->id. '" '.$selected.' >' .$country->name. '</option>';
                                }
                            }?>
                        </select>
                    </div>
                    <div class="post-field-col contact-form" style="width:200px; margin-left:20px;">
                        <label>City</label>
                        <select  name="search_city" id="search_city" class="lookUpPreloader" onchange="loadLookup(this, this.value, $('#locality'), 'locality');">
                            <option value="">Select</option>
                            <?php if (is_array($cityList)) {
                                foreach ($cityList as $id => $city) {
                                    $selected = (!empty($_GET['search_city']) && $id == $_GET['search_city']) ? 'selected="selected"' : '';
                                    echo '<option value="' .$id. '" '.$selected.' >' .$city. '</option>';
                                }
                            }?>
                        </select>
                    </div>

                    <div id="category_based-filter-wrap_<?php echo CATEG_VEHICLES_ID;?>">
                        <div class="clearfix"></div>
                        <div class="post-field-col contact-form" style="width:100px;">
                            <label>Brand Name</label>
                            <select name="brand_id" class="search-drop-down">
                                <option value="">Select</option>
                                <?php if(!empty($brandLookup) && is_array($brandLookup)){
                                    foreach ($brandLookup as $brand) {
                                        echo '<option '.((!empty($_GET['brand_id']) && $_GET['brand_id'] == $brand->id) ? 'selected="selected"' : '').' value="'.$brand->id.'">'.$brand->title.'</option>';
                                    }
                                }?>
                            </select>
                        </div>
                        <div class="post-field-col" style="width:90px; margin-left:20px;">
                            <label>Price Range</label>
                            <input value="<?php echo !empty($_GET['min_amount']) ? $_GET['min_amount'] : '';?>" placeholder="Price From" type="number" name="min_amount" id="min_amount">
                        </div>
                        <div class="post-field-col" style="width:90px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_amount']) ? $_GET['max_amount'] : '';?>" placeholder="Price To" type="number" name="max_amount" id="max_amount">
                        </div>
                        <div class="post-field-col" style="width:108px; margin-left:20px;">
                            <label>Year</label>
                            <input value="<?php echo !empty($_GET['manufacture_year_from']) ? $_GET['manufacture_year_from'] : '';?>" placeholder="Year From" type="number" name="manufacture_year_from" id="manufacture_year_from1">
                        </div>
                        <div class="post-field-col" style="width:108px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['manufacture_year_to']) ? $_GET['manufacture_year_to'] : '';?>" placeholder="Year To" type="number" maxlength="4" name="manufacture_year_to" id="manufacture_year_to">
                        </div>
                        <div class="post-field-col contact-form" style="width:85px; margin-left:20px;">
                            <label>Fuel Type</label>
                            <select name="fuel_type" class="search-drop-down">
                                <option value="">Select</option>
                                <?php if(!empty($fuelLookUp) && is_array($fuelLookUp)){
                                    foreach ($fuelLookUp as $fuel) {
                                        echo '<option '.((!empty($_GET['fuel_type']) && $_GET['fuel_type'] == $fuel->id) ? 'selected="selected"' : '').' value="'.$fuel->id.'">'.$fuel->title.'</option>';
                                    }
                                }?>
                            </select>
                        </div>
                        <div class="post-field-col contact-form" style="width:85px; margin-left:20px;">
                            <label>Transmission</label>
                            <select name="transmission" class="search-drop-down">
                                <option value="">Select</option>
                                <?php if(!empty($transmissionLookUp) && is_array($transmissionLookUp)){
                                    foreach ($transmissionLookUp as $transmission) {
                                        echo '<option '.((!empty($_GET['transmission']) && $_GET['transmission'] == $transmission->id) ? 'selected="selected"' : '').' value="'.$transmission->id.'">'.$transmission->title.'</option>';
                                    }
                                }?>
                            </select>
                        </div>
                    </div>
                    <div id="category_based-filter-wrap_<?php echo CATEG_REAL_EST_ID;?>">
                        <div class="clearfix"></div>
                        <div class="post-field-col" style="width:90px;">
                            <label>Price Range</label>
                            <input value="<?php echo !empty($_GET['min_amount']) ? $_GET['min_amount'] : '';?>" placeholder="Price From" type="number" name="min_amount" id="min_amount">
                        </div>
                        <div class="post-field-col" style="width:90px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_amount']) ? $_GET['max_amount'] : '';?>" placeholder="Price To" type="number" name="max_amount" id="max_amount">
                        </div>

                        <div class="post-field-col multiple" style="width:200px; margin-left:20px;">
                            <label>Bed Rooms</label><div class="clearfix"></div>
                            <input value="<?php echo !empty($_GET['min_bed_rooms']) ? $_GET['min_bed_rooms'] : '';?>" placeholder="Min." type="number" name="min_bed_rooms" id="min_bed_rooms1">
                            <!--</div>
                            <div class="post-field-col" style="width:75px; margin-left:10px;">-->
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_bed_rooms']) ? $_GET['max_bed_rooms'] : '';?>" placeholder="Max." type="number" maxlength="4" name="max_bed_rooms" id="max_bed_rooms">
                        </div>

                        <div class="post-field-col multiple" style="width:150px; margin-left:20px;">
                            <label>Bath Rooms</label><div class="clearfix"></div>
                            <input value="<?php echo !empty($_GET['min_bath_rooms']) ? $_GET['min_bath_rooms'] : '';?>" placeholder="Min." type="number" name="min_bath_rooms" id="min_bath_rooms1">
                            <!--</div>
                            <div class="post-field-col" style="width:75px; margin-left:10px;">-->
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_bath_rooms']) ? $_GET['max_bath_rooms'] : '';?>" placeholder="Max." type="number" maxlength="4" name="max_bath_rooms" id="max_bath_rooms">
                        </div>

                        <div class="post-field-col multiple" style="width:166px; margin-left:20px;">
                            <label>Area (Sq. ft.)</label>
                            <input value="<?php echo !empty($_GET['min_area_sqft']) ? $_GET['min_area_sqft'] : '';?>" placeholder="Min." type="number" name="min_area_sqft" id="min_area_sqft1">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_area_sqft']) ? $_GET['max_area_sqft'] : '';?>" placeholder="Max." type="number" maxlength="4" name="max_area_sqft" id="max_area_sqft">
                        </div>
                    </div>
                    <div id="category_based-filter-wrap_<?php echo CATEG_JOB_ID;?>">
                        <div class="clearfix"></div>

                        <div class="post-field-col" style="width:90px;">
                            <label>Salary</label>
                            <input value="<?php echo !empty($_GET['min_amount']) ? $_GET['min_amount'] : '';?>" placeholder="Salary From" type="number" name="min_amount" id="min_amount">
                        </div>
                        <div class="post-field-col" style="width:90px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_amount']) ? $_GET['max_amount'] : '';?>" placeholder="Salary To" type="number" name="max_amount" id="max_amount">
                        </div>

                        <div class="post-field-col contact-form" style="width:172px; margin-left: 20px;">
                            <label>Career</label>
                            <select name="career_level">
                                <option value="">Select</option>
                                <?php if (!empty($career_levelLookUp) && is_array($career_levelLookUp)) {
                                    foreach ($career_levelLookUp as $item) {
                                        $selected = (!empty($_GET['career_level']) && $item->id == $_GET['career_level']) ? 'selected="selected"' : '';?>
                                        <option <?php echo $selected;?> value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>

                        <div class="post-field-col contact-form" style="width:172px; margin-left: 20px;">
                            <label> Education</label>
                            <select name="education">
                                <option value="">Select</option>
                                <?php if (!empty($educationLookUp) && is_array($educationLookUp)) {
                                    foreach ($educationLookUp as $item) {
                                        $selected = (!empty($_GET['education']) && $item->id == $_GET['education']) ? 'selected="selected"' : '';?>
                                        <option <?php echo $selected;?> value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>

                        <div class="post-field-col contact-form" style="width:172px; margin-left: 20px;">
                            <label> Employment</label>
                            <select name="education">
                                <option value="">Select</option>
                                <?php if (!empty($employment_typeLookUp) && is_array($employment_typeLookUp)) {
                                    foreach ($employment_typeLookUp as $item) {
                                        $selected = (!empty($_GET['education']) && $item->id == $_GET['education']) ? 'selected="selected"' : '';?>
                                        <option <?php echo $selected;?> value="<?php echo $item->id;?>"><?php echo $item->title;?></option>
                                    <?php }
                                } ?>
                            </select>
                        </div>
                    </div>
                    <div id="category_based-filter-wrap_common">
                        <div class="clearfix"></div>
                        <div class="post-field-col" style="width:90px;">
                            <label>Price Range</label>
                            <input value="<?php echo !empty($_GET['min_amount']) ? $_GET['min_amount'] : '';?>" placeholder="Price From" type="number" name="min_amount" id="min_amount">
                        </div>
                        <div class="post-field-col" style="width:90px; margin-left:10px;">
                            <label>&nbsp;</label>
                            <input value="<?php echo !empty($_GET['max_amount']) ? $_GET['max_amount'] : '';?>" placeholder="Price To" type="number" name="max_amount" id="max_amount">
                        </div>
                    </div>
                        <div class="post-field-col " style="width:98px; margin-left:10px; margin-top: 21px; /*float: right;*/">
                            <label>&nbsp;</label>
                            <input type="submit" value="Search">
                        </div>

                </form>

            </div>

        </div>

    </div>

</div>


<div class="main-wraper">

    <div class="container searc-result-string">
        <h2 class="page_heading searc-result-string"><?php //if (!empty($_GET['searched']) && $_GET['searched'] ==1 && empty($searchResult) OR count($searchResult) <1){echo '0 result(s) found'; }?><?php if (!empty($_GET['searched'])) {echo !empty($searchResultTotal) ? $searchResultTotal : '0'; echo 'result(s) found';}?> <?php //echo !empty($searchPageHeader) ? $searchPageHeader : '';?></h2>
        <div class="pages">

            <div class="pages_left"><?php
                if(isset($searchResult) && count($searchResult) > 0) {
                    $i = 0;
                    foreach ($searchResult as $classified) {
                        $i++; ?>
                        <div class="product_list">

                            <div class="product_image">
                                <a class="product-thumb-wrap" href="<?php echo ROOT_URL.'details/'.$classified->classified_slug.'?back='.$back_url_query_string;?>">
                                    <?php
                                    $imageCount = 0;
                                    $firstImage = '';
                                    if($classified->image_list && count($classified->image_list) > 0){
                                        //$imageCount = count($classified->image_list);
                                        foreach ($classified->image_list as $image) {
                                            if ($image->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$image->classified_image)) {
                                                $imageCount++;
                                                $firstImage = !empty($firstImage) ? $firstImage : $image->classified_image;
                                            }
                                        }
                                        if (!empty($firstImage)) {?>
                                            <img src="<?php echo DIR_UPLOAD_CLASSIFIED_SHOW.$firstImage; ?>" >
                                        <?php }else{?>
                                            <img src="<?php echo ROOT_URL_BASE;?>images/placeholder-image.jpg" >
                                        <?php }
                                    } else { ?>
                                        <img src="<?php echo ROOT_URL_BASE;?>images/placeholder-image.jpg">
                                    <?php }?>
                                    <?php echo !empty($imageCount) ? "<span class='count_images'>$imageCount images</span>" : '';?>
                                </a>
                            </div>

                            <div class="product_deatils"><h1><?php echo !empty($classified->amount) ? 'AED '.number_format($classified->amount, 2) : 'AED 0.00';;?></h1>

                                <h2><a href="<?php echo ROOT_URL.'details/'.$classified->classified_slug.'?back='.$back_url_query_string;?>"><?php echo $classified->title;?></a></h2>
                                <?php if ($classified->category_id == CATEG_VEHICLES_ID) {?>
                                <ul class="product_feature">
                                    <li>Brand: <?php echo !empty($brandLookup[$classified->brand_id]->title) ? '<span>'.$brandLookup[$classified->brand_id]->title.'</span>' : ''; ?></li>
                                    <li>Fuel: <?php echo !empty($fuelLookUp[$classified->fuel_type]->title) ? '<span>'.$fuelLookUp[$classified->fuel_type]->title.'</span>' : '';?></li>
                                    <li>Trans.: <?php echo !empty($transmissionLookUp[$classified->fuel_type]->title) ? '<span>'.$transmissionLookUp[$classified->fuel_type]->title.'</span>' : '';?></li>
                                    <li>Year: <?php echo !empty($classified->manufacture_year) ? '<span>'.$classified->manufacture_year.'</span>' : '';?></li>
                                </ul>

                                <ul class="product_feature border_li_none">
                                    <li>Model: <?php echo !empty($modelLookup[$classified->model_id]->title) ? '<span>'.$modelLookup[$classified->model_id]->title.'<span>' : '';?></li>
                                    <li>KM's driven: <?php echo !empty($classified->running_km) ? '<span>'.$classified->running_km.'<span>' : '';?></li>
                                </ul>

                                <?php } else if ($classified->category_id == CATEG_REAL_EST_ID) {?>
                                    <ul class="product_feature">
                                        <li>Bed Rooms:<?php echo !empty($classified->bedrooms) ? '<span>'.$classified->bedrooms.'</span>' : ''; ?></li>
                                        <li>Bath Rooms:<?php echo !empty($classified->bathrooms) ? '<span>'.$classified->bathrooms.'</span>' : ''; ?></li>
                                    </ul>
                                    <ul class="product_feature border_li_none">
                                        <li>Area(Sq. Ft.):<?php echo !empty($classified->area_sqft) ? '<span>'.$classified->area_sqft.'</span>' : ''; ?></li>
                                    </ul>
                                <?php } else if ($classified->category_id == CATEG_JOB_ID) {?>
                                <?php }?>
                            </div>

                            <div class="product_location"><span>Located:</span>
                                <ul>
                                    <li><?php echo !empty($classified->classified_country) ? ' <a href="'.ROOT_URL.'search?search_country='.$classified->classified_country.'">'.$classified->country.'</a>' : ''; ?></li>
                                    <li><?php echo !empty($classified->city) ? ' <a href="'.ROOT_URL.'search?search_city='.$classified->classified_city.'">'.$classified->city.'</a>' : ''; ?></li>
                                    <li><?php echo !empty($classified->locality) ? ' <a href="'.ROOT_URL.'search?classified_locality='.$classified->classified_locality.'">'.$classified->locality.'</a>' : ''; ?></li>
                                </ul>
                            </div>


                        </div><?php
                    }
                }?>

                <div class="pagination_div">
                    <?php echo $paginator;?>
                </div>

            </div>

            <div class="pages_right"><?php //print_r($advertize[0]);?>
                <?php
                if (!empty($advertize[0]->image_path) && file_exists(DIR_UPLOAD_ADVERTIZE.$advertize[0]->image_path)) {
                    echo (!empty($advertize[0]->advertize_url)) ? '<a href="'.prep_url($advertize[0]->advertize_url).'" target="_blank">' : '';?>
                    <img src="<?php echo DIR_UPLOAD_ADVERTIZE_SHOW.$advertize[0]->image_path?>" class="margin_bottom" /><?php
                    echo (!empty($advertize[0]->advertize_url)) ? '</a>' : '';?>
                <?php }?>
                <div class="fb-wrap margin_bottom">
                <?php if (defined('FACEBOOK_PAGE_URL')) {?>




                                    <div id="fb-root"></div>
                                    <script>(function(d, s, id) {
                                            var js, fjs = d.getElementsByTagName(s)[0];
                                            if (d.getElementById(id)) return;
                                            js = d.createElement(s); js.id = id;
                                            js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4";
                                            fjs.parentNode.insertBefore(js, fjs);
                                        }(document, 'script', 'facebook-jssdk'));</script>
                                    <div class="fb-page" data-href="<?php echo defined('FACEBOOK_PAGE_URL') ? prep_url(FACEBOOK_PAGE_URL) : '';?>" data-width="100%" data-height="250" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false" data-show-posts="true">
                                        <div class="fb-xfbml-parse-ignore">
                                            <blockquote cite="<?php echo defined('FACEBOOK_PAGE_URL') ? prep_url(FACEBOOK_PAGE_URL) : '';?>"><a href="<?php echo defined('FACEBOOK_PAGE_URL') ? prep_url(FACEBOOK_PAGE_URL) : '';?>">Facebook</a></blockquote>
                                        </div>
                                    </div>




                <?php }?>
                </div>
                <?php
                if (!empty($advertize[1]->image_path) && file_exists(DIR_UPLOAD_ADVERTIZE.$advertize[1]->image_path)) {
                    echo (!empty($advertize[1]->advertize_url)) ? '<a href="'.prep_url($advertize[1]->advertize_url).'" target="_blank">' : '';?>
                    <img src="<?php echo DIR_UPLOAD_ADVERTIZE_SHOW.$advertize[1]->image_path?>" class="margin_bottom" /><?php
                    echo (!empty($advertize[1]->advertize_url)) ? '</a>' : '';?>
                <?php }?>
            </div>

        </div>



    </div>

</div>
<script>
    $(document).ready(function(){
        categ_id = $('#category').val();
        create_manage_lookUpfields(categ_id);
    })
</script>