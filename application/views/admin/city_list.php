<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">City List</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> Category  List</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content medium-select-wrap">
            <select data-rel="chosen" onchange="window.location='<?php echo ADMIN_ROOT_URL;?>city?countyId='+this.value;">
                <option value="">Select Country</option>
                <?php if (!empty($countryList) && is_array($countryList)){
                    foreach ($countryList as $country) {
                        echo '<option '.( (!empty($countyId) && $countyId == $country->id) ? 'selected="selected"' : '').' value="'.$country->id.'">'.$country->name.'</option>';
                    }
                }?>
            </select>
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="10%">Order</th>
          <th width="22%">Title</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($cityList && count($cityList) > 0 ){
			$paOrder =1;
		foreach ($cityList as $city){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?>
         
            <?php if($paOrder!=1){?>
            <a href="javascript:changeOrdercity('<?php echo $city->id ?>','<?php echo $city->city_order ?>','Up',<?php echo $city->country_id ?>);"><img src="<?php echo ROOT_URL_BASE?>images/admin/uparrow.png" alt="Up" width="16" height="16" border="0" /></a>
            <?php }?>
            <?php if($paOrder!=count($cityList)){?>
            <a href="javascript:changeOrdercity('<?php echo $city->id ?>','<?php echo $city->city_order ?>','Dn',<?php echo $city->country_id ?>);"><img src="<?php echo ROOT_URL_BASE?>images/admin/downarrow.png" alt="Down" width="16" height="16" border="0" /></a>
            <?php }?>
                 </td>
          <td>
            <a href="<?php echo ADMIN_ROOT_URL?>city/add/<?php echo $city->id?>" ><?php echo $city->title;  ?></a>
                     </td>

          <td style="text-align:center" id="td_status_<?php echo $city->id ?>">
          
            <?php if($city->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>city/status_inactive/<?php echo $city->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>city/status_active/<?php echo $city->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
			      </td>
          <td class="t-center">
            <a href="<?php echo ADMIN_ROOT_URL?>city/add/<?php echo $city->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>

            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>city/delete/<?php echo $city->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>

                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrdercity(id,city_order,position,parent)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>city/order?id="+id+"&city_order="+city_order+"&position="+position+"&country_id="+parent;
}


</script>