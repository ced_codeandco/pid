<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#"><?php echo $action;?> Packages</a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> Packages</h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content"> <?php echo validation_errors(); ?>
                        <?php
                        $editUrl = '';
                        if($action == 'Edit'){
                            $editUrl = '/'.$itemDetails->id;
                        }

                        $attributes = array('name' => 'cmsForm', 'id' => 'cmsForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_cms();');
                        echo form_open('', $attributes); ?>
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($itemDetails->id)) ? $itemDetails->id : 0;?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>" />


                        <div class="form-group input-group col-md-4" id="title_msg_error">
                            <label class="control-label" for="title">Title<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($itemDetails->title)) ? $itemDetails->title : ''; }?>" id="title" placeholder="Enter Title">
                            <br />
                            <label class="control-label" id="title_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="description_msg_error">
                            <label for="description">Description</label>
                            <textarea class="form-control"  maxlength="500" name="description"  id="description" placeholder="Description"><?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { echo $_SESSION['description']; unset($_SESSION['description']);}else { echo (isset($itemDetails->description)) ? $itemDetails->description : ''; }?></textarea>
                        </div>

                        <div class="form-group input-group col-md-4" id="files_count_msg_error">
                            <label class="control-label" for="files_count">Number of files allowed<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="files_count" value="<?php if(isset($_SESSION['files_count']) && $_SESSION['files_count'] != '') { echo $_SESSION['files_count']; unset($_SESSION['files_count']);}else { echo (isset($itemDetails->files_count)) ? $itemDetails->files_count : ''; }?>" id="files_count" placeholder="Enter Number of files">
                            <br />
                            <label class="control-label" id="files_count_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="files_per_day_msg_error">
                            <label class="control-label" for="files_per_day">Number of files allowed per day<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="files_per_day" value="<?php if(isset($_SESSION['files_per_day']) && $_SESSION['files_per_day'] != '') { echo $_SESSION['files_per_day']; unset($_SESSION['files_per_day']);}else { echo (isset($itemDetails->files_per_day)) ? $itemDetails->files_per_day : ''; }?>" id="files_per_day" placeholder="Enter Number of files">
                            <br />
                            <label class="control-label" id="files_per_day_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="duration_value_msg_error">
                            <label class="control-label" for="duration_value">Number of days allowed(Package duration) <span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="duration_value" value="<?php if(isset($_SESSION['duration_value']) && $_SESSION['duration_value'] != '') { echo $_SESSION['duration_value']; unset($_SESSION['duration_value']);}else { echo (isset($itemDetails->duration_value)) ? $itemDetails->duration_value : ''; }?>" id="duration_value" placeholder="Enter Number of files">
                            <label class="control-label" id="duration_value_msg"></label>
                        </div>

                        <div class="form-group input-group col-md-4" id="price_msg_error">
                            <label class="control-label" for="price">Price<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="price" value="<?php if(isset($_SESSION['price']) && $_SESSION['price'] != '') { echo $_SESSION['price']; unset($_SESSION['price']);}else { echo (isset($itemDetails->price)) ? $itemDetails->price : ''; }?>" id="price" placeholder="Enter Price">
                            <br />
                            <label class="control-label" id="price_msg"></label>
                        </div>

                        <!--<div class="control-group">
                            <label class="control-label" for="selectError">Is Public</label>
                            <div class="controls">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="1" <?php /*if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($itemDetails->is_active) && $itemDetails->is_active == 1) ? 'selected="selected"' : ''; }*/?> >Active</option>
                                    <option value="0" <?php /*if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 0) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($itemDetails->is_active) && $itemDetails->is_active == 1) ? 'selected="selected"' : ''; }*/?> >In Active</option>
                                </select>
                            </div>
                        </div>-->

                        <div class="control-group">
                            <label class="control-label" for="selectError">Is Active</label>
                            <div class="controls">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="1" <?php if(isset($_POST['is_active']) && $_POST['is_active'] == 1) { echo 'selected="selected"'; }else { echo (isset($itemDetails->is_active) && $itemDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
                                    <option value="0" <?php if(isset($_POST['is_active']) && $_POST['is_active'] == 0) { echo 'selected="selected"'; }else { echo (isset($itemDetails->is_active) && $itemDetails->is_active == 0) ? 'selected="selected"' : ''; }?> >In Active</option>
                                </select>
                            </div>
                        </div>


                        <br />
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        <?php echo form_close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validate_cms(){
            if($("#title").val()==''){
                $("#title_msg").html('Please enter CMS title');
                $("#title_msg_error").addClass('has-error');
                $("#title").focus();
                return false;
            }else{
                $("#title_msg").html('');
                $("#title_msg_error").removeClass('has-error');
            }
        }
    </script>
