<div id="content" class="col-lg-10 col-sm-10">
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#">Images for <?php echo $classifiedDetails->title?></a> </li>
   
  </ul>
  
</div>

<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
    
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> Images for <?php echo $classifiedDetails->title?>  </h2> <a href="<?php echo ADMIN_ROOT_URL?>classified/add_image/<?php echo $classifiedDetails->id?>" style="float:right"><i class="glyphicon glyphicon-cog"></i> Add Image</a>
          
         </div>
         
        <div class="box-content">
        
          <?php if(isset($successMsg) && $successMsg != ''){?>
          <div class="alert alert-success">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $successMsg; unset($successMsg);?></div>
          <?php } ?>
          <?php if(isset($errMsg) && $errMsg != ''){?>
          <div class="alert alert-danger">
            <button data-dismiss="alert" class="close" type="button">×</button>
            <?php echo $errMsg; unset($errMsg);?></div>
          <?php } ?>
          <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
            <thead>
              <tr>
          
          <th width="10%">No</th>
          <th width="22%">Image</th>
          <th width="12%" style="text-align:center">Status</th>
          <th width="20%" style="text-align:center">Action</th>
        </tr>
            </thead>
            <tbody>
              <?php 
		$i = 0;
		if($imageList && count($imageList) > 0 ){
			$paOrder =1; 
			/*
			
			
		  	foreach ($imageList as $image){ 
			$i++;
		?>
              <tr>
                <td><?php echo $i;?></td>
                <td><?php echo $image->title?></td>
               
                
                
                <td class="center"><?php if($image->id != 1) {?>
                  <?php if($image->is_active == 1) {?>
                  <a class="label-success label label-default" href="<?php echo ADMIN_ROOT_URL?>image/status_inactive/<?php echo $image->id?>" >Active</a>
                  <?php }else{?>
                  <a class="label-default label label-danger" href="<?php echo ADMIN_ROOT_URL?>image/status_active/<?php echo $image->id?>">Inactive</a>
                  <?php } 
		  }else{
			  if($image->is_active == 1) {?>
                  <span class="label-success label label-default">Active</span>
                  <?php }else{?>
                  <span class="label-default label label-danger" >Inactive</span>
                  <?php }	
		}
		?></td>
                <td class="center"><a class="btn btn-info" href="<?php echo ADMIN_ROOT_URL?>image/add/<?php echo $image->id?>"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit </a>
                  <?php if($image->id != 1) {?>
                  <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>image/delete/<?php echo $image->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
                  <?php }?>                  
              </tr>
              <?php 	}
		*/
		foreach ($imageList as $image){  ?>
        <tr>
          
          <td><?php echo $paOrder; ?> </td>
          <td>
            <?php if(isset($image->classified_image) && $image->classified_image!='' && file_exists(DIR_UPLOAD_CLASSIFIED.$image->classified_image)) {?>
     
      <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_CLASSIFIED_SHOW.$image->classified_image ?>&q=100&w=150"/>
     
      <?php } ?>
          </td>
          <td style="text-align:center" id="td_status_<?php echo $image->id ?>">
            
           
            <?php if($image->is_active=='1'){?>
            <a href="<?php echo ADMIN_ROOT_URL?>classified/image_status_inactive/<?php echo $image->id?>" class="label-success label label-default" >Active</a>
            <?php }else{?>
            <a href="<?php echo ADMIN_ROOT_URL?>classified/image_status_active/<?php echo $image->id?>" class="label-default label label-danger"  >In Active</a>
            <?php }?>
            
          </td>
          <td class="t-center">
            
            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>classified/image_delete/<?php echo $image->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
           
                   </td>
        </tr>
       
        <?php $paOrder++; }
		
		} ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
function changeOrderCMS(id,image_order,position,parent)
{
	location.href ="<?php echo ADMIN_ROOT_URL?>image/order?id="+id+"&image_order="+image_order+"&position="+position+"&parent="+parent;
}

</script>