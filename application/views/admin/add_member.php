<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/datepicker/jquery-ui.js"></script>
<link href="<?php echo ROOT_URL_BASE?>assets/datepicker/jquery-ui.css" rel="stylesheet">
<div id="content" class="col-lg-10 col-sm-10">
    <!-- content starts -->
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#"><?php echo $action;?> Member</a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-user"></i> <?php echo $action;?> Member</h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content"> <?php echo validation_errors(); ?>
                        <?php
                        $editUrl = '';
                        if($action == 'Edit'){
                            $editUrl = '/'.$memberDetails->id;
                        }
                        $attributes = array('name' => 'memberForm', 'id' => 'memberForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_member();');
                        echo form_open(ADMIN_ROOT_URL.'members/add'.$editUrl,$attributes); ?>
                        <input type="hidden" name="id" id="id" value="<?php echo (isset($memberDetails->id)) ? $memberDetails->id : 0;?>" />
                        <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
                        <div class="form-group input-group col-md-4" id="first_name_msg_error">
                            <label class="control-label" for="first_name">First name<span class="required">*</span></label>
                            <input type="text" class="form-control" maxlength="255" name="first_name" value="<?php if(isset($_SESSION['first_name']) && $_SESSION['first_name'] != '') { echo $_SESSION['first_name']; unset($_SESSION['first_name']);}else { echo (isset($memberDetails->first_name)) ? $memberDetails->first_name : ''; }?>" id="first_name" placeholder="Enter First Name">
                            <br />
                            <label class="control-label" id="first_name_msg"></label>
                        </div>
                        <div class="form-group input-group col-md-4" id="last_name_msg_error">
                            <label for="last_name">Last name</label>
                            <input type="text" class="form-control"  maxlength="255" name="last_name" value="<?php if(isset($_SESSION['last_name']) && $_SESSION['last_name'] != '') { echo $_SESSION['last_name']; unset($_SESSION['last_name']);}else { echo (isset($memberDetails->last_name)) ? $memberDetails->last_name : ''; }?>" id="last_name" placeholder="Enter Last Name">
                        </div>
                        <div class="form-group input-group col-md-4" id="email_msg_error">
                            <label class="control-label" for="email">Email Address / Username<span class="required">*</span></label>
                            <input type="email" class="form-control" maxlength="255" name="email" value="<?php if(isset($_SESSION['email']) && $_SESSION['email'] != '') { echo $_SESSION['email']; unset($_SESSION['v']);}else { echo (isset($memberDetails->email)) ? $memberDetails->email : ''; }?>" id="email" placeholder="Enter Email address / Username">
                            <br />
                            <label class="control-label" id="email_msg"></label>
                        </div>
                        <?php if($action == 'Add'){ ?>
                            <div class="form-group input-group col-md-4" id="password_msg_error">
                                <label for="password" class="control-label" >Password<span class="required">*</span></label>
                                <input type="password" class="form-control" maxlength="255" name="password" value="" id="password" placeholder="Enter Password">
                                <br />
                                <label class="control-label" id="password_msg"></label>
                                <span id="loading_pass" class="aloader" style="display:none"><img src="<?php echo CSS_PATH?>img/ajax-loaders/ajax-loader-1.gif"></span> </div>
                            <div class="form-group input-group col-md-4" id="retype_password_msg_error">
                                <label for="retype_password" class="control-label" >Password again<span class="required">*</span></label>
                                <input type="password" class="form-control" maxlength="255" name="retype_password" value="" id="retype_password" placeholder="Enter Password again">
                                <br />
                                <label class="control-label" id="retype_password_msg"></label>
                                <span id="loading_repass" class="aloader" style="display:none"><img src="<?php echo CSS_PATH?>img/ajax-loaders/ajax-loader-1.gif"></span> </div>
                        <?php } ?>

                        <div class="form-group input-group col-md-4" id="date_of_birth_msg_error">
                            <label class="control-label" for="date_of_birth">Date Of Birth</label>
                            <input type="text" class="form-control" maxlength="255" name="date_of_birth" value="<?php if(isset($_SESSION['date_of_birth']) && $_SESSION['date_of_birth'] != '') { echo $_SESSION['date_of_birth']; unset($_SESSION['date_of_birth']);}else { echo (isset($memberDetails->date_of_birth) && $memberDetails->date_of_birth != '0000-00-00') ? date('Y-m-d',strtotime($memberDetails->date_of_birth)) : ''; }?>" id="date_of_birth" placeholder="Enter Date of Birth">
                            <br />
                            <label class="control-label" id="date_of_birth_msg"></label>
                        </div>


                        <div class="control-group">
                            <label class="control-label" for="selectError">Country</label>
                            <div class="controls">
                                <select id="country" name="country" data-rel="chosen">
                                    <option value="0" selected="selected">Select Country</option>
                                    <?php foreach($countryList as $item){ ?>
                                        <option value="<?php echo $item->id?>" <?php if(isset($_SESSION['country']) && $_SESSION['country'] == $item->id) { echo 'selected="selected"'; unset($_SESSION['country']); }else { echo (isset($memberDetails->country) && $memberDetails->country == $item->id) ? 'selected="selected"' : ''; }?> ><?php echo $item->name; ?></option>
                                    <?php }?>
                                </select>
                            </div>
                            <label class="control-label" id="country_msg"></label>
                        </div>


                        <div class="form-group input-group col-md-4" id="state_msg_error">
                            <label class="control-label" for="selectError">City</label>
                            <input type="text" class="form-control" maxlength="255" name="city" value="<?php if(isset($_SESSION['city']) && $_SESSION['city'] != '') { echo $_SESSION['city']; unset($_SESSION['city']);}else { echo (isset($memberDetails->city)) ? $memberDetails->city : ''; }?>" id="city" placeholder="Enter City">
                            <br />
                            <label class="control-label" id="city_msg"></label>
                        </div>

                        <?php if (!empty($memberDetails->active_package_id)) {?>
                        <div class="control-group">
                            <label class="control-label" for="selectError">Package</label>
                            <div class="controls">
                                <?php
                                    foreach ($packageList as $item) {
                                        if (isset($memberDetails->active_package_id) && $memberDetails->active_package_id == $item->id) {
                                            echo $item->title;
                                        }
                                    }
                                ?>
                            </div>
                            <label class="control-label" id="active_package_id_msg"></label>
                        </div>
                        <?php }?>




                        <div class="control-group">
                            <label class="control-label" for="selectError">Is Active</label>
                            <div class="controls">
                                <select id="is_active" name="is_active" data-rel="chosen">
                                    <option value="0" selected="selected">In Active</option>
                                    <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($memberDetails->is_active) && $memberDetails->is_active == 1) ? 'selected="selected"' : ''; }?>>Active</option>
                                </select>
                            </div>
                        </div>
                        <br />
                        <button type="submit" class="btn btn-success btn-sm">Submit</button>
                        <?php echo form_close(); ?> </div>
                </div>
            </div>
        </div>
    </div>
    <script language="javascript" type="text/javascript">
        function validate_member(){

            if($("#first_name").val()==''){
                $("#first_name_msg").html('Please enter first name');
                $("#first_name_msg_error").addClass('has-error');
                $("#first_name").focus();
                return false;
            }else{
                $("#first_name_msg").html('');
                $("#first_name_msg_error").removeClass('has-error');
            }

            if($("#email").val()==''){
                $("#email_msg").html('Please enter Email address');
                $("#email_msg_error").addClass('has-error');
                $("#email").focus();
                return false;
            }else if(!validateEmailRegexp($("#email").val()) ){

                $("#email_msg").html('Please enter valid Email address');
                $("#email_msg_error").addClass('has-error');
                $("#email").focus();
                return false;
            }else{
                $("#email_msg").html('');
                $("#email_msg_error").removeClass('has-error');
            }
            if($("#action").val() == 'Add') {
                var password = $("#password").val();
                var passed = validatePassword(password, {
                    length:   [6, Infinity],
                    numeric:  1,
                    special:  1
                });



                if($("#password").val()==''){
                    $("#password_msg").html('Please enter Password');
                    $("#password_msg_error").addClass('has-error');
                    $("#password").focus();
                    return false;



                }else if(!passed){

                    $("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char.');

                    $("#password_msg_error").addClass('has-error');

                    $("#password").focus();

                    return false;

                }else if($("#password").val()!='' && $("#password").val()!=$("#retype_password").val()){

                    $("#retype_password_msg").html('Password and Again password do not matched');

                    $("#retype_password_msg_error").addClass('has-error');

                    $("#retype_password").focus();

                    return false;

                }else{

                    $("#password_msg").html('');

                    $("#password_msg_error").removeClass('has-error');

                    $("#retype_password_msg").html('');

                    $("#retype_password_msg_error").removeClass('has-error');

                }
            }
            if($("#address_1").val()==''){
                $("#address_1_msg").html('Please enter address');
                $("#address_1_msg_error").addClass('has-error');
                $("#address_1").focus();
                return false;
            }else{
                $("#address_1_msg").html('');
                $("#address_1_msg_error").removeClass('has-error');
            }

        }
        $(document).ready(function(){

            $("#password").blur(function (){
                $("#loading_pass").show();
                var password = $("#password").val();
                var passed = validatePassword(password, {
                    length:   [6, Infinity],
                    numeric:  1,
                    special:  1
                });


                if($("#password").val()==''){
                    $("#password_msg").html('Please enter Password');
                    $("#password_msg_error").addClass('has-error');
                    $("#loading_pass").hide();
                    $("#password").focus();
                    return false;
                }else if(!passed){
                    $("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char. ');
                    $("#password_msg_error").addClass('has-error');
                    $("#loading_pass").hide();
                    $("#password").focus();
                    return false;
                }else{
                    $("#password_msg_error").removeClass('has-error');
                    $("#loading_pass").hide();
                    $("#password_msg").html('');
                }

            });
            $("#retype_password").blur(function (){
                $("#loading_repass").show();
                if($("#retype_password").val()!==$("#password").val()){
                    $("#retype_password_msg_error").addClass('has-error');
                    $("#retype_password_msg").html('Password and Again password do not matched');
                    $("#loading_repass").hide();
                }else{



                    $("#retype_password_msg").html('');

                    $("#retype_password_msg_error").removeClass('has-error');
                    $("#loading_repass").hide();
                }
            });
        });
        $(document).ready(function(){
            $( "#date_of_birth" ).datepicker({

                changeMonth: true,
                changeYear: true,
                maxDate: '0',
                minDate: '-100y',
                dateFormat: "yy-mm-dd"

            });
        });
    </script>
