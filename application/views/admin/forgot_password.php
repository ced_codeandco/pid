     
    <div class="row">
        <div class="col-md-12 center login-header">
            <h2>Welcome to <?php echo SITE_NAME?></h2>
        </div>
        <!--/span-->
    </div><!--/row-->

    <div class="row">
        <div class="well col-md-5 center login-box">
            <div class="alert alert-info">
                Please Email address for retrive password.
            </div>
            
           <?php 
			if(isset($errMsg) && $errMsg != ''){ ?>
			<div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
			<?php unset($errMsg);
			}
			if(isset($succMsg) && $succMsg != ''){ ?>
			<div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
			<?php unset($succMsg);
			}
			
				$attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
				echo form_open(ADMIN_ROOT_URL.'forgot_password',$attributes); ?>
                <fieldset>
                    <div class="input-group input-group-lg">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user red"></i></span>
                        <input  class="form-control" name="email" id="username" placeholder="Email Address">
                    </div>
                    <div class="clearfix"></div><br>

                    
                    

                    
                    <div class="clearfix"></div>

                    <p class="center col-md-5">
                        <button type="submit" name="submitLoginForm" id="submitLoginForm"  class="btn btn-primary">Login</button> &nbsp;                        
                    </p>
                </fieldset>
           <?php echo form_close(); ?>
            
            
           
                
                
        </div>
        <!--/span-->
    </div><!--/row-->