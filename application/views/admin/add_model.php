<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Model </a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i>  <?php echo $action;?>  Model For <?php echo $brandDetails->title?></h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	
		$editUrl = '/'.$brandDetails->id;
	if($action == 'Edit'){
		$editUrl .= '/'.$modelDetails->id;
	}
	
	$attributes = array('name' => 'modelForm', 'id' => 'modelForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_model();');
				echo form_open(ADMIN_ROOT_URL.'brand/add_model'.$editUrl,$attributes); ?>
         
          <input type="hidden" name="action" id="action" value="Add" />
          
          <div class="form-group input-group col-md-4" id="title_msg_error">
         
         
            <label class="control-label" for="title">Classified title : <?php echo $brandDetails->title?></label>
            
             <input type="hidden" class="form-control" maxlength="255" name="brand_id" value="<?php echo $brandDetails->id;?>" id="brand_id">
             <input type="hidden" name="id" id="id" value="<?php echo (isset($modelDetails->id)) ? $modelDetails->id : 0;?>" />
          </div>
          
          
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($modelDetails->title)) ? $modelDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>
          
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($modelDetails->is_active) && $modelDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_model(){	
	if($("#brand_model").val()==''){
		$("#brand_model_msg").html('Please Select Image');
		$("#brand_model_msg_error").addClass('has-error');
		$("#brand_model").focus();
		return false;
	}else{
		$("#brand_model_msg").html('');
		$("#brand_model_msg_error").removeClass('has-error');
	}
}
</script> 
