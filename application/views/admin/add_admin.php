<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Admin</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-user"></i> <?php echo $action;?> Admin</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$adminDetails->id;
	}
	$attributes = array('name' => 'adminForm', 'id' => 'adminForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_admin();');
				echo form_open(ADMIN_ROOT_URL.'admin/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($adminDetails->id)) ? $adminDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          <div class="form-group input-group col-md-4" id="first_name_msg_error">
            <label class="control-label" for="first_name">First name</label>
            <input type="text" class="form-control" maxlength="255" name="first_name" value="<?php if(isset($_SESSION['first_name']) && $_SESSION['first_name'] != '') { echo $_SESSION['first_name']; unset($_SESSION['first_name']);}else { echo (isset($adminDetails->first_name)) ? $adminDetails->first_name : ''; }?>" id="first_name" placeholder="Enter First Name">
            <br />
            <label class="control-label" id="first_name_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="last_name_msg_error">
            <label for="last_name">Last name</label>
            <input type="text" class="form-control"  maxlength="255" name="last_name" value="<?php echo (isset($adminDetails->last_name)) ? $adminDetails->last_name : '';?>" id="last_name" placeholder="Enter Last Name">
          </div>
          <div class="form-group input-group col-md-4" id="email_msg_error">
            <label for="email">Email Address / Username</label>
            <input type="email" class="form-control" maxlength="255" name="email" value="<?php echo (isset($adminDetails->email)) ? $adminDetails->email : '';?>" id="email" placeholder="Enter Email address / Username">
            <br />
            <label class="control-label" id="email_msg"></label>
          </div>
          <?php if($action == 'Add'){ ?>
          <div class="form-group input-group col-md-4" id="password_msg_error">
            <label for="password">Password</label>
            <input type="password" class="form-control" maxlength="255" name="password" value="" id="password" placeholder="Enter Password">
            <br />
            <label class="control-label" id="password_msg"></label>
            <span id="loading_pass" class="aloader" style="display:none"><img src="<?php echo CSS_PATH?>img/ajax-loaders/ajax-loader-1.gif"></span> </div>
          <div class="form-group input-group col-md-4" id="retype_password_msg_error">
            <label for="retype_password">Password again</label>
            <input type="password" class="form-control" maxlength="255" name="retype_password" value="" id="retype_password" placeholder="Enter Password again">
            <br />
            <label class="control-label" id="retype_password_msg"></label>
            <span id="loading_repass" class="aloader" style="display:none"><img src="<?php echo CSS_PATH?>img/ajax-loaders/ajax-loader-1.gif"></span> </div>
          <?php } ?>
          <?php if(!isset($adminDetails->id) || (isset($adminDetails->id) && $adminDetails->id!=1) ){ 
					$accessArr = array();
						
						
						if(isset($adminDetails->module_access) && $adminDetails->module_access!=''){
							$accessArr = explode(',',$adminDetails->module_access);	
						}
					?>
          
          <!-- Permissions -->
              <div class="form-group input-group col-md-4" id="retype_password_msg_error">
                  <label for="retype_password">Module Permission</label><?php
                  if (!empty($adminDetails->admin_role) && $adminDetails->admin_role == 'Super Admin') {?>
                      <div class="checkbox">
                          <label>
                              <input type="checkbox" onclick="return false"
                                     value="Full" checked='checked'
                                     name="module_access[]" id="module_access[]">Full </label>
                      </div>
                          <?php
                  } else {
                      if ($this->headerData['activeAdminDetails']->module_access[0] == 'FULL') {?>
                          <div class="checkbox">
                              <label>
                                  <input type="checkbox" value="Full" name="module_access_full" id="module_access_full" <?php echo (in_array('FULL', $accessArr)) ? "checked='checked'" : '' ?>>
                                  Full </label>
                          </div><?php

                      }
                      foreach ($adminModuleList as $key => $val) { ?>
                          <div class="checkbox">
                              <label>
                                  <input type="checkbox"
                                         value="<?php echo $val->id ?>" <?php echo ($accessArr && in_array($val->id, $accessArr)) ? "checked='checked'" : '' ?>
                                         name="module_access[]" id="module_access[]" class="module_access_partial">
                                  <?php echo $val->module_name; ?> </label>
                          </div>
                      <?php }
                  }
                  ?>
              </div>
          <?php } ?>
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php echo (isset($adminDetails->is_active) && $adminDetails->is_active==1) ? "selected='selected'" : "" ?> >Active</option>
              </select>
            </div>
          </div>
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_admin(){
	
	if($("#first_name").val()==''){
		$("#first_name_msg").html('Please enter first name');
		$("#first_name_msg_error").addClass('has-error');
		$("#first_name").focus();
		return false;
	}else{
		$("#first_name_msg").html('');
		$("#first_name_msg_error").removeClass('has-error');
	}
	
	if($("#email").val()==''){
		$("#email_msg").html('Please enter Email address');
		$("#email_msg_error").addClass('has-error');
		$("#email").focus();
		return false;
	}else if(!validateEmailRegexp($("#email").val()) ){
		
		$("#email_msg").html('Please enter valid Email address');
		$("#email_msg_error").addClass('has-error');
		$("#email").focus();
		return false;
	}else{
		$("#email_msg").html('');
		$("#email_msg_error").removeClass('has-error');
	}
if($("#action").val() == 'Add') {
		var password = $("#password").val();
	
		var passed = validatePassword(password, {
	
			length:   [6, Infinity],
	
			numeric:  1,
	
			special:  1
	
		});
	
		
	
		if($("#password").val()==''){
	
			$("#password_msg").html('Please enter Password');
	
			$("#password_msg_error").addClass('has-error');
	
			$("#password").focus();
	
			return false;
	
			
	
		}else if(!passed){
	
			$("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char.');
	
			$("#password_msg_error").addClass('has-error');
	
			$("#password").focus();
	
			return false;
	
		}else if($("#password").val()!='' && $("#password").val()!=$("#retype_password").val()){
	
			$("#retype_password_msg").html('Password and Again password do not matched');
	
			$("#retype_password_msg_error").addClass('has-error');
	
			$("#retype_password").focus();
	
			return false;
	
		}else{
	
			$("#password_msg").html('');
	
			$("#password_msg_error").removeClass('has-error');
			
			$("#retype_password_msg").html('');
	
			$("#retype_password_msg_error").removeClass('has-error');
	
		}
	}
	
}
$(document).ready(function(){
	
	$("#password").blur(function (){
		$("#loading_pass").show();
		var password = $("#password").val();
		var passed = validatePassword(password, {
			length:   [6, Infinity],
			numeric:  1,
			special:  1
		});
		
		if($("#password").val()==''){
			$("#password_msg").html('Please enter Password');
			$("#password_msg_error").addClass('has-error');
			$("#loading_pass").hide();
			$("#password").focus();
			return false;
		}else if(!passed){
			$("#password_msg").html('Password should have minimum 6 char and atleast one numeric or one special char. ');
			$("#password_msg_error").addClass('has-error');
			$("#loading_pass").hide();
			$("#password").focus();
			return false;
		}else{
			$("#password_msg_error").removeClass('has-error');
			$("#loading_pass").hide();
			$("#password_msg").html('');
  		}
  		
 	});
	$("#retype_password").blur(function (){
		$("#loading_repass").show();
		if($("#retype_password").val()!==$("#password").val()){
			$("#retype_password_msg_error").addClass('has-error');
			$("#retype_password_msg").html('Password and Again password do not matched');
	  		$("#loading_repass").hide();
	  	}else{
			
			
			
			$("#retype_password_msg").html('');
	
			$("#retype_password_msg_error").removeClass('has-error');
	  		$("#loading_repass").hide();
  		}
	});
    $('#module_access_full').click(function(){
        if ($(this).is(':checked')) {
            $('.module_access_partial').prop('checked', false);
        }
    })
    $('.module_access_partial').click(function(){
        if ($(this).is(':checked')) {
            $('#module_access_full').prop('checked', false);
        }
    })
});
</script> 
