<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Tips and Tricks Page</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class="glyphicon glyphicon-list-alt"></i> <?php echo $action;?> Tips and Tricks Page</h2>
          <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php
          //print_r($tipsAndTricksDetails);
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$tipsAndTricksDetails->id;
	}
	
	$attributes = array('name' => 'classifiedForm', 'id' => 'classifiedForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_classified();');
				echo form_open('',$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($tipsAndTricksDetails->id)) ? $tipsAndTricksDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          <div class="control-group">
            <label class="control-label" for="selectError">Category Id<span class="required">*</span></label>
            <div class="controls">
              <select id="category_id" name="category_id" data-rel="chosen" onchange="loadTipsSubCategory(this, this.value, true)" >
			  	<?php echo $contentSelectData;?>
              </select>
            </div>
          </div>



          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(!empty($_POST['title'])) { echo $_POST['title']; }else { echo (isset($tipsAndTricksDetails->title)) ? $tipsAndTricksDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>

            <div class="form-group input-group col-md-4" id="short_description_msg_error">
                <label for="short_description">Short Description</label>
                <textarea class="form-control"  maxlength="400" name="short_description"  id="short_description" placeholder="Short Description"><?php if(isset($_SESSION['short_description']) && $_SESSION['short_description'] != '') { echo $_SESSION['short_description']; unset($_SESSION['short_description']);}else { echo (isset($tipsAndTricksDetails->short_description)) ? $tipsAndTricksDetails->short_description : ''; }?></textarea>
            </div>

            <div class="form-group input-group col-md-4" id="description_msg_error">
                <label for="description">Description</label><br />
                <?php if(isset($_SESSION['description']) && $_SESSION['description'] != '') { $description =  $_SESSION['description']; unset($_SESSION['description']);}else { $description =  (isset($tipsAndTricksDetails->description)) ? stripslashes($tipsAndTricksDetails->description) : ''; }?>
                <?php echo $this->ckeditor->editor("description",$description);?>
            </div>

            <div class="form-group input-group col-md-4" id="image_msg_error">
                <label for="image">Image</label><br />

                <input type="file" name="image" id="image" class="input-text-02"   />
                <?php if(isset($tipsAndTricksDetails->image) && $tipsAndTricksDetails->image!='' && file_exists(DIR_UPLOAD_BLOG.$tipsAndTricksDetails->image)) {?>

                    <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_BLOG_SHOW.$tipsAndTricksDetails->image ?>&q=100&w=300"/>
                    <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $tipsAndTricksDetails->image;  ?>" />
                <?php } ?>

            </div>

          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($tipsAndTricksDetails->is_active) && $tipsAndTricksDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<input type="hidden" id="rootUrl" value="<?php echo ROOT_URL;?>">
<script language="javascript" type="text/javascript">
$(document).ready(function(){
    if (parseInt($('#category_id').val()) != <?php echo RECIPES_ID;?>) {
        //$('#sub-category-wrap').hide();
    }
})
function validate_classified(){
    if (parseInt($('#category_id').val()) == <?php echo RECIPES_ID;?>) {

    }


    if($("[name='country_id']").val()==''){
        $("#country_id_msg").html('Please select country');
        $("#country_id_msg_error").addClass('has-error');
        $("[name='country_id']").focus();
        return false;
    }else{
        $("#country_id_msg").html('');
        $("#country_id_msg_error").removeClass('has-error');
    }
    $('textarea[name="description"]').each(function () {
        var data = CKEDITOR.instances['description'].getData();
        $('textarea[name="description"]').val(data);
    });
}

    $(document).ready(function(){
        var category = $('#category_id').val();//$subCategoryList
        if ($('#selected-sub-category').length > 0) {
            var currentId = $('#selected-sub-category').val();
        }
        loadTipsSubCategory($('#category_id'), category, true, currentId)
        if (typeof currentId != 'undefined') {
            $('#subCategoryContainer').find('select').val(currentId);
        }
    })
</script> 
