<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckeditor/ckeditor.js"></script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>assets/ckfinder/ckfinder.js"></script>
<div id="content" class="col-lg-10 col-sm-10">
<!-- content starts -->
<div>
  <ul class="breadcrumb">
    <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
    <li> <a href="#"><?php echo $action;?> Advertize</a> </li>
  </ul>
</div>
<div class="row">
  <div class="box-content">
    <div class="box col-md-12">
      <div class="box-inner">
        <div class="box-header well" data-original-title="">
          <h2><i class=" glyphicon glyphicon-picture"></i> <?php echo $action;?> Advertize</h2>
          
        </div>
        <div class="box-content"> <?php echo validation_errors(); ?>
          <?php 
	$editUrl = '';
	if($action == 'Edit'){
		$editUrl = '/'.$imageDetails->id;
	}
	
	$attributes = array('name' => 'cmsForm', 'id' => 'cmsForm', 'enctype' => 'multipart/form-data', 'role'=>'form', 'onsubmit'=>'return validate_cms();');
				echo form_open(ADMIN_ROOT_URL.'advertize/add'.$editUrl,$attributes); ?>
          <input type="hidden" name="id" id="id" value="<?php echo (isset($imageDetails->id)) ? $imageDetails->id : 0;?>" />
          <input type="hidden" name="action" id="action" value="<?php echo $action?>" />
          
          
          <div class="form-group input-group col-md-4" id="title_msg_error">
            <label class="control-label" for="title">Title<span class="required">*</span></label>
            <input type="text" class="form-control" maxlength="255" name="title" value="<?php if(isset($_SESSION['title']) && $_SESSION['title'] != '') { echo $_SESSION['title']; unset($_SESSION['title']);}else { echo (isset($imageDetails->title)) ? $imageDetails->title : ''; }?>" id="title" placeholder="Enter Title">
            <br />
            <label class="control-label" id="title_msg"></label>
          </div>
          
          <div class="form-group input-group col-md-4" id="advertize_url_msg_error">
            <label class="control-label" for="advertize_url">Advertize URL</label><br>
           <label style="padding-top: 10px;"> http://</label><input type="text" class="form-control" style="float: right;width: 90%;"  maxlength="255" name="advertize_url" value="<?php if(isset($_SESSION['advertize_url']) && $_SESSION['advertize_url'] != '') { echo $_SESSION['advertize_url']; unset($_SESSION['advertize_url']);}else { echo (isset($imageDetails->title)) ? $imageDetails->advertize_url : ''; }?>" id="advertize_url" placeholder="Enter Url">
            <br />
            <label class="control-label" id="advertize_url_msg"></label>
          </div>
          <div class="form-group input-group col-md-4" id="image_path_msg_error">
            <label for="image_path">Banner Image</label><br />
            <input type="file" name="image_path" id="image_path" class="input-text-02"   />
      <?php if(isset($imageDetails->image_path) && $imageDetails->image_path!='' && file_exists(DIR_UPLOAD_ADVERTIZE.$imageDetails->image_path)) {?>
     
      <img src="<?php echo ROOT_URL_BASE?>assets/timthumb.php?src=<?php echo DIR_UPLOAD_ADVERTIZE_SHOW.$imageDetails->image_path ?>&q=100&w=150"/>
      <input type="hidden" id="uploaded_file" name="uploaded_file" value="<?php echo $imageDetails->image_path;  ?>" />
      <?php } ?>
        
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Type</label>
            <div class="controls">
              <select id="advertize_type" name="advertize_type" data-rel="chosen">
                <option value="Square" selected="selected">Square &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                <!--<option value="Full Width" <?php /*if(isset($_SESSION['advertize_type']) && $_SESSION['advertize_type'] == 'Full Width') { echo 'selected="selected"'; unset($_SESSION['advertize_type']); }else { echo (isset($imageDetails->advertize_type) && $imageDetails->advertize_type == 'Full Width') ? 'selected="selected"' : ''; }*/?> >Full Width&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>-->
              </select>
            </div>
          </div>
          <div class="control-group">
            <label class="control-label" for="selectError">Is Active</label>
            <div class="controls">
              <select id="is_active" name="is_active" data-rel="chosen">
                <option value="0" selected="selected">In Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
                <option value="1" <?php if(isset($_SESSION['is_active']) && $_SESSION['is_active'] == 1) { echo 'selected="selected"'; unset($_SESSION['is_active']); }else { echo (isset($imageDetails->is_active) && $imageDetails->is_active == 1) ? 'selected="selected"' : ''; }?> >Active&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</option>
              </select>
            </div>
          </div>
         
          
          <br />
          <button type="submit" class="btn btn-success btn-sm">Submit</button>
          <?php echo form_close(); ?> </div>
      </div>
    </div>
  </div>
</div>
<script language="javascript" type="text/javascript">
function validate_cms(){	
	if($("#title").val()==''){
		$("#title_msg").html('Please enter Home Page Banner title');
		$("#title_msg_error").addClass('has-error');
		$("#title").focus();
		return false;
	}else{
		$("#title_msg").html('');
		$("#title_msg_error").removeClass('has-error');
	}
}
</script> 
