<div id="content" class="col-lg-10 col-sm-10">
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#">Contact Requests</a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-user"></i> Contact Requests</h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <?php if(isset($successMsg) && $successMsg != ''){?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $successMsg; unset($successMsg);?></div>
                        <?php } ?>
                        <?php if(isset($errMsg) && $errMsg != ''){?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $errMsg; unset($errMsg);?></div>
                        <?php } ?>
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
                            <thead>
                            <tr>

                                <th width="5%">Order</th>
                                <th width="15%">Name</th>
                                <th width="15%">Email</th>
                                <th width="31%">Message</th>
                                <th width="12%">Time</th>
                                <th width="12%">IP address</th>
                                <th width="10%" style="text-align:center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 0;
                            if($contactsList && count($contactsList) > 0 ){
                                $paOrder =1;

                                foreach ($contactsList as $contacts){  ?>
                                    <tr>

                                        <td><?php echo $paOrder; ?> </td>
                                        <td><?php echo $contacts->name;?></td>
                                        <td><?php echo $contacts->email;?></td>
                                        <td><?php echo $contacts->comments;?></td>
                                        <td><?php echo $contacts->created_date_time;?></td>
                                        <td><?php echo $contacts->created_ip;?></td>
                                        <td class="t-center">
                                            <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>contacts/delete/<?php echo $contacts->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
                                        </td>
                                    </tr>

                                    <?php $paOrder++; }

                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>