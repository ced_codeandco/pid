<div id="content" class="col-lg-10 col-sm-10">
    <div>
        <ul class="breadcrumb">
            <li> <a href="<?php echo ADMIN_ROOT_URL?>">Home</a> </li>
            <li> <a href="#">News List</a> </li>
        </ul>
    </div>
    <div class="row">
        <div class="box-content">
            <div class="box col-md-12">
                <div class="box-inner">
                    <div class="box-header well" data-original-title="">
                        <h2><i class="glyphicon glyphicon-user"></i> News List</h2>
                        <div class="box-icon"> <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a> <a href="#" class="btn btn-minimize btn-round btn-default"><i
                                    class="glyphicon glyphicon-chevron-up"></i></a> <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a> </div>
                    </div>
                    <div class="box-content">
                        <?php if(isset($successMsg) && $successMsg != ''){?>
                            <div class="alert alert-success">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $successMsg; unset($successMsg);?></div>
                        <?php } ?>
                        <?php if(isset($errMsg) && $errMsg != ''){?>
                            <div class="alert alert-danger">
                                <button data-dismiss="alert" class="close" type="button">×</button>
                                <?php echo $errMsg; unset($errMsg);?></div>
                        <?php } ?>
                        <table class="table table-striped table-bordered bootstrap-datatable datatable responsive" id="datatable_list">
                            <thead>
                            <tr>

                                <th width="10%">Order</th>
                                <th width="22%">Title</th>
                                <th width="22%">Short Description</th>
                                <!--<th width="20%">Slug</th>-->
                                <th width="12%" style="text-align:center">Status</th>
                                <th width="20%" style="text-align:center">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php
                            $i = 0;
                            if($newsList && count($newsList) > 0 ){
                                $paOrder =1;

                                foreach ($newsList as $pages){  ?>
                                    <tr>

                                        <td><?php echo $paOrder; ?>

                                            <?php if($paOrder!=1){?>
                                                <a href="javascript:changeOrderCMS('<?php echo $pages->id ?>','<?php echo $pages->sort_order ?>','Up',<?php echo $pages->parent_id ?>);">
                                                    <img src="<?php echo ROOT_URL_BASE?>images/admin/uparrow.png" alt="Up" width="16" height="16" border="0" /></a>
                                            <?php }?>
                                            <?php if($paOrder!=count($newsList)){?>
                                                <a href="javascript:changeOrderCMS('<?php echo $pages->id ?>','<?php echo $pages->sort_order ?>','Dn',<?php echo $pages->parent_id ?>);">
                                                    <img src="<?php echo ROOT_URL_BASE?>images/admin/downarrow.png" alt="Down" width="16" height="16" border="0" /></a>
                                            <?php }?>
                                        </td>
                                        <td><a href="<?php echo ADMIN_ROOT_URL?>news/add/<?php echo $pages->id?>" ><?php echo $pages->title;  ?></a></td>
                                        <td><?php echo $pages->small_description;  ?></td>
                                        <!--<td><?php /*echo $pages->url_slug;  */?></td>-->

                                        <td style="text-align:center" id="td_status_<?php echo $pages->id ?>">

                                            <?php if(!in_array(trim($pages->url_slug),$noneEditPage)) {?>
                                                <?php if($pages->is_active=='1'){?>
                                                    <a href="<?php echo ADMIN_ROOT_URL?>news/status_inactive/<?php echo $pages->id?>" class="label-success label label-default" >Active</a>
                                                <?php }else{?>
                                                    <a href="<?php echo ADMIN_ROOT_URL?>news/status_active/<?php echo $pages->id?>" class="label-default label label-danger"  >In Active</a>
                                                <?php }?>
                                            <?php }else{ ?>
                                                <?php if($pages->is_active=='1'){?>
                                                    <span class="label-success label label-default" >Active</span>
                                                <?php }else{?>
                                                    <span class="label-default label label-danger" >In Active</span>
                                                <?php }?>
                                            <?php } ?>
                                        </td>
                                        <td class="t-center">
                                            <a href="<?php echo ADMIN_ROOT_URL?>news/add/<?php echo $pages->id?>" class="btn btn-info"> <i class="glyphicon glyphicon-edit icon-white"></i> Edit</a>
                                            <?php if(!in_array(trim($pages->url_slug),$noneEditPage)) {?>
                                                <a class="btn btn-danger" href="#" onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo ADMIN_ROOT_URL?>news/delete/<?php echo $pages->id?>'}"> <i class="glyphicon glyphicon-trash icon-white"></i> Delete </a>
                                            <?php } ?>
                                        </td>
                                    </tr>

                                    <?php $paOrder++; }

                            } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    <script type="text/javascript">
        function changeOrderCMS(id,sort_order,position,parent)
        {
            location.href ="<?php echo ADMIN_ROOT_URL?>news/order?id="+id+"&sort_order="+sort_order+"&position="+position+"&parent="+parent;
        }

    </script>