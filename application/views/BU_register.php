<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL.(!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">

<div class="main-wraper">

    <div class="container">

        <div class="pages">

            <ul class="breadcrumbs"><li><a href="<?php echo ROOT_URL;?>">Home</a></li><li>Sign In / Register</li></ul>

            <h2 class="page_heading">Sign In / Register</h2>

            <h1 class="page_heading2">Alredy have an <span>P&ID</span> account? Please Sign in.</h1>

            <div class="pages_left">
                <form method="post" action="<?php echo ROOT_URL.'register';?>"  name="registerForm" id="registerForm" class="register-form user-registration">
                <div class="registration_left">

                    <h2>Create a free account</h2>
                    <?php echo validation_errors();?>
                    <!--<a><img src="images/fb_login.png" /></a>

                    <div class="right_fb_reg"><span>James Brown, Nicolas Cage</span> and 7 other <br />friends use <a href="#">negaderas.com</a>.</div>

                    <div class="or_div">or</div>-->

                    <h2><span class="requried_span">Note: All fields are mandatory</span></h2>

                    <div class="input_div"><label>Email Address*</label>
                        <input  class="form-control" placeholder="username@provider.com" type="text" name="email" value="<?php echo !empty($formData['email']) ? $formData['email'] : ''?>" id="email" onblur="validate_user_name(this, '<?php echo ROOT_URL.'register/check_duplicate_email';?>', 'label', 'error')">
                    </div>

                    <div class="input_div input_div_right"><label>Confirm Email Address*</label>
                        <input  class="form-control" placeholder="username@provider.com" type="text" name="email_again" value="<?php echo !empty($formData['email_again']) ? $formData['email_again'] : ''?>">
                    </div>
<div class="clearfix"></div>
                    <div class="input_div"><label>Password*</label>
                        <input  class="form-control" type="password" class="validate[required] inputbg" placeholder="Password" name="password" id="password"  value="">
                    </div>

                    <div class="input_div input_div_right"><label>Confirm Password*</label>
                        <input  class="form-control" type="password" class="validate[required] inputbg" placeholder="Password" name="password_again" value="">
                    </div>

                    <div class="input_div"><label>First Name*</label>
                        <input  class="form-control" placeholder="Enter your real name" class="validate[required] inputbg" type="text" name="first_name" value="<?php echo !empty($formData['first_name']) ? $formData['first_name'] : ''?>" >
                    </div>

                    <div class="input_div input_div_right"><label>Last Name*</label>
                        <input  class="form-control" placeholder="Your last name will be kept private" class="validate[required] inputbg" type="text" name="last_name" value="<?php echo !empty($formData['last_name']) ? $formData['last_name'] : ''?>" >
                    </div>

                    <div class="input_div"><label>Select your question*</label>
                        <select name="security_question">
                            <option value="">Select one</option>
                            <?php
                            if (!empty($questionList)) {
                                foreach ($questionList as $question) {
                                    echo '<option '.((!empty($formData['security_question']) && $formData['security_question'] == $question->question) ? "selected='selected'" : "").'>' . $question->question . '</option>';
                                }
                            }?>
                        </select>
                    </div>

                    <div class="input_div input_div_right"><label>Secret Answer*</label>
                        <input  class="form-control" placeholder="Secreat Answer(Minimum 5 characters)" class="validate[required] inputbg" type="text" name="security_answer" value="<?php echo !empty($formData['security_answer']) ? $formData['security_answer'] : ''?>" >
                    </div>

                    <div class="input_div ui-scope-wrap"><label>Date of Birth*</label>
                        <input  class="form-control" id="datePicker" placeholder="Date of birth" class="validate[required] inputbg" type="text" name="date_of_birth" value="<?php echo !empty($formData['date_of_birth']) ? $formData['date_of_birth'] : ''?>" >
                    </div>
                    <div class="input_div input_div_right"><label>Select Country*</label>
                        <select name="country" id="country">
                            <option value="">Select</option>
                            <?php if (is_array($cityList)) {
                                foreach ($countryList as $country) {
                                    echo '<option value="' .$country->id. '" ' .((!empty($formData['country']) && $formData['country'] == $country->id) ? 'selected="selected"' : ''). '>' .$country->name. '</option>';
                                }
                            }?>
                        </select>
                    </div>

                    <div class="input_div"><label>City*</label>
                        <input  class="form-control" placeholder="City" class="validate[required] inputbg" type="text" name="city" value="<?php echo !empty($formData['city']) ? $formData['city'] : ''?>" >
                        <?php /*<select name="city" id="city">
                            <option value="">Select</option>
                            <?php if (is_array($cityList)) {
                                foreach ($cityList as $city) {
                                    echo '<option value="' .$city->id. '" ' .((!empty($formData['city']) && $formData['city'] == $city->id) ? 'selected="selected"' : ''). '>' .$city->title. '</option>';
                                }
                            }?>
                        </select>*/?>
                    </div>

                    <div class="border_div"></div>

                    <input type="checkbox" id="send_updates" name="send_updates" value="1" /><label for="send_updates">Allow P&ID to send me occasional updates about the site. </label>

                    <input type="checkbox" id="send_amazing" name="send_amazing" value="1" /><label for="send_amazing">Send me amazing offers and bargains from our advertising partners.</label>

                    <div class="border_div"></div>

                    <input type="submit" value="Register" />

            <span class="reg_term">By clicking on Register, you agree to the<br />

P&ID <a href="#">Terms and Conditions</a> and the P&ID <a href="#">Privacy Policy</a>.</span>

                    <div class="clearfix"></div>

                </div>
                </form>
            </div>

            <div class="pages_right">



            </div>

        </div>



    </div>

</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<link href="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.css" rel="stylesheet">
<script src="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">
    $(document).ready(function(){

    })
    $(function() {

        $( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear()
        });
        /*$( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear()
        });*/

        $.validator.addMethod("customvalidation",
            function(value, element) {
                return /^[A-Za-z\d=#$%@_ -]+$/.test(value);
            },
            "Sorry, no special characters allowed"
        );
        $.validator.addMethod("validPhoneNumber", function(value, element) {
            //$(element).val(value.trim());
            return this.optional(element) || validPhoneNumber(value);
        }, "Invalid phone number");
        $('#registerForm').validate({
            rules: {
                email:{required: true, email:true, maxlength:100},
                email_again:{required: true, email:true, equalTo: '#email', maxlength:100},
                password:{required: true, maxlength:100, minlength:5},
                password_again:{required: true, maxlength:100 ,equalTo: '#password'},
                first_name: {required: true, maxlength:100},
                last_name: {required: true, maxlength:100},
                security_question:{required: true},
                security_answer:{required: true, maxlength:100, minlength:5 },
                date_of_birth:{required: true, date:true},
                contact_no:{required: true, maxlength: 15, validPhoneNumber: true, maxlength:10},
                country:{required: true},
                city:{required: true},
                age_confirmation:{required: true},
            },
            messages: {
                first_name: {required: 'Please enter your first name'},
                last_name: {required: 'Please enter your last name'},
                email:{required: 'Please enter a valid email', email:'Invalid/Incomplete Email ID'},
                email_again:{required: 'Please enter a valid email', email:'Invalid/Incomplete Email ID', equalTo:'Email mismatch'},
                password:{required: 'Please enter a password', minlength:'Your password should contain at least 5 character'},
                password_again:{required: 'Please confirm your password', equalTo: 'Password mismatch'},
                security_question:{required: 'Please select a security question'},
                security_answer:{required: 'Please enter your answer to security  question' },
                /*gender:{required: 'Please select your gender'},*/
                date_of_birth:{required: 'Please enter your date of birth'},
                contact_no:{required: 'Please enter your contact number', maxlength: 'Invalid phone number', validPhoneNumber: 'Invalid phone number'},
                address_1:{required: 'Address 1 is required'},
                city:{required: 'Please select your city'},
                country:{required: 'Please select your country'},
                age_confirmation:{required: 'Please confirm your age'},
            }
        })
    });
</script>


<!--<link rel="stylesheet" href="<?php echo ROOT_URL_BASE;?>css/themes/base/jquery.ui.all.css">
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.core.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE;?>js/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript">
    $(function() {
        $( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear()
        });
    });
</script>-->

