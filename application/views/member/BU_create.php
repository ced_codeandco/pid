<div class="main-wraper">

    <div class="container">

        <div class="pages">

            <ul class="breadcrumbs"><li><a href="#">Home</a></li><li>Submit a file</li></ul>

            <h2 class="page_heading">Submit a file</h2>



            <div class="pages_left">
                <form class="register-form" name="createClassified" method="post" id="createClassified" enctype="multipart/form-data">
                    <input type="hidden" name="created_by" value="<?php echo $user_id;?>" />
                <div class="registration_left">

                    <h1 class="page_heading2">Submit a <span>File</span></h1>
                    <div class="clearfix"></div>
                    <?php  echo validation_errors();?>


                    <div class="input_div2"><label>Ad Title*</label>
                        <input value="" placeholder="Title" class="validate[required] inputbg" type="text" name="title" id="title" />
                    </div>



                    <div class="clearfix"></div>

                    <div class="input_div2 file-uplopader-row"><label>Upload file</label>

                        <ul class="upload_images_ul">

                            <li><input type="file" name="myfile"  /><span class="placeme"></span></li>

                        </ul>
                        <label id="myfile-error" class="error hidden" for="myfile"></label>
                    </div>

                    <div class="border_div"></div>

                    <!--<span class="reg_term1">By clicking 'Submit' you accept our <a href="#">Terms of Use</a> & <a href="#">Posting Rules</a>.</span>-->

                    <input type="submit" value="SUBMIT" />

                    <div class="clearfix"></div>
                </div>
                </form>

            </div>


        </div>



    </div>

</div>
<script src="<?php echo ROOT_URL_BASE; ?>assets/ajax_fileupload/js/vendor/jquery.ui.widget.js"></script>
<script src="<?php echo ROOT_URL_BASE; ?>assets/ajax_fileupload/js/jquery.iframe-transport.js"></script>
<script src="<?php echo ROOT_URL_BASE; ?>assets/ajax_fileupload/js/jquery.fileupload.js"></script>
<!--<script src="<?php /*echo ROOT_URL_BASE; */?>assets/ajax_fileupload/js/bootstrap.min.js"></script>-->

<script src="<?php echo ROOT_URL_BASE; ?>assets/ajax_fileupload/js/fileuploadscript.js"></script>
<script type="text/javascript">
$(document).ready(function(){



    jQuery('input:file').change(function () {

        var fileName = $(this).val();

        fileParts = fileName.split("\\");

        if (typeof fileParts[2] != 'undefined') {

            fileName = fileParts[2];

        }

        jQuery(this).parent().find('.placeme').text(fileName);

    });
})
</script>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script type="text/javascript">
$(document).ready(function(){
    jQuery.validator.addMethod("validPhoneNumber", function(value, element) {
        //$(element).val(value.trim());
        return this.optional(element) || validDubaiPhoneNumber(value);
    }, "Invalid phone number");
    $("#createClassified").validate({
        rules: {
            title:{
                required: true,
                maxlength: 200
            },
            /*'myfile':{
                required: true,
            },*/

        },
        messages: {
            title:{
                required: 'Please enter a title',
                maxlength: 'Maximum length allowed is 200'
            },
            'myfile':{
                required: 'Please select your file',
            }
        }
    });
})
<?php
    $classifiedDetails = (array) $classifiedDetails;
    if(!empty($classifiedDetails) && is_array($classifiedDetails)) {?>
    $(document).ready(function(){
        <?php echo build_filldata_javascript($classifiedDetails);?>
    })
<?php }?>
</script>
