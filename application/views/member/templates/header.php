<!DOCTYPE html>
<html lang="en">
<head>
<!--
        ===
        This comment should NOT be removed.

        Charisma v2.0.0

        Copyright 2012-2014 Muhammad Usman
        Licensed under the Apache License v2.0
        http://www.apache.org/licenses/LICENSE-2.0

        http://usman.it
        http://twitter.com/halalit_usman
        ===
    -->
<meta charset="utf-8">

<title><?php echo $title.' - '.SITE_NAME;?> Member</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="<?php echo $title.' - '.SITE_NAME;?>">
<meta name="author" content="Muhammad Munir">

<!-- The styles -->
<link id="bs-css" href="<?php echo ADMIN_CSS_PATH?>bootstrap-cerulean.min.css" rel="stylesheet">
<link href="<?php echo ROOT_URL_BASE?>css/admin/charisma-app.css" rel="stylesheet">
<link href='<?php echo ROOT_URL_BASE?>bower_components/fullcalendar/dist/fullcalendar.css' rel='stylesheet'>
<link href='<?php echo ROOT_URL_BASE?>bower_components/fullcalendar/dist/fullcalendar.print.css' rel='stylesheet' media='print'>
<link href='<?php echo ROOT_URL_BASE?>bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='<?php echo ROOT_URL_BASE?>bower_components/colorbox/example3/colorbox.css' rel='stylesheet'>
<link href='<?php echo ROOT_URL_BASE?>bower_components/responsive-tables/responsive-tables.css' rel='stylesheet'>
<link href='<?php echo ROOT_URL_BASE?>bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>jquery.noty.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>noty_theme_default.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>elfinder.min.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>elfinder.theme.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>jquery.iphone.toggle.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>uploadify.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>animate.min.css' rel='stylesheet'>
<link href='<?php echo ADMIN_CSS_PATH?>common.css' rel='stylesheet'>

<!-- jQuery -->
<script src="<?php echo ROOT_URL_BASE?>bower_components/jquery/jquery.min.js"></script>
<script src="<?php echo ROOT_URL_BASE?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
<!-- library for cookie management -->
<script src="<?php echo ADMIN_JS_PATH?>jquery.cookie.js"></script>
<!-- calender plugin -->
<script src='<?php echo ROOT_URL_BASE?>bower_components/moment/min/moment.min.js'></script>
<script src='<?php echo ROOT_URL_BASE?>bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
<!-- data table plugin -->
<script src='<?php echo ADMIN_JS_PATH?>jquery.dataTables.min.js'></script>

<!-- select or dropdown enhancer -->
<script src="<?php echo ROOT_URL_BASE?>bower_components/chosen/chosen.jquery.min.js"></script>
<!-- plugin for gallery image view -->
<script src="<?php echo ROOT_URL_BASE?>bower_components/colorbox/jquery.colorbox-min.js"></script>
<!-- notification plugin -->
<script src="<?php echo ADMIN_JS_PATH?>jquery.noty.js"></script>
<!-- library for making tables responsive -->
<script src="<?php echo ROOT_URL_BASE?>bower_components/responsive-tables/responsive-tables.js"></script>
<!-- tour plugin -->
<script src="<?php echo ROOT_URL_BASE?>bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
<!-- star rating plugin -->
<script src="<?php echo ADMIN_JS_PATH?>jquery.raty.min.js"></script>
<!-- for iOS style toggle switch -->
<script src="<?php echo ADMIN_JS_PATH?>jquery.iphone.toggle.js"></script>
<!-- autogrowing textarea plugin -->
<script src="<?php echo ADMIN_JS_PATH?>jquery.autogrow-textarea.js"></script>
<!-- multiple file upload plugin -->
<script src="<?php echo ADMIN_JS_PATH?>jquery.uploadify-3.1.min.js"></script>
<!-- history.js for cross-browser state change on ajax -->
<script src="<?php echo ADMIN_JS_PATH?>jquery.history.js"></script>
<!-- application script for Charisma demo -->
<script src="<?php echo ADMIN_JS_PATH?>charisma.js"></script>
<script src="<?php echo ADMIN_JS_PATH?>jquery.validate.js"></script>
<script src="<?php echo JS_PATH?>common.js"></script>
<link rel="shortcut icon" href="<?php echo CSS_PATH?>img/favicon.ico">
</head>

<body>
<!-- topbar starts -->
<?php if($isMemberLogin != false) {?>
<div class="navbar navbar-default" role="navigation">
  <div class="navbar-inner">
    <button type="button" class="navbar-toggle pull-left animated flip"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
    <a class="navbar-brand" href="<?php echo ROOT_URL;?>"> <span><?php echo SITE_NAME?></span></a>
    
    <!-- user dropdown starts -->
    <div class="btn-group pull-right">
      <button class="btn btn-default dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-user"></i><span class="hidden-sm hidden-xs"> Welcome <?php echo $activeMemberDetails->first_name;?></span> <span class="caret"></span> </button>
      <ul class="dropdown-menu">
        <li><a href="<?php echo MEMBER_ROOT_URL?>admin_profile">Profile</a></li>
        <li class="divider"></li>
        <li><a href="logout">Logout</a></li>
      </ul>
    </div>
    <!-- user dropdown ends --> 
    
    <!-- theme selector starts --> 
    
    <!-- theme selector ends -->
    
    <ul class="collapse navbar-collapse nav navbar-nav top-menu">
<!--      <li><a href="--><?php //echo ROOT_URL?><!--"><i class="glyphicon glyphicon-globe"></i> Visit Site</a></li>-->
      <?php /*?><li class="dropdown"> <a href="#" data-toggle="dropdown"><i class="glyphicon glyphicon-star"></i> Dropdown <span
                            class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li><a href="#">Action</a></li>
          <li><a href="#">Another action</a></li>
          <li><a href="#">Something else here</a></li>
          <li class="divider"></li>
          <li><a href="#">Separated link</a></li>
          <li class="divider"></li>
          <li><a href="#">One more separated link</a></li>
        </ul>
      </li>
      <li>
        <form class="navbar-search pull-left">
          <input placeholder="Search" class="search-query form-control col-md-10" name="query"
                               type="text">
        </form>
      </li><?php */?>
    </ul>
  </div>
</div>
<?php } ?>

<!-- topbar ends -->
<div class="ch-container">
<div class="row">

<?php 
if($isMemberLogin != false) {
?>
<!-- left menu starts -->
<div class="col-sm-2 col-lg-2">
  <div class="sidebar-nav">
    <div class="nav-canvas">
      <div class="nav-sm nav nav-stacked"> </div>
      <ul class="nav nav-pills nav-stacked main-menu">
        <li class="nav-header">Main</li>
        <li><a class="ajax-link" href="<?php echo MEMBER_ROOT_URL ?>"><i class="glyphicon glyphicon-home"></i><span> Dashboard</span></a> </li>
        <?php
        //if($activeMemberDetails->module_access[0] == 'FULL' || in_array('1', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-user"></i><span> Saved search</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo MEMBER_ROOT_URL;?>search">Saved search</a></li>
            <li><a href="<?php echo MEMBER_ROOT_URL;?>admin/add">New search</a></li>
          </ul>
        </li>
        <?php //}
        /*if($activeMemberDetails->module_access[0] == 'FULL' || in_array('2', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-user"></i><span> Members</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>members">Member List</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>members/add">Add Member</a></li>
          </ul>
        </li>
        <?php }
        if($activeMemberDetails->module_access[0] == 'FULL' || in_array('3', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-file"></i><span> Category</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>category">Category List</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>category/add">Add Category</a></li>
          </ul>
        </li>
        <?php }
        if($activeMemberDetails->module_access[0] == 'FULL' || in_array('3', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-file"></i><span> Classified</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>classified">Classified List</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>classified/add">Add Classified</a></li>
          </ul>
        </li>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-file"></i><span> Brands</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>brand">Brand List</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>brand/add">Add Brand</a></li>
          </ul>
        </li>
        <?php }
        if($activeMemberDetails->module_access[0] == 'FULL' || in_array('4', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-list-alt"></i><span> CMS Pages</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>cms">CMS Page List</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>cms/add">Add CMS Page</a></li>
          </ul>
        </li>
        <?php }
        if($activeMemberDetails->module_access[0] == 'FULL' || in_array('6', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-picture"></i><span> Home Page Banners</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>home_page_image">Home Page Banners</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>home_page_image/add">Add Home Page Banner Image</a></li>
          </ul>
        </li>
        <?php }
        if($activeMemberDetails->module_access[0] == 'FULL' || in_array('7', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-picture"></i><span> Advertize</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>advertize">Advertize List</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>advertize/add">Add Advertize </a></li>
          </ul>
        </li>
        <?php }
        if($activeMemberDetails->module_access[0] == 'FULL' || in_array('5', $activeMemberDetails->module_access)){?>
        <li class="accordion"> <a href="#"><i class="glyphicon glyphicon-cog"></i><span> Settings</span></a>
          <ul class="nav nav-pills nav-stacked">
            <li><a href="<?php echo ADMIN_ROOT_URL ?>setting">Setting List</a></li>
            <li><a href="<?php echo ADMIN_ROOT_URL ?>setting/add">Add Setting </a></li>
          </ul>
        </li>
        <?php }*/ ?>
        
      </ul>
    </div>
  </div>
</div>
<!--/span--> 
<!-- left menu ends -->
<?php } ?>
<noscript>
<div class="alert alert-block col-md-12">
  <h4 class="alert-heading">Warning!</h4>
  <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
</div>
</noscript>
