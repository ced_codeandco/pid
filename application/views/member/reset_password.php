<div class="row">
    <div class="col-lg-12">
        <h3 class="text-left">Reset Password</h3>
    </div>

    <div class="col-lg-4">
        <?php
        echo validation_errors();
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        ?>
        <?php
        if (!empty($validToken)) {
            $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validate_admin();');
            echo form_open(MEMBER_ROOT_URL . 'reset_password/' . $password_token, $attributes); ?>
                <div class="form-group">
                    <label for="exampleInputEmail1">New Password*</label>
                    <input type="password" required="required" name="password" id="password" placeholder="Enter New Password" class="form-control">
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Confirm Password*</label>
                    <input type="password" required="required" name="retype_password" id="retype_password" placeholder="Password Again" class="form-control">
                </div>
                <button class="sign-in">Change Password</button>

            </form><?php
        } else {?>
            <a href="<?php echo ROOT_URL?>forgot_password"><button class="sign-in">Forget Password</button></a><?php
        }?>
        <div class="clearfix"></div>

        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="clearfix"></div>
    </div><!-- /.col-lg-4 -->

    <!-- /.col-lg-4 -->


</div>
<?php
return;
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/1/2015
 * Time: 11:44 AM
 */?>
<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL . (!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<div class="main-wraper">

    <div class="container">

        <div class="pages">

            <ul class="breadcrumbs"><li><a href="<?php echo ROOT_URL;?>">Home</a></li><li>Reset Password</li></ul>

            <h2 class="page_heading">Reset Password</h2>


                <?php
                echo validation_errors();
                $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
                echo form_open('',$attributes); ?>
                <?php
                    if (!empty($validToken)) {
                        $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data', 'onsubmit' => 'return validate_admin();');
                        echo form_open(MEMBER_ROOT_URL . 'reset_password/' . $password_token, $attributes); ?>
                            <div class="pages_left reset_pwd">
                                <div class="registration_login reset_pwd">
                                    <input type="password" required="required" name="password" id="password" placeholder="Enter New Password">

                                    <input type="password" required="required" name="retype_password" id="retype_password" placeholder="Password Again">

                                    <input type="submit" value="Change Password" />
                                </div>
                            </div>
                         </form><?php
                    } else {
                        $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
                        echo form_open(MEMBER_ROOT_URL.'forgot_password',$attributes);?>
                        <div class="pages_left">
                            <div class="registration_login forget_pwd">
                                <h2>Enter Your Email Address</h2>
                                <?php
                                if(isset($errMsg) && $errMsg != ''){ ?>
                                    <div class="alert alert-danger">
                                        <?php echo $errMsg;?>
                                    </div>
                                    <?php unset($errMsg);
                                }
                                if(isset($succMsg) && $succMsg != ''){ ?>
                                    <div class="alert alert-success">
                                        <?php echo $succMsg;?>
                                    </div>
                                    <?php unset($succMsg);
                                }?>
                                <input type="email" required="required" name="username" id="username"
                                       placeholder="username@provider.com"
                                       value="<?php echo !empty($_POST['username']) ? $_POST['username'] : '' ?>">

                                <input type="submit" value="Reset Password" class="fgt-pwd-button"/>

                                <span class="reg_term">Don't have an account? <a
                                        href="<?php echo ROOT_URL; ?>register">Register</a></span>
                                </form>
                            </div>
                        </div>
                    <?php
                    }?>

        </div>

    </div>

</div>
