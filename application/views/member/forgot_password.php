<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 11/1/2015
 * Time: 11:44 AM
 */?>
<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL . (!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<div class="row">
    <div class="col-lg-12">
        <h3 class="text-left">Forgot Password</h3>
        <p>Don't have an <strong class="red-text">P&ID</strong> account? Please <a href="<?php echo ROOT_URL?>register">register</a>. (It's FREE!)</p>
    </div>
    <?php echo validation_errors();?>
    <div class="col-lg-4">
        <?php
        $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
        echo form_open(MEMBER_ROOT_URL.'forgot_password',$attributes);
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        ?>
        <div class="form-group">
            <label for="exampleInputEmail1">Email Address*</label>
            <input type="email" required="required" name="username" id="username" placeholder="username@provider.com" value="<?php echo !empty($_POST['username']) ? $_POST['username'] : ''?>" class="form-control">
        </div>


        <button type="submit" class="sign-in">Reset Password</button>
        <div class="clearfix"></div>

        </form>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="clearfix"></div>
    </div><!-- /.col-lg-4 -->

    <!-- /.col-lg-4 -->


</div>

