

    <div class="row">
        <div class="col-md-12">
            <div class="flexslider">
                <ul class="slides">
                    <li>World's only Automated <strong>P&ID parts count software.</strong><br class="visible-lg">
            <span>We do parts counts <strong>24 hours a day 7 days a week.<br <br class="visible-lg">
                    2000 P&ID diagrams</strong> counted in 48 hours</span>.</li>
                    <li>World's only Automated <strong>P&ID parts count software.</strong><br class="visible-lg">
            <span>We do parts counts <strong>24 hours a day 7 days a week.<br <br class="visible-lg">
                    2000 P&ID diagrams</strong> counted in 48 hours</span>.</li>
                </ul>
            </div>

        </div>
    </div>


    <div class="row how-it-work-box">
        <!-- Three columns of text below the carousel -->
        <h2>HOW IT WORKS</h2>
        <div class="col-lg-4">
            <center><img src="<?php echo ROOT_URL_BASE;?>images/upload.png" class="visible-md visible-sm visible-xs"></center>
            <p><span>UPLOAD</span><br>
                The first step is to log in upload your P&ID diagrams to our server. Click multiple files to upload them. The first step is to log in upload your P&ID diagrams to our server. Click multiple files to upload them.  </p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <center><img src="<?php echo ROOT_URL_BASE;?>images/prosess.png" class="visible-md visible-sm visible-xs"></center>
            <p><span>PROCESS</span><br>
                We then get to work counting all the parts on the P&ID. Our process is about 99% accurate (we find that general parts counts are at best 95% accurate). The process is very fast because all we do is count parts.  If you upload 2000 drawings or less, then you will hear back from us within 48 hours with the results. We send you an email when the process is complete alerting you that the process is complete. </p>
        </div><!-- /.col-lg-4 -->
        <div class="col-lg-4">
            <center><img src="<?php echo ROOT_URL_BASE;?>images/download.png" class="visible-md visible-sm visible-xs"></center>
            <p><span>DOWNLOAD</span><br>
                When you receive an email that your files have been processed then all you have to do is log into your account and you will be able to download your results. There will be one results file to download for every P&ID you have uploaded. </p>
        </div><!-- /.col-lg-4 -->
    </div><!-- /.row -->
    <?php if (!empty($news_list) && is_array($news_list)) {?>
    <div class="home_news_setion">
        <div class="news_head">
            <h2>News<a class="viewAll" href="<?php echo ROOT_URL;?>news_list">View All</a></h2>

        </div><div class="clearfix"></div>
        <div class="flexslider">
            <ul class="slides"><?php
                if (!empty($news_list) && is_array($news_list)) {
                    foreach ($news_list as $news) { ?>
                        <li><a href="<?php echo ROOT_URL.'news/'.$news->url_slug;?>"><strong><?php echo $news->title;?>:</strong> <?php echo $news->small_description;?> <span class="read_more">Read more &raquo;</span></a></li>
                    <?php
                    }
                }?>
            </ul>
        </div>
    </div>
    <?php }?>



