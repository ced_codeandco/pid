<div class="row">
    <div class="col-lg-12">
        <!--<ul class="bradcram">
            <li><a href="<?php /*echo ROOT_URL;*/?>">Home</a></li>
            <li>My Files</li>
        </ul>-->
        <h3 class="text-left">My Files</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">

        <?php $this->load->view('templates/member_tab', $tabData);?>

        <div id="myTabContent" class="tab-content">


            <div role="tabpanel" class="tab-pane fade active in" id="profile" aria-labelledby="profile-tab">

                <?php
                if(isset($errMsg) && $errMsg != ''){ ?>
                    <div class="alert alert-danger">
                        <?php echo $errMsg;?>
                    </div>
                    <?php unset($errMsg);
                }
                if(isset($succMsg) && $succMsg != ''){ ?>
                    <div class="alert alert-success">
                        <?php echo $succMsg;?>
                    </div>
                    <?php unset($succMsg);
                }?>

                <table class="table">
                    <?php
                    if (empty($classifiedList) OR !is_array($classifiedList)) {?>

                        <div class="alert alert-danger">You have not uploaded any files.</div>
                        <p>
                            <a href="<?php echo ROOT_URL?>submit_file"><button class="update-profile-btn update-profile-btn">Submit a file </button></a>
                        </p>
                     <?php
                    } else {?>
                        <tr>
                            <th class="border-none table-col-1">No</th>
                            <th class="table-col-2">Title</th>
                            <th class="table-col-4">Creted Date</th>
                            <th class="table-col-4">Conversion Status</th>
                            <th class="table-col-6">Action</th>
                        </tr>
                        <?php

                        $i = 0;
                        $paOrder = !empty($recordCountStart) ? $recordCountStart : 1;
                        foreach ($classifiedList as $classified) {//classified_slug ?>
                            <tr>
                                <td class="border-none table-col-1"><?php echo $paOrder; ?></td>
                                <td class="table-col-2"> <?php echo $classified->title; ?></td>
                                <td class="table-col-4"><?php echo date('d M Y H:i', strtotime($classified->created_date_time)); ?></td>
                                <td class="table-col-4">
                                    <?php if ($classified->file_status == '1') { ?>
                                        <span class="active">In progress</span>
                                    <?php } else { ?>
                                        <span class="red-text"><a
                                                href="<?php echo ROOT_URL; ?>download_file/<?php echo $classified->id ?>">Download</a></span>
                                    <?php } ?>
                                    </span>
                                </td>
                                <td class="table-col-6">
                                <span class="in-active bg-none">
                                    <a href="#"
                                       onclick="javascript:if(confirm('Are you sure to delete ? ')){location.href='<?php echo MEMBER_ROOT_URL ?>dashboard/delete_classifieds/<?php echo $classified->id ?>'}"
                                       class="red-text">Delete</a>
                                </span>
                                </td>
                            </tr>
                            <?php
                            $paOrder++;

                        }
                    }?>
                </table>


            </div>


        </div>
    </div><!-- /.col-lg-12 -->


</div>