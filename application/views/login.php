<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL . (!empty($_GET['back']) ? base64_decode($_GET['back']) : 'submit_file');?>">
<div class="row">
    <div class="col-lg-12">
        <h3 class="text-left">Login</h3>
        <p class="login-note-margin-botm">Don't have an <strong class="red-text">P&ID</strong> account? Please <a href="<?php echo ROOT_URL?>register_via_linkedin">register</a>. (It's FREE!)</p>
    </div>

    <div class="col-lg-4">
        <?php
        $attributes = array('name' => 'loginForm', 'id' => 'loginForm', 'enctype' => 'multipart/form-data');
        $loginUrl = ROOT_URL.'login'.(!empty($_GET['back']) ? '?back='.$_GET['back'] : '');
        echo form_open($loginUrl, $attributes); ?>
        <?php
        if(isset($errMsg) && $errMsg != ''){ ?>
            <div class="alert alert-danger">
                <?php echo $errMsg;?>
            </div>
            <?php unset($errMsg);
        }
        if(isset($succMsg) && $succMsg != ''){ ?>
            <div class="alert alert-success">
                <?php echo $succMsg;?>
            </div>
            <?php unset($succMsg);
        }
        ?>
            <div class="form-group">
                <p class="posword-box quick-title"><strong>Login</strong></p>
                <label for="exampleInputEmail1">Email Address</label>
                <input type="email" required="required" name="username" id="username" placeholder="username@provider.com" value="<?php echo !empty($_POST['username']) ? $_POST['username'] : ''?>" class="form-control" >
            </div>
            <div class="form-group">
                <p class="posword-box">Password <a href="<?php echo ROOT_URL;?>forgot_password" class="text-right">Forgot Password?</a></p>
                <input type="password" class="form-control" name="password" id="password">
            </div>


            <button type="submit" class="sign-in">Sign in</button>
        <div class="clearfix"></div>
        <p>Don't have an account? <a href="<?php echo ROOT_URL;?>register_via_linkedin" class="red-text bold-text">Register</a></p>
        </form>
        <p>&nbsp;</p>
        <p>&nbsp;</p>
        <div class="clearfix"></div>
    </div><!-- /.col-lg-4 -->

    <div class="col-lg-4">
        <div class="col-lg-12">
            <p class="posword-box quick-title"><strong>Instant Connect</strong></p>
            <!--<div class="social-link"><a href="#" id="facebookLoginLink">Sign in with Facebook</a></div>-->
            <div class="social-link"><a href="#" class="linkdine" id="linkedInBtn">Sign in with Linkedin</a></div>
            <div class="clearfix"></div>

            <p class="font-light">Sign in with an existing social account to
                get started right away.</p>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
            <p class="visible-lg">&nbsp;</p>
        </div>
    </div><!-- /.col-lg-4 -->


</div><!-- /.row -->
<input type="hidden" id="linked-in-post" value="Checkout <?php echo SITE_NAME. ' | ' .ROOT_URL;?>">
<script src="<?php echo ROOT_URL_BASE?>js/facebookLogin.js"></script>
<script src="<?php echo ROOT_URL_BASE?>js/linkedInLogin.js"></script>
<script type="text/javascript" src="//platform.linkedin.com/in.js">
    api_key: 77rf7f8avavofz
    authorize: true
    onLoad: onLinkedInLoad
</script>


