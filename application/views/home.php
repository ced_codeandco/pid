

    <div class="row">
        <div class="col-md-12">
            <div class="flexslider homepage-slider">
                <ul class="slides">
                    <li><span class="bold-slider">World's only Automated <strong><br />P&ID Parts Count Software.</strong></span><br class="visible-lg">
            <span>We do parts counts <strong>24 hours a day 7 days a week.<br <br class="visible-lg">
                    2000 P&ID diagrams</strong> counted in 48 hours</span>.</li>
                    <li><span class="bold-slider">World's only Automated <strong><br />P&ID Parts Count Software.</strong></span><br class="visible-lg">
            <span>We do parts counts <strong>24 hours a day 7 days a week.<br <br class="visible-lg">
                    2000 P&ID diagrams</strong> counted in 48 hours</span>.</li>
                </ul>
            </div>
            <!-- Start EasyHtml5Video.com BODY section -->
            <div class="col-md-12">
                <div class="video_wrap">
                    <video controls="controls"  poster="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.jpg" style="width:100%" width="100%" title="P&IDPartsCount.com">
                            <source src="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.m4v" type="video/mp4" />
                            <source src="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.webm" type="video/webm" />
                            <source src="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.ogv" type="video/ogg" />
                            <source src="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.mp4" />
                            <object type="application/x-shockwave-flash" data="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/flashfox.swf" width="100%" height="500" style="position:relative;">
                                <param name="movie" value="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/flashfox.swf" />
                                <param name="allowFullScreen" value="true" />
                                <param name="flashVars" value="autoplay=false&controls=true&fullScreenEnabled=true&posterOnEnd=true&loop=false&poster=<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.jpg&src=partscount2.m4v" />
                                <embed src="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/flashfox.swf" width="100%" height="500" style="position:relative;"  flashVars="autoplay=false&controls=true&fullScreenEnabled=true&posterOnEnd=true&loop=false&poster=<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.jpg&src=partscount2.m4v"	allowFullScreen="true" wmode="transparent" type="application/x-shockwave-flash" pluginspage="http://www.adobe.com/go/getflashplayer_en" />
                                <img alt="partscount2" src="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/partscount2.jpg" style="position:absolute;left:0;" width="100%" title="Video playback is not supported by your browser" />
                            </object>
                        </video>
                    <script src="<?php echo ROOT_URL_BASE;?>assets/eh5v.files/html5video/html5ext.js" type="text/javascript"></script>
                    <!-- End EasyHtml5Video.com BODY section -->
                </div>
            </div>
        </div>
    </div>


    <div class="row how-it-work-box">
        <?php echo $cmsData->description;?>
    </div>
    <?php if (!empty($news_list) && is_array($news_list)) {?>
    <div class="home_news_setion">
        <div class="news_head">
            <h2>News<a class="viewAll" href="<?php echo ROOT_URL;?>news_list">View All</a></h2>

        </div><div class="clearfix"></div>
        <div class="flexslider">
            <ul class="slides"><?php
                if (!empty($news_list) && is_array($news_list)) {
                    foreach ($news_list as $news) { ?>
                        <li><a href="<?php echo ROOT_URL.'news/'.$news->url_slug;?>"><strong><?php echo $news->title;?>:</strong> <?php
                                if (strlen($news->title) + strlen($news->small_description) > 275){
                                    $len = 275 - strlen($news->title);
                                    echo substr($news->small_description, 0, $len).'...';
                                } else {
                                    echo $news->small_description;
                                }
                                ?> <span class="read_more">Read more &raquo;</span></a></li>
                    <?php
                    }
                }?>
            </ul>
        </div>
    </div>
        <div class="clearfix"></div>
    <?php }?>
