<div class="row cms_page_wrap">
    <div class="col-lg-12">
        <h3 class="text-left"><?php echo (!empty($show404)) ? 'Page not found' :$cmsData->title;?></h3>
    </div>
    <div class="devider-25px"></div>
    <?php if (!empty($cmsData->banner_image) && file_exists(DIR_UPLOAD_BANNER.$cmsData->banner_image)) {?>
    <div class="col-lg-4">
        <center><img src="<?php echo DIR_UPLOAD_BANNER_SHOW.$cmsData->banner_image;?>" class="img-responsive"></center>
        <p>&nbsp;</p>
    </div><!-- /.col-lg-4 -->
    <?php }?>

    <?php
    if ($show404 != true) {?>
        <div class="cms_page_content_wrap">
            <?php echo stripslashes($cmsData->description); ?>
        </div>
    <?php } else {?>
        <div class="col-lg-12">
            <div class="page-not-found">
                <p class="error-code">404</p>
                <p class="error-message">Page not found</p>
                <p class="error-message+"><a href="<?php echo ROOT_URL;?>">Click here to go to home page</a></p>
            </div>
        </div>
    <?php }?>
</div>
