<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/8/2015
 * Time: 7:45 PM
 */?>
<div class="main-wraper">
    <div class="container">
        <div class="pages">
            <ul class="breadcrumbs"><li><a href="<?php echo ROOT_URL;?>">Home</a></li><li>Category</li></ul>
            <h2 class="page_heading">Category</h2>
            <div class="page_content">


                <?php if (!empty($categoryList['mainCategory']) && is_array($categoryList['mainCategory'])){?>
                    <div class="row"><?php
                    $j = 1;
                    $subCategory = !empty($categoryList['subCategory']) ? $categoryList['subCategory'] : array();
                    $totalCount = count($categoryList['mainCategory']);
                    function sortByOption($a, $b) {
                        return strcmp($a->category_order, $b->category_order);
                    }
                    usort($categoryList['mainCategory'], 'sortByOption');

                    foreach ($categoryList['mainCategory'] as $category) {
                        if (!empty($category->id)) { ?>
                            <div class="col-cate-md-4">
                                <div class="item-icon-2">
                                    <a href="<?php echo ROOT_URL . 'search?category=' . $category->id; ?>">
                                        <?php
                                        if (!empty($category->category_image) && file_exists(DIR_UPLOAD_BANNER . $category->category_image)) {
                                            echo '<img src="' . DIR_UPLOAD_BANNER_SHOW . $category->category_image . '">';
                                        } else {
                                            echo '<img src="' . ROOT_URL_BASE . 'images/noicon.png">';
                                        }
                                        ?>
                                    </a>
                                </div>
                                <a href="<?php echo ROOT_URL . 'search?category=' . $category->id; ?>">
                                    <h3><?php echo $category->title; ?></h3></a>
                                <?php
                                if (!empty($subCategory[$category->id]) && is_array($subCategory[$category->id])) { ?>
                                    <ul><?php
                                    $subCategCount = 1;
                                    foreach ($subCategory[$category->id] as $subCateg) { ?>
                                    <li class="<?php echo ($subCategCount > 5) ? 'hidden-categ-list' : ''; ?>"><a
                                            href="<?php echo ROOT_URL . 'search?category=' . $category->id . '&sub_category=' . $subCateg->id; ?>"><?php echo content_truncate($subCateg->title, 35,' ', '...', true); ?>
                                            <span>(<?php echo !empty($subCateg->classifiedsCount) ? $subCateg->classifiedsCount : 0; ?>)</span></a>
                                        </li><?php
                                        $subCategCount++;
                                        //if ($subCategCount > 6 ) break;
                                    }
                                    if ($subCategCount > 6) {
                                        echo '<li class="categ-read-more"><a href="javascript:void(0)">Read more &raquo;&raquo;</a></li>';
                                    } else {
                                        for ($subCategCount; $subCategCount < 6; $subCategCount++) {
                                            echo '<li class="nodata"><a href="javascript:void(0)">&nbsp;</a></li>';
                                        }
                                        echo '<li class="nodata categ-read-more"><a href="javascript:void(0)">&nbsp;</a></li>';
                                    }?>
                                    </ul><?php
                                } else {
                                    echo '<div class="home-categ-nodata"> No Sub categories found</div>';
                                } ?>
                            </div>
                            <?php
                            $j++;
                        }
                    }?>
                    </div><?php
                }?>

                <?php /*
                <div class="col-cate-md-4 margin_none_cate">
                    <div class="item-icon-2"></div>
                    <h3>Home & Furniture</h3>
                    <ul>
                        <li><a href="#">Furniture (23) </a></li>
                        <li><a href="#">Clothing - Garments (21) </a></li>
                        <li><a href="#">Antiques - Handicrafts (10) </a></li>
                        <li><a href="#">Household (2) </a></li>
                        <li><a href="#">Jewellery (89) </a></li>
                    </ul>
                </div>

                <div class="col-cate-md-4">
                    <div class="item-icon-2 vehicles"></div>
                    <h3>Vehicles / Automobiles</h3>
                    <ul>
                        <li><a href="#">Used Cars for Sale (331)</a></li>
                        <li><a href="#">Auto Accessories & Parts (63)</a></li>
                        <li><a href="#">Boats (32)</a></li>
                        <li><a href="#">Heavy Vehicles (10)</a></li>
                        <li><a href="#">Motorcycles (2)</a></li>
                        <li class="hidden-categ-list"><a href="#">Used Cars for Sale (331)</a></li>
                        <li class="hidden-categ-list"><a href="#">Auto Accessories & Parts (63)</a></li>
                        <li class="hidden-categ-list"><a href="#">Boats (32)</a></li>
                        <li class="hidden-categ-list"><a href="#">Heavy Vehicles (10)</a></li>
                        <li class="hidden-categ-list"><a href="#">Motorcycles (2)</a></li>
                        <li class="categ-read-more"><a href="javascript:void(0)">Read more »»</a></li>
                    </ul>
                </div>

                <div class="col-cate-md-4">
                    <div class="item-icon-2 services"></div>
                    <h3>Services</h3>
                    <ul>
                        <li><a href="#">Education & Classes (46)</a></li>
                        <li><a href="#">Movers & Packers (23)</a></li>
                        <li><a href="#">Drivers & Taxi (31)</a></li>
                        <li><a href="#">Event Services (2)</a></li>
                        <li><a href="#">Maids & Domestic Help  (21)</a></li>
                    </ul>
                </div>

                <div class="col-cate-md-4 margin_none_cate">
                    <div class="item-icon-2 electronics"></div>
                    <h3>Electronics</h3>
                    <ul>
                        <li><a href="#">Used Cars for Sale (331)</a></li>
                        <li><a href="#">Auto Accessories & Parts (63)</a></li>
                        <li><a href="#">Boats (32)</a></li>
                        <li><a href="#">Heavy Vehicles (10)</a></li>
                        <li><a href="#">Motorcycles (2)</a></li>
                    </ul>
                </div>

                <div class="col-cate-md-4">
                    <div class="item-icon-2 book"></div>
                    <h3>Books & Sports</h3>
                    <ul>
                        <li><a href="#">Activities (23)</a></li>
                        <li><a href="#">Fitness (1)</a></li>
                        <li><a href="#">Gym (8) </a></li>
                        <li><a href="#">Hospitals & Clinics (11)</a></li>
                        <li><a href="#">Parlours (79)</a></li>
                    </ul>
                </div>

                <div class="col-cate-md-4">
                    <div class="item-icon-2 beauty"></div>
                    <h3>Beauty & Fashion</h3>
                    <ul>
                        <li><a href="#">Bags  (77)</a></li>
                        <li><a href="#">Beauty Products (213) </a></li>
                        <li><a href="#">Clothing M/F (1231)</a></li>
                        <li><a href="#">Jewelry (211)</a></li>
                        <li><a href="#">Shoes M/F (623)</a></li>
                    </ul>
                </div>*/?>


            </div>

        </div>

    </div>
</div>
<?php return;?>



<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Categories</li>
    </ul>
    <h2 class="page-title">Categories</h2>
    <?php
    //print_r($categoryList);
    if (!empty($categoryList['mainCategory']) && is_array($categoryList['mainCategory'])){?>
        <div class="row"><?php
        $j = 1;
        $subCategory = !empty($categoryList['subCategory']) ? $categoryList['subCategory'] : array();
        $totalCount = count($categoryList['mainCategory']);
        function sortByOption($a, $b) {
            return strcmp($a->category_order, $b->category_order);
        }
        usort($categoryList['mainCategory'], 'sortByOption');
        foreach ($categoryList['mainCategory'] as $category) {
            if (!empty($category->id)) {
                ?>
            <div class="main-categries-box <?php echo (($j % 4) == 1) ? 'margin-none' : '';?>">
                <a href="<?php echo ROOT_URL?>search?category=<?php echo $category->id;?>&source_catgories=1">
                    <div class="img_div">
                        <?php
                        if (!empty($category->category_image) && file_exists(DIR_UPLOAD_BANNER . $category->category_icon_large)) {
                            echo '<img src="' . DIR_UPLOAD_BANNER_SHOW . $category->category_icon_large . '">';
                        } else {
                            echo '<img src="' . ROOT_URL_BASE . 'images/real-estate.png">';
                        }
                        ?>
                    </div>
                    <p><?php echo $category->title;?></p>
                </a>
                </div><?php
                echo (($j % 4) == 0) ? '</div><div class="row">' : '';
                $j++;
            }
        }?>
        </div><?php
    }?>
    <div class="divider-futered"></div>
</div>