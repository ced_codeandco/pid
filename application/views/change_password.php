<div class="row">
    <div class="col-lg-12">
        <h3 class="text-left">Change Password</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">

        <?php //$this->load->view('templates/member_tab', $tabData);?>


        <div id="myTabContent" class="tab-content">
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">

                <p class="border-bottom">Name:<span class="red-text"><?php echo $profileData->first_name. ' ' .$profileData->last_name?></span> (not <?php echo $profileData->first_name;?> ? <a href="<?php echo ROOT_URL?>logout"><strong>logout</strong></a>)</p>

                <div class="col-lg-8 peding-left-none zeebra-form">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <form id="passwordChangeForm" method="post" action="<?php echo MEMBER_ROOT_URL?>change_password" class="profile-lable">
                        <input type="hidden" name="formAction" value="changePassword">
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Old Password</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input class="form-control" name="old_password" required type="password" placeholder="Old Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">New Password</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input class="form-control" name="new_password" id="new_password_field" required type="password" placeholder="New Password">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Confirm Password</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input class="form-control" name="confirm_password" required type="password" placeholder="Confirm Password">
                            </div>
                        </div>
                        <div class="post-field-col col-md-4-form left-margine-20px-rorm">
                            <button type="submit" class="update-profile-btn">Change Password</button>
                        </div>

                    </form>

                </div>

            </div>




        </div>
    </div><!-- /.col-lg-12 -->


</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#passwordChangeForm').validate({
            rules: {
                old_password: { required: true },
                new_password: {
                    required: true,
                    minlength: 6
                },
                confirm_password: {
                    required: true,
                    equalTo: "#new_password_field"
                }
            },
            messages: {
                old_password: { required: 'Please enter your old password' },
                new_password: {
                    required: 'Please enter a new password',
                    minlength: 'Password should contain at least 6 characters'
                },
                confirm_password: {
                    required: 'Retype new password to confirm',
                    equalTo: 'New password and confirm password fields should match'
                }
            }
        })
        var row_count = 1;
        $('.zeebra-form').find('div.form-group').each(function(){
            if (row_count % 2 == 1) {
                $(this).addClass('odd');
            }
            row_count++;
        })
    })
</script>
