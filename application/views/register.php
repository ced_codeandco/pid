<input type="hidden" id="login-redirect" value="<?php echo ROOT_URL.(!empty($_GET['back']) ? base64_decode($_GET['back']) : '');?>">
<div class="row">
    <div class="col-lg-12">
        <!--<ul class="bradcram">
            <li><a href="<?php /*echo ROOT_URL;*/?>">Home</a></li>
            <li>Register</li>
        </ul>-->
        <h3 class="text-left">Register</h3>
    </div>

    <div class="devider-25px"></div>

    <div class="col-lg-12">




        <div id="myTabContent" class="tab-content ">
            <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home-tab">

                <p class="border-bottom"><strong>Already have a <strong class="red-text">P&ID</strong> account? Please <a href="<?php echo ROOT_URL?>login">Login</a>.</strong><br /><span class="red-text">Note: All fields are mandatory</span></p>

                <div class="col-lg-10 peding-left-none zeebra-form">
                    <?php
                    if(isset($errMsg) && $errMsg != ''){ ?>
                        <div class="alert alert-danger">
                            <?php echo $errMsg;?>
                        </div>
                        <?php unset($errMsg);
                    }
                    if(isset($succMsg) && $succMsg != ''){ ?>
                        <div class="alert alert-success">
                            <?php echo $succMsg;?>
                        </div>
                        <?php unset($succMsg);
                    }?>
                    <?php echo validation_errors(); ?>
                    <form method="post" action="<?php echo ROOT_URL.'register';?>"  name="registerForm" id="registerForm" class="profile-lable">
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Email Address</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" placeholder="username@provider.com" type="text" name="email" value="<?php echo !empty($formData['email']) ? $formData['email'] : ''?>" id="email" onblur="validate_user_name(this, '<?php echo ROOT_URL.'register/check_duplicate_email';?>', 'label', 'error')">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Confirm Email Address</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" placeholder="username@provider.com" type="text" name="email_again" value="<?php echo !empty($formData['email_again']) ? $formData['email_again'] : ''?>">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Password</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" type="password" class="validate[required] inputbg" placeholder="Password" name="password" id="password"  value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Confirm Password</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" type="password" class="validate[required] inputbg" placeholder="Password" name="password_again" value="">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">First Name</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" placeholder="Enter your real name" class="validate[required] inputbg" type="text" name="first_name" value="<?php echo !empty($formData['first_name']) ? $formData['first_name'] : ''?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Last Name</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" placeholder="Your last name will be kept private" class="validate[required] inputbg" type="text" name="last_name" value="<?php echo !empty($formData['last_name']) ? $formData['last_name'] : ''?>" >
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Select your question</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <div class="select_style">
                                    <select name="security_question">
                                        <option value="">Select one</option>
                                        <?php
                                        if (!empty($questionList)) {
                                            foreach ($questionList as $question) {
                                                echo '<option '.((!empty($formData['security_question']) && $formData['security_question'] == $question->question) ? "selected='selected'" : "").'>' . $question->question . '</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Secret Answer</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" placeholder="Secret Answer (Minimum 5 characters)" class="validate[required] inputbg" type="text" name="security_answer" value="<?php echo !empty($formData['security_answer']) ? $formData['security_answer'] : ''?>" >
                            </div>
                        </div>
                        <div class="clearfix"></div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Date Of Birth</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" id="datePicker" placeholder="Date of birth" class="validate[required] inputbg" type="text" name="date_of_birth" value="<?php echo !empty($formData['date_of_birth']) ? $formData['date_of_birth'] : ''?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">Country</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <div class="select_style">
                                    <select name="country" id="country">
                                        <option value="">Select</option>
                                        <?php if (is_array($cityList)) {
                                            foreach ($countryList as $country) {
                                                echo '<option value="' .$country->id. '" ' .((!empty($formData['country']) && $formData['country'] == $country->id) ? 'selected="selected"' : ''). '>' .$country->name. '</option>';
                                            }
                                        }?>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">City</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input  class="form-control" placeholder="City" class="validate[required] inputbg" type="text" name="city" value="<?php echo !empty($formData['city']) ? $formData['city'] : ''?>" >
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">&nbsp;</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none">
                                <input type="checkbox" class="checkbox" id="subscribe_intimation"   name="subscribe_intimation" value="1" >
                                <label class="subscribe_text" for="subscribe_intimation">Allow P&ID to send me occasional updates about the site.</label>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <div class="form-group no-zebra">
                            <div class="col-lg-3 col-sm-3 peding-left-none">
                                <label for="exampleInputEmail1">&nbsp;</label>
                            </div>
                            <div class="col-lg-9 col-sm-9  peding-left-none register-btn-wrap">
                                <button type="submit" class="update-profile-btn">Register</button>
                                <p class="grey-text register_form">
                                    By clicking on Register, you agree to the

                                    <?php echo SITE_NAME;?> <a class="ajax-popup" href="<?php echo ROOT_URL.'cms/'.$terms_popup_content->cms_slug.'/ajax';?>">Terms and Conditions</a> and the <?php echo SITE_NAME;?> <a class="ajax-popup" href="<?php echo ROOT_URL.'cms/'.$privacy_popup_content->cms_slug.'/ajax';?>">Privacy Policy</a>.
                                </p>
                            </div>
                        </div>
                    </form>



                </div>
                <div class="clearfix"></div>


                <div class="clearfix"></div>

            </div>




        </div>
    </div><!-- /.col-lg-12 -->


</div>
<script type="text/javascript" src="<?php echo ROOT_URL_BASE?>js/jquery.validate.min.js"></script>
<link href="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.css" rel="stylesheet">
<script src="<?php echo ROOT_URL_BASE;?>assets/jquery-ui/jquery-ui.js"></script>
<script type="text/javascript">
    $(function() {

        $( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear(),
            maxDate: "-1Y"
        });
        /*$( "#datePicker" ).datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'd MM yy',
            yearRange: ((new Date).getFullYear() - 90)+':'+(new Date).getFullYear()
        });*/

        $.validator.addMethod("customvalidation",
            function(value, element) {
                return /^[A-Za-z\d=#$%@_ -]+$/.test(value);
            },
            "Sorry, no special characters allowed"
        );
        $.validator.addMethod("validPhoneNumber", function(value, element) {
            //$(element).val(value.trim());
            return this.optional(element) || validPhoneNumber(value);
        }, "Invalid phone number");
        $('#registerForm').validate({
            rules: {
                email:{required: true, email:true, maxlength:100},
                email_again:{required: true, email:true, equalTo: '#email', maxlength:100},
                password:{required: true, maxlength:100, minlength:5},
                password_again:{required: true, maxlength:100 ,equalTo: '#password'},
                first_name: {required: true, maxlength:100},
                last_name: {required: true, maxlength:100},
                security_question:{required: true},
                security_answer:{required: true, maxlength:100, minlength:5 },
                date_of_birth:{required: true, date:true},
                contact_no:{required: true, maxlength: 15, validPhoneNumber: true, maxlength:10},
                country:{required: true},
                city:{required: true},
                age_confirmation:{required: true},
            },
            messages: {
                first_name: {required: 'Please enter your first name'},
                last_name: {required: 'Please enter your last name'},
                email:{required: 'Please enter a valid email', email:'Invalid/Incomplete Email ID'},
                email_again:{required: 'Please enter a valid email', email:'Invalid/Incomplete Email ID', equalTo:'Email mismatch'},
                password:{required: 'Please enter a password', minlength:'Your password should contain at least 5 character'},
                password_again:{required: 'Please confirm your password', equalTo: 'Password mismatch'},
                security_question:{required: 'Please select a security question'},
                security_answer:{required: 'Please enter your answer to security  question' },
                /*gender:{required: 'Please select your gender'},*/
                date_of_birth:{required: 'Please enter your date of birth'},
                contact_no:{required: 'Please enter your contact number', maxlength: 'Invalid phone number', validPhoneNumber: 'Invalid phone number'},
                address_1:{required: 'Address 1 is required'},
                city:{required: 'Please enter your city'},
                country:{required: 'Please select your country'},
                age_confirmation:{required: 'Please confirm your age'},
            }
        })
        var row_count = 1;
        $('.zeebra-form').find('div.form-group').not('.no-zebra').each(function(){
            if (row_count % 2 == 1) {
                $(this).addClass('odd');
            }
            row_count++;
        })
    });
</script>