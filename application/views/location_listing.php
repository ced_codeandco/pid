<?php
/**
 * Created by PhpStorm.
 * User: USER
 * Date: 6/8/2015
 * Time: 7:10 PM
 */?>

<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Locations</li>
    </ul>
    <h2 class="page-title">Locations</h2>
    <?php if (!empty($cityList)){?>
        <div class="row"><?php
        $i = 1;
        foreach ($cityList as $city) {?>
            <ul class="locations-list <?php echo (($i %4 ==1)) ? 'first-list-margine-none': ''?>">
                <li><?php //print_r($cityClassifiedsCount);?>
                    <?php echo '<a class="location-list-head" href="'.ROOT_URL.'search?search_city='.$city->id.'&source_locations=1">'.$city->title.'</span></a>';?>
                    <span>
                    (<?php echo !empty($cityClassifiedsCount[$city->id]->allClassifiedsCount) ? ($cityClassifiedsCount[$city->id]->allClassifiedsCount) : '0'?>)
                    </span><?php
                    if (!empty($localityClassifiedsCount[$city->id])){
                        //print_r($localityClassifiedsCount[$city->id]);
                        $j = 1;
                        foreach ($localityClassifiedsCount[$city->id] as $classifiedsLocality) {
                            echo '<li class="'.(($j > 5) ? 'hidden-categ-list' : '').'"><a href="'.ROOT_URL.'search?search_city='.$city->id.'&classified_locality='.$classifiedsLocality->id.'&source_locations=1">'.$classifiedsLocality->title.' <span>'.(!empty($classifiedsLocality->classifiedsCount) ? $classifiedsLocality->classifiedsCount : '0').'</span></a></li>';
                            $j++;
                        }
                        if ($j > 6){
                            echo  '<li class="categ-read-more"><a href="javascript:void(0)">Read more &raquo;&raquo;</a></li>';
                        }
                    }?>
                </li>
            </ul><?php
            echo ($i%4==0) ? '</div><div class="row">' : '';
            $i++;
        }?>
        </div><?php
    }?>
    </div>

    <div class="divider-futered"></div>
</div>



<?php /*<div class="container ">
    <ul class="bradcram">
        <li><a href="<?php echo ROOT_URL;?>">Home</a></li>
        <li>Locations</li>
    </ul>
    <h2 class="page-title">Locations</h2>
    <?php if (!empty($cityList)){?>
        <div class="row"><?php
        $i = 1;
        foreach ($cityList as $city) {?>
            <div class="location-list-wrap">
            <ul class="locations-list <?php echo (($i %4 ==1)) ? 'first-list-margine-none': ''?>">
                <li><?php //print_r($cityClassifiedsCount);?>
                    <?php echo '<a class="location-list-head" href="'.ROOT_URL.'search?search_city='.$city->id.'&source_locations=1">'.$city->title.'</span></a>';?>
                    <span>
                    (<?php echo !empty($cityClassifiedsCount[$city->id]->allClassifiedsCount) ? ($cityClassifiedsCount[$city->id]->allClassifiedsCount) : '0'?>)
                    </span><?php
                    $j = 1;
                    if (!empty($localityClassifiedsCount[$city->id])){
                        //print_r($localityClassifiedsCount[$city->id]);
                        foreach ($localityClassifiedsCount[$city->id] as $classifiedsLocality) {
                            echo '<li><a href="'.ROOT_URL.'search?search_city='.$city->id.'&classified_locality='.$classifiedsLocality->id.'&source_locations=1">'.$classifiedsLocality->title.' <span>'.(!empty($classifiedsLocality->classifiedsCount) ? $classifiedsLocality->classifiedsCount : '0').'</span></a></li>';

$j++;
}


}?>
                </li>
            </ul><?php
if ($j > 7){ ?><div class="more-link-list <?php echo (($i %4 ==1)) ? ' first-list-margine-none': ''?>"><a href="javascript:void(0)">More &raquo;</a></div><?php }
echo '</div>';
echo ($i%4==0) ? '</div><div class="row">' : '';
$i++;
}?>
        </div><?php
}?>
</div>

<div class="divider-futered"></div>
</div>*/?>