<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Public_controller extends CI_Controller {

    public $user_id = false;
    public $is_admin = false;
    public $is_member = false;
    public $is_logged_in = false;
    public $headerData;
    public $contentData;
    public $footerData;
    public $language;

    /**
     * Class constructor.
     *
     */
    public function __construct()
    {
        parent::__construct();
        $this->user_id = $this->session->userdata('id');
        $this->is_member = $this->session->userdata('is_member');
        $this->load->model('member_model');
        $this->load->helper('url');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model(
            array(
                'member_model', 'classified_model', 'cms_model', 'admin_model', 'model_model', 'brand_model',
                'advertize_model', 'inquiry_model', 'home_page_image_model', 'setting_model', 'category_model'
            )
        );

        $this->setting_model->defineAllSettingVariables();
        $this->footerData['current_slug'] = $this->headerData['current_slug'] = $this->get_current_slug();
        $this->headerData['top_pages'] = $this->cms_model->getAllRecords('id, title, cms_slug, is_popup', " on_header ='1' ", 'ORDER BY cms_order ASC');
        $this->footerData['bottom_pages'] = $this->cms_model->getAllRecords('id, title, cms_slug, is_popup', " on_footer ='1' ", 'ORDER BY cms_order ASC');
        $this->footerData['refund_popup_content'] = $this->cms_model->getDetails(CMS_REFUND_POPUP_PAGE_ID);
        $this->footerData['terms_popup_content'] = $this->cms_model->getDetails(CMS_TERMS_POPUP_PAGE_ID);
        $this->headerData['isMemberLogin'] = !empty($this->user_id) ? $this->member_model->checkMemberLogin() : false;

        if (!empty($this->user_id) && $this->is_member == true && $this->headerData['isMemberLogin'] != false) {
            $this->is_logged_in = true;
            $this->headerData['isLoggedIn'] = $this->is_logged_in;
            $this->headerData['isMemberLogin'] = $this->member_model->checkMemberLogin();
        } else {
            $this->session->unset_userdata('is_member');
            $this->session->unset_userdata('id');
            $this->headerData['isMemberLogin'] = false;
        }
    }


    public function generateRobotText() {
        $verify_robot = array(
            mt_rand(1,9), mt_rand(1,9)
        );

        if ($this->input->post('verify_robot')) {
            $this->contentData['verify_robot_old'] = $this->session->userdata('verify_robot');
        }
        $this->contentData['verify_robot'] = $verify_robot;
        $data = array('verify_robot' => $verify_robot);
        $this->session->set_userdata($data);
    }

    public function valid_captcha($val) {
        $verify_robot = $this->session->userdata('verify_robot');
        //print_r($verify_robot);
        if (!is_numeric($val) OR empty($verify_robot[0]) OR empty($verify_robot[1]) OR ($val != ($verify_robot[0] + $verify_robot[1])) ) {
            $this->form_validation->set_message('valid_captcha', 'Please verify you are not a robot');
            return false;
        }

        return true;
    }

    public function get_current_slug() {
        $current_url = current_url();
        $current_slug = str_replace(ROOT_URL, '', $current_url);

        return $current_slug;
    }
}