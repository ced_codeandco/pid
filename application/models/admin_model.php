<?php
class Admin_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function getPage($slug)
    {
		$this->db->where("url like '/$slug'");
        $query = $this->db->get('pages');
		return $query->result();
    }
	
	function fnColumnToField( $i )
	{
		if ( $i == 0 )
			return "id";
		if ( $i == 1 )
			return "name";			
		else if ( $i == 2 )
			return "email";
		
	}
	function checkAdminLogin()
	{
		$user = $this->session->userdata('admin_id');

        if($user == null || $user == '')
		{
			return FALSE;
		}
		else
		{
			$this->db->where('id',$user);
			$query=$this->db->get("tbl_admin");
			if($query->num_rows >0)
			{
				$user_id = $query->row()->id;
				$screen_name = $query->row()->first_name.' '.$query->row()->last_name;
				$data = array(
					'admin_name' => $query->row()->first_name.' '.$query->row()->last_name,
					'admin_id' => $query->row()->id,
					'email' => $query->row()->email,
					'module_access' => $query->row()->module_access,		  
					'is_admin_logged_in' => true
				);
				$this->session->set_userdata($data);
				return TRUE;	
			}else{
				return FALSE;
			}	
	
       }		
	
	}
	function adminLogout(){
        $this->session->unset_userdata('admin_name');
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('module_access');
        $this->session->unset_userdata('is_admin_logged_in');
        $this->session->unset_userdata('is_admin');

		return TRUE;
	}
	function adminLogin(){
		$email =  $this->input->post('username');
		$password =  md5($this->input->post('password'));
		
		$this->db->where('email', $email);
		$this->db->where('password', $password);
        $this->db->where('is_active','1');
		$query = $this->db->get('tbl_admin') or die(mysql_error());
		if($query->num_rows >= 1) {
			mysql_query("UPDATE tbl_admin SET last_login_date = NOW(), last_login_ip='".$this->input->ip_address()."' WHERE id= ".$query->row()->id."");
			return $query->row()->id;
		}else{
            return '0';
		}
	}

    /**
     * Function to update password
     * If password token is passed, where condition checks password token and resets the token field aswell
     *
     * @return bool
     */
	function updatePassword($password_token = null){
		$data['password'] = md5($this->input->post('password'));
        if (!empty($password_token)) {
            $data['password_token'] = '';
        }
        if (empty($password_token)) {
            $this->db->where("email", $this->input->post('email'));
        } else {
            $this->db->where("password_token", $password_token);
        }
		$this->db->update('tbl_admin',$data);

        return true;
	}

	function getAdminDetails($adminId){
		
		$this->db->where('id', $adminId);		
		$query = $this->db->get('tbl_admin') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsFromEmail($email){
		
		$this->db->where('email', $email);		
		$query = $this->db->get('tbl_admin') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getCountryList(){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
        $this->db->where('is_active', '1');
        $this->db->where('is_deleted', '0');
        $this->db->order_by('sort_order ASC');
        $query = $this->db->get('tbl_country');
		return $query->result();
	}
    function getCountryById($country_id){
        $this->db->where('id',$country_id);
        $query = $this->db->get('tbl_country');
        return $query->result();
    }
	function getBusinessList(){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
		$query = $this->db->get('tbl_business_type');
		return $query->result();
	}
	function getRandomSecurityQuestions($count = 10){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
		$this->db->order_by(' id ', 'random', false);
        $this->db->limit($count);
        $query = $this->db->get('tbl_secret_question');

		return $query->result();
	}
	function getCityLookup(){

        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
        $this->db->where('is_active', '1');
        $this->db->where('is_deleted', '0');
        $this->db->order_by('city_order ASC');
		$query = $this->db->get('tbl_cities');
		$results = $query->result();
        $return = array();
        foreach ($results as $result) {
            $return[$result->id] = $result;
        }

        return $return;
	}
	function getCityList($country_id=0){

        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
		if($country_id != 0){
			$this->db->where('country_id',$country_id);
		}
        $this->db->where('is_active', '1');
        $this->db->where('is_deleted', '0');
        $this->db->order_by('city_order ASC');
		$query = $this->db->get('tbl_cities');
        //echo$this->db->last_query();
		return $query->result();
	}
	function getCity($city_id=0){
		if($city_id != 0){
			$this->db->where('id',$city_id);
		}
		$query = $this->db->get('tbl_cities');
		return $query->result();
	}
	function getCityLookUpByCountry($country_id=0){

        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }

		if($country_id != 0){
			$this->db->where('country_id',$country_id);
		}
        $this->db->where('is_active', '1');
        $this->db->where('is_deleted', '0');
        $this->db->order_by('city_order ASC');
		$query = $this->db->get('tbl_cities');
		$queryData = $query->result();
        $return = array();
        if ($queryData) {
            foreach ($queryData as $data) {
                $return[$data->id] = $data->title;
            }
        }

        return $return;
	}
    function getFuelTypeList($id=0){
        if($id != 0){
            $this->db->where('id',$id);
        } else {
            $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
            if(!empty($language_id)) {
                $this->db->where('language_id', $language_id);
            }
        }
        $query = $this->db->get('tbl_fuel_type');
        return $query->result();
    }
	function changePassword(){
		$_POST['password'] = md5($this->input->post('password'));
		$data = array(
			'password' => $this->input->post('password'),
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_admin',$data);
		
	}
	public function sendEmail($toName, $fromName, $toEmail, $fromEmail, $subject, $content){

	$this->load->library('email');
	$this->email->set_mailtype("html");
	$this->email->set_crlf( "\r\n" );
	$this->email->from($fromEmail,$fromName);
	$this->email->to($toEmail,$toName);
	$this->email->subject($subject);
	$this->email->message($content);
	if($this->email->send())
		return TRUE;
    else
	    return FALSE;
	}
	function addDetails(){
		
		$_POST['password'] = md5($this->input->post('password'));
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password'),
			'module_access' => $this->input->post('module_access'),
			'admin_role' => 'Member Admin',
			'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
		
		$this->db->insert('tbl_admin',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	function updateDetails(){
		
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email'),
			'module_access' => $this->input->post('module_access'),
			'is_active' => $this->input->post('is_active')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_admin',$data);

		
		return true;
		
	}
	function updateCurrentDetails(){
		
		$data = array(
			'first_name' => $this->input->post('first_name'),
			'last_name' => $this->input->post('last_name'),
			'email' => $this->input->post('email')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_admin',$data);

		
		return true;
		
	}
	
	function checkEmailExist($email,$id=0){
		if($email!='' && $id != 0){
			$sql = "SELECT * FROM tbl_admin WHERE email = '".$email."' AND id != $id";
		}else if($email!=''){
			$sql = "SELECT * FROM tbl_admin WHERE email = '".$email."' ";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}
	public function activeAdminDetails(){
		$adminId = $this->session->userdata('admin_id');
		if(isset($adminId) && $adminId > 0){
			$this->db->where('id', $adminId);		
			$query = $this->db->get('tbl_admin') or die(mysql_error());
			if($query->num_rows >= 1){
				$query->row()->module_access = explode(',',$query->row()->module_access);
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_admin SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		
		mysql_query("DELETE FROM tbl_admin WHERE id= ".$id);
		return true;
	}
	function getModuleList(){
			
		$query = $this->db->get('tbl_module');

		return $query->result();
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_admin WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		return $query->result();
	}

    /**
     * Function to generate random string and update the token field of admin table
     *
     * @param $user_id
     *
     * @return string
     */
    function setPasswordToken($user_id)
    {
        $reset_pwd_string = strtolower(base64_encode($user_id . gen_random_string(15)));
        $this->db->where('id', $user_id);
        $this->db->update('tbl_admin', array('password_token' => $reset_pwd_string));

        return $reset_pwd_string;
    }

    /**
     * Function to check if password token is valid
     *
     * @param $password_token
     *
     * @return boolean
     */
    function isValidPasswordToken($password_token)
    {
        $this->db->select('id');
        $this->db->where('password_token', $password_token);
        $query = $this->db->get('tbl_admin');

        return ($query->num_rows > 0);
    }

    function subscribeEmail($email) {
        $this->db->where('subscriber_email_address', $email);
        $this->db->from('tbl_subscriber');
        $query = $this->db->get();
        /*$result = $query->result();
        var_dump($result);*/
        //echo $this->db->last_query();
        if($query->num_rows > 0) {

            return false;
        } else {
            $this->db->insert('tbl_subscriber', array('subscriber_email_address' => $email));

            return true;
        }

    }

    function prepareLookUpItems($table)
    {
        $this->db->from($table);
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
        $this->db->where('is_active', '1');
        $this->db->where('is_deleted', '0');
        $this->db->order_by('sort_order ASC');

        $query = $this->db->get();
        return $query->result();
    }

    function getTransmissionLookUp(){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
        $query = $this->db->get('tbl_transmission');
        $query_data = $query->result();
        $return = false;
        foreach ($query_data as $data) {
            $return[$data->id] = $data;
        }
        return $return;
    }

    function getFuelLookUp(){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
        $query = $this->db->get('tbl_fuel_type');
        $query_data = $query->result();
        $return = false;
        foreach ($query_data as $data) {
            $return[$data->id] = $data;
        }
        return $return;
    }

    function getSubscriberList()
    {
        $this->db->from('tbl_subscriber');
        $query = $this->db->get();
        if($query->num_rows > 0) {

            return $query->result();
        } else {

            return false;
        }
    }

    function deleteSubscriber($id)
    {
        mysql_query("DELETE FROM tbl_subscriber WHERE subscriber_id= ".$id);
        return true;
    }

    function getLanguageInfo($languageHandle = '', $languageId = '')
    {
        $return = '';
        if (!empty($languageHandle)) {
            $this->db->where('handle', $languageHandle);
            $this->db->from('tbl_languages');
            $query = $this->db->get();
            if($query->num_rows > 0) {

                $return = $query->row();
            }
        } else if (!empty($languageId)) {

            $this->db->where('id', $languageId);
            $this->db->from('tbl_languages');
            $query = $this->db->get();
            if($query->num_rows > 0) {

                $return = $query->row();
            }
        }
        if (empty($return) && defined('DEFAULT_LANGUAGE_ID')) {
            $this->db->where('id', DEFAULT_LANGUAGE_ID);
            $this->db->from('tbl_languages');
            $query = $this->db->get();

            $return = $query->row();
        }
        //print_r($return);
        return $return;

        $this->db->where('handle', $languageHandle);
        $this->db->from('tbl_languages');
        $query = $this->db->get();
        if($query->num_rows > 0) {

            return $query->row();
        } else if (defined('DEFAULT_LANGUAGE_ID')){

            $this->db->where('id', DEFAULT_LANGUAGE_ID);
            $this->db->from('tbl_languages');
            $query = $this->db->get();

            return $query->row();
        }
    }

    public function getLanguagesList()
    {
        $this->db->where('status', '1');
        $this->db->from('tbl_languages');
        $query = $this->db->get();

        return $query->result();
    }
}