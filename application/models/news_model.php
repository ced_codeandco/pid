<?php
class News_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		
		$order = $this->getLastOrder();
		$sort_order = $order + 1;
		
		$data = array(
			'title' => $this->input->post('title'),
			'url_slug' => $this->input->post('url_slug'),
			'parent_id' => $this->input->post('parent_id'),
			'small_description' => $this->input->post('small_description'),
			'description' => $this->input->post('description'),
			'banner_image' => $this->input->post('banner_image'),
			'sort_order' => $sort_order,
			'meta_title' => $this->input->post('meta_title'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'on_header' => $this->input->post('on_header'),
			'on_footer' => $this->input->post('on_footer'),
			'is_active' => $this->input->post('is_active'),
			'created_by' => $this->session->userdata('admin_id'),
			'updated_by' => $this->session->userdata('admin_id'),
			'created_date_time' =>date('Y-m-d H:i:s')			
		);



		$this->db->insert('tbl_news',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){
		$data = array(
			'title' => $this->input->post('title'),
			'parent_id' => $this->input->post('parent_id'),
			'small_description' => $this->input->post('small_description'),
			'description' => $this->input->post('description'),
			'banner_image' => $this->input->post('banner_image'),
			'meta_title' => $this->input->post('meta_title'),
			'meta_desc' => $this->input->post('meta_desc'),
			'meta_keywords' => $this->input->post('meta_keywords'),
			'on_header' => $this->input->post('on_header'),
			'on_footer' => $this->input->post('on_footer'),
			'is_active' => $this->input->post('is_active'),
			'updated_by' => $this->session->userdata('admin_id')
		);

		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_news',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_news SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("UPDATE tbl_news SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_news') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function generateNewsSlug($title='cms page'){
		$urltitle=preg_replace('/[^a-z0-9]/i',' ', ltrim(rtrim(strtolower($title))));
		$newurltitle=str_replace(" ","-",$urltitle);
		$queryCount = "SELECT url_slug from tbl_news WHERE url_slug LIKE '".$newurltitle."%'";
		$rqC = mysql_num_rows(mysql_query($queryCount));
		if($rqC != 0){
			$newurltitle = $newurltitle.'-'.$rqC; 
		}
		return $newurltitle;				
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_news WHERE 1=1 ";

		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				
				$query_data[$i]->sub_page_count = $this->getSubPageCount($value['id']);
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getCMSPageList($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_news WHERE 1=1 ";


		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		if(count($query_data) > 0 )
		{
			$i = 0;
			foreach ($query->result_array() as $value) 
			{
				if (!empty($value['id'])) {
                    $sql1 = "select $all FROM tbl_news WHERE parent_id = " . $value['id'] . " AND is_deleted='0' AND is_active='1'  ";
                    $query = $this->db->query($sql1);
                    $query_data[$i]->sub_page_List = $query->result();
                }
				$i++;
			}
		
		}
		
		return $query_data;
	}
	function getSubPageCount($parent_id){

		$sql ="select count(id) as sub_page_count FROM tbl_news WHERE parent_id = $parent_id AND is_deleted='0'   ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;		
	}
	function getLastOrder(){
		$sql ="select sort_order FROM tbl_news ORDER BY sort_order desc LIMIT 0,1 "; 
		$query = $this->db->query($sql);
		$result = $query->result();

		return !empty($result[0]->sort_order) ? $result[0]->sort_order : 0;
	}
	
	function changeOrder($id,$sort_order,$position){
		if($id!='' && $sort_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);

			if($position=='Dn'){
				$qr="select sort_order,id from tbl_news where parent_id = $pageDetails->parent_id  AND sort_order > '".$sort_order."' AND is_deleted='0' order by sort_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->sort_order;
					$qry = "UPDATE tbl_news SET `sort_order`= $sort_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_news SET `sort_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}
			
			if($position=='Up'){
				$qr="select sort_order,id from tbl_news where parent_id = $pageDetails->parent_id  AND sort_order < '".$sort_order."' AND is_deleted='0' order by sort_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
                //print_r(array($id,$sort_order,$position));
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->sort_order;
					$qry = "UPDATE tbl_news SET `sort_order`= $sort_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_news SET `sort_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}
		}
	}

    /**
     * Function to get page details with slug
     *
     * @param $url_slug
     *
     * @return bool
     */
    function getPageDetails($url_slug){
        $this->db->where('url_slug', $url_slug);
        $query = $this->db->get('tbl_news');
        $query_data = $query->result();

        return ($query_data != false && is_array($query_data)) ? $query_data[0] : false;
    }

    public function getCount($where) {
        $sql ="select COUNT(*) AS row_count FROM tbl_news WHERE 1=1 ";

        if($where!=''){
            $sql .= " AND $where ";
        }
        $sql .= " AND is_deleted='0' ";
        $query = $this->db->query($sql);
        $query_data = $query->row();

        return !empty($query_data->row_count) ? $query_data->row_count : 0;
    }
}