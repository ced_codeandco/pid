<?php
class Packages_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),
			'advertize_url1' => $this->input->post('advertize_url'),
			'image_path' => $this->input->post('image_path'),
			'advertize_type' => $this->input->post('advertize_type'),			
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')			
		);

		$this->db->insert('tbl_packages',$data) or die(mysql_error()); 	
		$id=mysql_insert_id();
		return $id;
	}
	
	function updateDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),
			'advertize_url' => $this->input->post('advertize_url'),
			'image_path' => $this->input->post('image_path'),
			'advertize_type' => $this->input->post('advertize_type'),
			'is_active' => $this->input->post('is_active'),
			'created_date_time' =>date('Y-m-d H:i:s')
		);
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_packages',$data);

		return true;
		
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_packages SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("DELETE FROM tbl_packages WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){


		$this->db->where('id', $id);
		$query = $this->db->get('tbl_packages') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getRandomAdvt($count = 1){
        $type='Square';
		$sql ="select * FROM tbl_packages WHERE 1=1 AND is_active = '1' AND advertize_type = '$type'";

        $sql .=" ORDER BY RAND() LIMIT $count";
		$query = $this->db->query($sql);
		$result = $query->result();

		return !empty($result) ? $result : false;
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_packages WHERE 1=1 ";

        if($where!=''){
			$sql .= " AND $where ";
		}
		
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		return $query_data;
	}
	
		
}