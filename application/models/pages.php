<?php
class pages extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		if($this->config->item('language') == "english")
			$this->load->database();
		else if($this->config->item('language') == "arabic")
			$this->load->database("arabic");
		else
			$this->load->database();
    }
    
    function getPage($slug)
    {
		$this->db->where("url like '/$slug'");
        $query = $this->db->get('pages');
		return $query->result();
    }
}