<?php
class City_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

    function addDetails(){

		$order = $this->getLastOrder();
		$city_order = $order + 1;

		$data = array(
			'title' => $this->input->post('title'),
			'country_id' => $this->input->post('country_id'),
			'created_by' => $this->session->userdata('admin_id'),
			'updated_by' => $this->session->userdata('admin_id'),
            'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')
		);
        if (empty($data['language_id'])) {
            $data['language_id'] = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        }
		$this->db->insert('tbl_cities',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;

	}

	function updateDetails(){

		$data = array(
            'title' => $this->input->post('title'),
            'country_id' => $this->input->post('country_id'),
            'created_by' => $this->session->userdata('admin_id'),
            'updated_by' => $this->session->userdata('admin_id'),
            'created_date' =>date('Y-m-d H:i:s'),
			'is_active' => $this->input->post('is_active'),
			'updated_by' => $this->session->userdata('admin_id')
		);
        if (empty($data['language_id'])) {
            $data['language_id'] = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        }
		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_cities',$data);

		return true;

	}

	function changeStatus($status,$id){

		mysql_query("UPDATE tbl_cities SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		mysql_query("UPDATE tbl_cities SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){

		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_cities') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}

	function getAllRecords($all='*',$where='',$orderby='',$limit=''){

		$sql ="select $all FROM tbl_cities WHERE 1=1 ";
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $sql .= " AND language_id='$language_id' ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}

		$query = $this->db->query($sql);
		$query_data = $query->result();
		//echo $this->db->last_query();

		return $query_data;
	}
	function getClassifiedLocalityList($locality_id, $getActive = false){

		$sql ="select id,title FROM tbl_cities WHERE 1=1 ";
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $sql .= " AND language_id='$language_id' ";
		$sql .= " AND country_id = 0 AND is_deleted='0' ";
		if ($getActive != false) {
            $sql .= " AND is_active = '1' ";
        }
		$query = $this->db->query($sql);
		$query_data = $query->result();



		$options = '';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent)
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($locality_id != 0 && $locality_id== $parent->id) ? 'selected="selected"' : "";
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title FROM tbl_cities WHERE 1=1 ";
                $sqlChild .= " AND language_id='$language_id' ";
				$sqlChild .= " AND country_id = ".$parent->id." AND is_deleted='0' ";
                if ($getActive != false) {
                    $sqlChild .= " AND is_active = '1' ";
                }

				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child)
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($locality_id != 0 && $locality_id== $child->id) ? 'selected="selected"' : "";
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
						$sqlChild1 ="select id,title FROM tbl_cities WHERE 1=1 ";
                        $sqlChild1 .= " AND language_id='$language_id' ";
						$sqlChild1 .= " AND country_id = ".$child->id." AND is_deleted='0' ";
                        if ($getActive != false) {
                            $sqlChild1 .= " AND is_active = '1' ";
                        }

						$queryChild1 = $this->db->query($sqlChild1);
						$childData1 = $queryChild1->result();
						if(count($childData1) > 0 )
						{
							foreach ($childData1 as $child1)
							{
								$options .='<option value="'.$child1->id.'"';
								$options .= ($locality_id != 0 && $locality_id== $child1->id) ? 'selected="selected"' : "";
								$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child1->title.'</option>';
							}
						}
					}
				}
			}
		}
		$options .= '';
		return $options;
	}
	function getParentLocalityLists($id=0,$country_id=0){
		$sql ="select id,title,country_id FROM tbl_cities WHERE 1=1 ";
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $sql .= " AND language_id='$language_id' ";
		$sql .= " AND country_id = 0 AND is_deleted='0' ";

		$query = $this->db->query($sql);
		$query_data = $query->result();



		$options = '<option value="0" selected="selected">Select Parent locality</option>';
		if(count($query_data) > 0 )
		{
			foreach ($query_data as $parent)
			{
				$options .='<option value="'.$parent->id.'"';
				$options .= ($country_id != 0 && $country_id== $parent->id) ? 'selected="selected"' : "";
				$options .= ($id != 0 && $id == $parent->id) ? "disabled='disabled'" : "";
				$options .= '>'.$parent->title.'</option>';
				$sqlChild ="select id,title,country_id FROM tbl_cities WHERE 1=1 ";
                $sqlChild .= " AND language_id='$language_id' ";
				$sqlChild .= " AND country_id = ".$parent->id." AND is_deleted='0' ";

				$queryChild = $this->db->query($sqlChild);
				$childData = $queryChild->result();
				if(count($childData) > 0 )
				{
					foreach ($childData as $child)
					{
						$options .='<option value="'.$child->id.'"';
						$options .= ($country_id != 0 && $country_id== $child->id) ? 'selected="selected"' : "";
						$options .= (($id != 0 && $id == $child->id) || ($id != 0 && $id == $child->country_id)) ? "disabled='disabled'" : "";
						$options .= '>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$child->title.'</option>';
					}
				}
			}

		}
		$options .= '';
		return $options;
	}
	function getSubPageCount($country_id){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
		$sql ="select count(id) as sub_page_count FROM tbl_cities WHERE is_deleted='0' AND country_id = $country_id AND language_id='$language_id' ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->sub_page_count;
	}
	function getLastOrder(){
		$sql ="select city_order FROM tbl_cities WHERE is_deleted='0' ORDER BY city_order desc LIMIT 0,1 ";
		$query = $this->db->query($sql);
		$result = $query->result();
		return $result[0]->city_order;

	}

	function changeOrder($id,$city_order,$position){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
		if($id!='' && $city_order!='' && $position!=''){
		$pageDetails = $this->getDetails($id);
			if($position=='Dn'){
				$qr="select city_order,id from tbl_cities where language_id='$language_id' AND country_id = $pageDetails->country_id  AND city_order > '".$city_order."' AND is_deleted='0' order by city_order asc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();
				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->city_order;
					$qry = "UPDATE tbl_cities SET `city_order`= $city_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_cities SET `city_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}

			if($position=='Up'){
				$qr="select city_order,id from tbl_cities where language_id='$language_id' AND country_id = $pageDetails->country_id  AND city_order < '".$city_order."' AND is_deleted='0' order by city_order desc limit 0,1";
				$query = $this->db->query($qr);
				$result1 = $query->result();

				if($result1 && count($result1)>0){
					$NewId=$result1[0]->id;
					$NewOrder = $result1[0]->city_order;
					$qry = "UPDATE tbl_cities SET `city_order`= $city_order WHERE id =".$NewId;
                    $this->db->query($qry);

					$qry1 = "UPDATE tbl_cities SET `city_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
				}
			}
		}
//echo $this->db->last_query(); die("here");
	}

	function getLocalityHierarchy($getActive = false)
    {
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $this->db->select('loc.id, loc.title, loc.sub_title, loc.country_id, loc.locality_image, count(clasfds.id) AS classifiedsCount', false);
        $this->db->from('tbl_cities  loc');
        $this->db->join('tbl_classified AS clasfds', "clasfds.classified_locality = loc.id AND clasfds.is_active='1'  AND clasfds.language_id='$language_id' ", 'LEFT');
        if ($getActive != false) {
            $this->db->where('loc.is_active', '1');
            $this->db->where('loc.is_deleted', '0');
        }
        $this->db->where('language_id', $language_id);
        $this->db->group_by('loc.id');
        $this->db->order_by('loc.city_order', 'ASC');

        $query = $this->db->get();
        $query_data = $query->result();
        $mainLocality = array();
        $subLocality = array();

        foreach ($query_data as $data) {
            if ($data->country_id == 0) {
                if (!empty($mainLocality[$data->id]->allClassifiedsCount)) {
                    $data->allClassifiedsCount = $mainLocality[$data->id]->allClassifiedsCount + $data->classifiedsCount;
                } else {
                    $data->allClassifiedsCount = $data->classifiedsCount;
                }
                $mainLocality[$data->id] = $data;
            } else {
                if (empty($mainLocality[$data->country_id])) {
                    $mainLocality[$data->country_id]->allClassifiedsCount = $data->classifiedsCount;
                } else {
                    $mainLocality[$data->country_id]->allClassifiedsCount += $data->classifiedsCount;
                }
                $subLocality[$data->country_id][$data->id] = $data;
            }
        }

        return array(
            0 => $mainLocality,
            1 => $subLocality
        );
    }

    function getLocalityLookup($country_id, $getActive = false){
        $sqlChild ="select id,title FROM tbl_cities WHERE 1=1 ";
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $sqlChild .= " AND language_id='$language_id' ";
        $sqlChild .= " AND country_id = ".$country_id." AND is_deleted='0' ";
        if ($getActive != false) {
            $sqlChild .= " AND is_active = '1' ";
        }

        $queryChild = $this->db->query($sqlChild);
        $childData = $queryChild->result();
        $lookUp = array();
        if(count($childData) > 0 ) {
            foreach ($childData as $child) {
                $lookUp[$child->id] = $child->title;
            }
        }
        return $lookUp;
    }

}