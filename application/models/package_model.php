<?php
class Package_model extends CI_Model {

    static $user_table = 'tbl_packages';
    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }

    function addDetails($emailVerification = false, $data = array()){
        if (empty($data)) {
            $data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'files_count' => $this->input->post('files_count'),
                'files_per_day' => $this->input->post('files_per_day'),
                'duration_value' => $this->input->post('duration_value'),
                'price' => $this->input->post('price'),
                'created_by' => $this->admin_id,
                'created_date' => date('Y-m-d H:i:s'),
                'created_ip' => $this->input->ip_address(),
                'is_active' => $this->input->post('is_active'),
            );
        }
        $order = $this->getLastOrder();
        $sort_order = $order + 1;
        if (empty($data['sort_order'])) {
            $data['sort_order'] = $sort_order;
        }


		$this->db->insert('tbl_packages',$data);
        //echo "<br /><br />".$this->db->last_query()."<br /><br />"; die();
		return $this->db->insert_id();
	}


    function getLastOrder(){
        $sql ="select sort_order FROM tbl_packages ORDER BY sort_order desc LIMIT 0,1 ";
        $query = $this->db->query($sql);
        $result = $query->result();

        return !empty($result[0]->sort_order) ? $result[0]->sort_order : 0;
    }

    function updateDetails($id = false, $data = array()){
        if (empty($data)) {
            $data = array(
                'title' => $this->input->post('title'),
                'description' => $this->input->post('description'),
                'files_count' => $this->input->post('files_count'),
                'files_per_day' => $this->input->post('files_per_day'),
                'duration_value' => $this->input->post('duration_value'),
                'price' => $this->input->post('price'),
                'updated_by' => $this->admin_id,
                'updated_ip' => $this->input->ip_address(),
                'is_active' => $this->input->post('is_active'),
            );
        }

        if ($id != false) {
            $this->db->where("id", $id);
        } else {
            $this->db->where("id", $this->input->post('id'));
        }
		$this->db->update('tbl_packages',$data);

		return true;
	}
	

	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_packages SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){
		
		mysql_query("UPDATE tbl_packages SET is_deleted = '1', deleted_date_time = '".date('Y-m-d H:i:s')."' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		$this->db->where('is_deleted', '0');
		$this->db->where('id', $id);		
		$query = $this->db->get('tbl_packages') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_packages WHERE 1=1 ";
		if($where!=''){

			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		$query = $this->db->query($sql);
		$query_data = $query->result();
		return $query_data;
	}

	function checkEmailExist($email,$id=0){
		if($email!='' && $id != 0){
			$sql = "SELECT * FROM tbl_packages WHERE email = '".$email."' AND id != $id AND is_deleted='0'";
		}else if($email!=''){
			$sql = "SELECT * FROM tbl_packages WHERE email = '".$email."' AND is_deleted='0'";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}
	public function activeMemberDetails(){
		$memberId = $this->session->userdata('id');
		if(isset($memberId) && $memberId > 0){
			$this->db->where('id', $memberId);		
			$query = $this->db->get('tbl_packages') or die(mysql_error());
			if($query->num_rows >= 1){
//				$query->row()->module_access = explode(',',$query->row()->module_access);
				return $query->row();
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

    /**
     * Function to get details of members from email
     *
     * @param string $email
     *
     * @return bool | array
     */
    function getDetailsFromEmail($email){

        $this->db->where('email', $email);
        $query = $this->db->get('tbl_packages') or die(mysql_error());
        if($query->num_rows >= 1)
            return $query->row();
        else
            return false;
    }



    function update_file_count($file_count, $user_id)
    {
        $active_package_details = $this->get_active_package_details($user_id);

        $this->db->where('id', $user_id);
        $this->db->set('pending_file_count', "pending_file_count-$file_count", FALSE);
        $this->db->update('tbl_member_package');
//echo $this->db->last_query();
        /*$this->member_model->addMemberLog($user_id);
        $this->db->where('id', $user_id);
        $this->db->set('file_pending_count', "file_pending_count-$file_count", FALSE);
        $this->db->update('tbl_member');*/
    }

    function increment_file_count($file_count, $user_id)
    {
        $active_package_details = $this->get_active_package_details($user_id);

        $this->db->where('id', $user_id);
        $this->db->set('pending_file_count', "pending_file_count+$file_count", FALSE);
        $this->db->update('tbl_member_package');

        /*$this->member_model->addMemberLog($user_id);
        $this->db->where('id', $user_id);
        $this->db->set('file_pending_count', "file_pending_count+$file_count", FALSE);
        $this->db->update('tbl_member');*/
    }
    public function validate_package($user_id = false)
    {

    }

    public function is_valid_package($package_id)
    {
        $package = $this->getDetails($package_id);

        return ($package->is_public == 1 && $package->is_active == 1);
    }

    public function update_member_package($data, $member_id)
    {
        if (!empty($data)) {
            //Set old subscription to expired
            $this->db->update('tbl_member_package', array('status' => '2', 'updated_ip' => $this->input->ip_address()), array('member_id' => $member_id));

            //Add new subscription
            $data['created_ip'] = $this->input->ip_address();
            $data['current_package_start_date'] = date('Y-m-d H:i:s');
            $data['created_date'] = date('Y-m-d H:i:s');
            $this->db->insert('tbl_member_package',$data);
            //echo $this->db->last_query();
            $member_package_id =  $this->db->insert_id();

            $this->member_model->addMemberLog($member_id);
            //Update member table
            $member_pkg_data = array(
                'active_package_id' => $data['package_id'],
                'active_package_subscription_id' => $member_package_id,
            );
            $this->db->update('tbl_member', $member_pkg_data, array('id' => $member_id));
            //echo $this->db->last_query(); die();
            return $member_package_id;
        }
    }

    public function updatePaymentRecords($data)
    {
        $this->db->insert('tbl_member_package',$data);
    }

    function sendPackageEmail($data, $packageDetails)
    {
        $member_id = $data['member_id'];
        $memberDetails = $this->member_model->getDetails($member_id);

        $this->load->library('emailclass');
        $message = "<p>Dear " . $memberDetails->first_name . ",</p><br />";

        $message .= "<p><strong>Congratulations!!</strong> Your package activated successfully at ".SITE_NAME." </p>";
        $message .= "<p>Please see the details below</p>";

        if ($data['price'] > 0) {
            $message .= "<p><strong>Payment Details</strong></p>";
            $message .= "<p><label>Billing Email:</label><strong>$data[billing_email]</strong></p>";
            $message .= "<p><label>Order Number:</label><strong>$data[order_number]</strong></p>";
        }
        $message .= "<p><strong>Package Details</strong></p>";
        $message .= "<p><label>Package Name:</label><strong>$packageDetails->title</strong></p>";
        $message .= "<p><label>Benefits:</label><strong>$packageDetails->description</strong></p>";
        $message .= "<p><label>Price:</label><strong>".(!empty($data['price']) ? 'AED '.$data['price'] : 'Free')."</strong></p>";

        $subject = 'Package activated successfully  ' . SITE_NAME;
        $content = '';
        $content .= $this->emailclass->emailHeader();
        $content .= $message;
        $content .= $this->emailclass->emailFooter();

        $email = $this->emailclass->send_mail($memberDetails->email, $subject, $content);
    }

    function changeOrder($id,$sort_order,$position){
        if($id!='' && $sort_order!='' && $position!=''){
            $pageDetails = $this->getDetails($id);

            if($position=='Dn'){
                $qr="select sort_order,id from tbl_packages where  sort_order > '".$sort_order."' AND is_deleted='0' order by sort_order asc limit 0,1";
                $query = $this->db->query($qr);
                $result1 = $query->result();
                if($result1 && count($result1)>0){
                    $NewId=$result1[0]->id;
                    $NewOrder = $result1[0]->sort_order;
                    $qry = "UPDATE tbl_packages SET `sort_order`= $sort_order WHERE id =".$NewId;
                    $this->db->query($qry);

                    $qry1 = "UPDATE tbl_packages SET `sort_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
                }
            }

            if($position=='Up'){
                $qr="select sort_order,id from tbl_packages where  sort_order < '".$sort_order."' AND is_deleted='0' order by sort_order desc limit 0,1";
                $query = $this->db->query($qr);
                $result1 = $query->result();
                //print_r(array($id,$sort_order,$position));
                if($result1 && count($result1)>0){
                    $NewId=$result1[0]->id;
                    $NewOrder = $result1[0]->sort_order;
                    $qry = "UPDATE tbl_packages SET `sort_order`= $sort_order WHERE id =".$NewId;
                    $this->db->query($qry);

                    $qry1 = "UPDATE tbl_packages SET `sort_order`= $NewOrder WHERE id =".$id;
                    $this->db->query($qry1);
                }
            }
        }
    }

    function get_active_package_details($member_id) {
        $this->db->where('is_deleted', '0');
        $this->db->where('is_active', '1');
        $this->db->where('status', '1');
        $this->db->where('current_package_end_date >', date('Y-m-d 00:00:00', strtotime('tomorrow')));
        $this->db->where('member_id', $member_id);
        $query = $this->db->get('tbl_member_package') or die(mysql_error());
        //echo $this->db->last_query();

        return ($query->num_rows >= 1) ? $query->row() :  false;
    }
    function get_active_package_limits($user_id)
    {
        //Get package details
        $package_limits = $this->get_active_package_details($user_id);

        if (!empty($package_limits)) {
            //Get number of fies uploaded today
            $where = " created_by = '$user_id' AND DATE_FORMAT(created_date_time,'%Y-%m-%d')  = '" . date('Y-m-d') . "' AND is_deleted = '0' AND is_active = '1' ";
            $files_uploaded_today = $this->classified_model->getAllRecordCount($where);
            $current_package_start_date = $package_limits->current_package_start_date;

            //Get number of fies uploaded after the package is activated
            $where = " created_by = '$user_id' AND created_date_time >= '$current_package_start_date' AND is_deleted = '0' AND is_active = '1' ";
            $files_uploaded_in_package = $this->classified_model->getAllRecordCount($where);

            //Calculate number of fies pending in the package
            $package_file_count = (!empty($package_limits->package_file_count)) ? $package_limits->package_file_count : 0;
            $pending_files_in_package = ($package_file_count - $files_uploaded_in_package);
            $package_limits->pending_files_in_package = $pending_files_in_package;

            //Calculate number of fies pending today
            $files_per_day = (!empty($package_limits->files_per_day)) ? $package_limits->files_per_day : 0;
            $files_pending_today = ($files_per_day - $files_uploaded_today);
            $package_limits->files_pending_today = $files_pending_today;
        }

        return $package_limits;
    }

    function calculate_package_end_date($duration_value)
    {
        if (!empty($duration_value)) {
            $duration_value++;
            return date('Y-m-d 00:00:00', strtotime("+ $duration_value days"));
        }

        return false;
    }
}