<?php
class Brand_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails(){
		
			
		$data = array(
			'title' => $this->input->post('title'),
			'is_active' => $this->input->post('is_active'),
			'created_date' =>date('Y-m-d H:i:s')			
		);
        if (empty($data['language_id'])) {
            $data['language_id'] = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        }

        $this->db->insert('tbl_vehicle_brand',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails(){
		
		$data = array(
			'title' => $this->input->post('title'),					
			'is_active' => $this->input->post('is_active')
			
		);
        if (empty($data['language_id'])) {
            $data['language_id'] = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        }

        $this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_vehicle_brand',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		
		mysql_query("UPDATE tbl_vehicle_brand SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){		
		mysql_query("UPDATE tbl_vehicle_brand SET is_deleted = '1',is_active = '0' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_vehicle_brand') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsFromTitle($title){
		
		$this->db->where('title', $title);
		$this->db->where('is_deleted', '0');

        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }

		$query = $this->db->get('tbl_vehicle_brand') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function checkTitleExist($title,$id=0){
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
		if($title!='' && $id != 0){
			$sql = "SELECT * FROM tbl_vehicle_brand WHERE title = '".$title."' AND id != $id AND is_deleted='0'  AND language_id='$language_id' ";
		}else if($title!=''){
			$sql = "SELECT * FROM tbl_vehicle_brand WHERE title = '".$title."' AND is_deleted='0'  AND language_id='$language_id' ";
		}
		$query = $this->db->query($sql);
		return $query->result();

	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_vehicle_brand WHERE 1=1 ";
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        $sql .= " AND language_id='$language_id' ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		
		return $query_data;
	}

    function getBrandLookup()
    {
        $this->db->select('*', false);
        $this->db->from('tbl_vehicle_brand');
        $language_id = !empty($this->language->id) ? $this->language->id : DEFAULT_LANGUAGE_ID;
        if(!empty($language_id)) {
            $this->db->where('language_id', $language_id);
        }
        $this->db->where('is_active', '1');

        $this->db->order_by('title', 'ASC');

        $query = $this->db->get();
        $query_data = $query->result();
        $return = false;
        foreach ($query_data as $data) {
            $return[$data->id] = $data;
        }
        return $return;
    }

}