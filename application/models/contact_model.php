<?php
class Contact_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
		$this->load->database();
    }
    
    function addDetails($data){
		
		$this->db->insert('tbl_contact_us',$data) or die(mysql_error());
		$id=mysql_insert_id();
		return $id;
		
	}
	
	function updateDetails($data){

		$this->db->where("id",$this->input->post('id'));
		$this->db->update('tbl_contact_us',$data);
		
		
		return true;
		
	}
	function changeStatus($status,$id){
		mysql_query("UPDATE tbl_contact_us SET is_active = '$status' WHERE id= ".$id."");
		return true;
	}
	function deleteRecord($id){		
		mysql_query("UPDATE tbl_contact_us SET is_deleted = '1',is_active = '0' WHERE id= ".$id."");
		return true;
	}
	function getDetails($id){
		
		$this->db->where('id', $id);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_contact_us') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getDetailsFromEmail($email){
		
		$this->db->where('email', $email);
		$this->db->where('is_deleted', '0');
		$query = $this->db->get('tbl_contact_us') or die(mysql_error());
		if($query->num_rows >= 1)
			return $query->row();
		else
            return false;
	}
	function getAllRecords($all='*',$where='',$orderby='',$limit=''){
		
		$sql ="select $all FROM tbl_contact_us WHERE 1=1 ";
		if($where!=''){
			$sql .= " AND $where ";
		}
		$sql .= " AND is_deleted='0' ";
		if($orderby!=''){
			$sql .= " $orderby ";
		}
		if($limit!=''){
			$sql .= " $limit ";
		}
		
		$query = $this->db->query($sql);
		$query_data = $query->result();
		
		
		return $query_data;
	}

}